/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {ErrorToast, WarningToast} from '../Components/Toasts';

/**
 * Checks response status after GET request, and fires Toast in any case of error
 * @param response server response to check
 * @returns true if status is 200, false otherwise
 */
export function CheckGetResponse(response: Response): boolean {
    switch (response.status) {
        case 200: //Request successful
            return true;
        case 400:
            ErrorToast("Error 400: Bad request, missing required parameters or otherwise malformed");
            break;
        case 401:
            ErrorToast("Error 401: Access token is missing or invalid (expired)");
            break;
        case 403:
            WarningToast("Error 403: Not authorized to perform this action or access this resource");
            break;
        default:
            ErrorToast("ERROR: unknown error occurred.");
            break;
    }

    return false;
}

/**
 * Checks response status after PUT request, and fires Toast in any case of error
 * @param response server response to check
 * @returns true if status is 200, false otherwise
 */
export function CheckPutResponse(response: Response): boolean {
    return CheckGetResponse(response);
}

/**
 * Checks response status after DELETE request, and fires Toast in any case of error
 * @param response server response to check
 * @returns true if status is 200, false otherwise
 */
export function CheckDeleteResponse(response: Response): boolean {
    return CheckGetResponse(response);
}

/**
 * Checks response status after POST request, and fires Toast in any case of error
 * @param response server response to check
 * @returns true if status is 200 or 201, false otherwise
 */
export function CheckPostResponse(response: Response): boolean {
    switch (response.status) {
        case 200: //Post but no creation
            return true;
        case 201: //Post successful
            return true;
        case 400:
            //TODO enable this again for debugging in development
            ErrorToast("Bad request, missing required parameters or otherwise malformed");
            break;
        case 401:
            ErrorToast("Access token is missing or invalid (expired)");
            break;
        case 403:
            WarningToast("Not authorized to perform this action or access this resource");
            break;
        default:
            ErrorToast("ERROR: unknown error occurred.");
            break;
    }

    return false;
}