/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {Pin, PinPreview, Utm} from '../Types';
import {getCookie} from './Cookie';
import {JSONHeader, request} from './Request';
import {CheckDeleteResponse, CheckGetResponse, CheckPostResponse, CheckPutResponse} from './ErrorHandling';
import {convertContent, ServerPinContent} from './PinContentRequests';

export type ServerPinPreview = {
    id_str: string,
    title: string,
    text: string,
    last_edited: string,
}

export type ServerPin = {
    id_str: string,
    last_edited: string,
    latitude_str: string,
    longitude_str: string,
    text: string,
    title: string,
    user_id_str: string,
    contents: ServerPinContent[]
    utm: Utm;
}

/**
 * Converts a ServerPinPreview to a local PinPreview
 * @param serverpinPreview a server version of pinPreview
 * @returns a local version of the pinPreview
 */
export function convertPinPreview(serverpinPreview: ServerPinPreview) {
    const fixedPinPreview: PinPreview = {
        id: serverpinPreview.id_str,
        last_edited: serverpinPreview.last_edited,
        text: serverpinPreview.text,
        title: serverpinPreview.title
    }
    return (fixedPinPreview);
}

/**
 * Converts a ServerPin to a local Pin
 * @param serverPin a server version of a Pin
 * @returns a local version of the Pin
 */
export function convertPin(serverPin: ServerPin) {
    const fixedPin: Pin = {
        lat: serverPin.latitude_str,
        lon: serverPin.longitude_str,
        contents: serverPin.contents.map(x => convertContent(x,serverPin.id_str)),
        utm: {Easting:"", Northing:"", ZoneNumber:"", ZoneLetter:""},
        text: serverPin.text,
        id: serverPin.id_str,
        last_edited: serverPin.last_edited,
        title: serverPin.title
    }
    return fixedPin
}

/**
 * If possible gets the pins corresponding to the given class.
 * @param classId id corresponding to the class you want the pins from.
 * @returns A promise of a list of pins or empty array if request went wrong.
 */
export async function getClassPins(classId: string, from: 'class' | 'class_all'): Promise<PinPreview[]> {
    const empty: PinPreview[] = [];
    const data: ServerPinPreview[] | undefined = await request("pins/?" + from + "=" + classId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got class pins");
                return response.json();
            }
        })
    if (data) {
        //Return list of PinPreview which complies to local standard
        return data.map(x => convertPinPreview(x));
    }
    return empty;
}

/**
 * If possible gets the pins corresponding to the given user.
 * @returns A promise of a list of pins or an empty array if request went wrong.
 */
export async function getUserPins(): Promise<PinPreview[]> {
    const empty: PinPreview[] = [];
    const userId = getCookie("user_id");
    const data: ServerPinPreview[] = await request("pins/?user=" + userId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got user pins");
                return response.json();
            }
        })
    if (data) {
        //Return list of PinPreview which complies to local standard
        return data.map(x => convertPinPreview(x));
    }
    return empty;
}

/**
 * Gets a single pin from the server with all it's details
 * @param pinId id of the pin you want to retrieve
 * @returns Promise of all the pinData
 */
export async function getPin(pinId: string): Promise<Pin | undefined> {
    const data: ServerPin | undefined = await request("pins/" + pinId, "GET")
        .then(function (response) {
            if (CheckGetResponse(response)) {
                const res = response.json();
                const data: Promise<ServerPin> = res;
                console.log("Got pin from server");
                return data;
            }
        })
    //Fix the data to comply with local pin type
    if (data) {
        return convertPin(data);
    }
    return data;
}

/**
 * Removes a pin from the server
 * @param pinId id of pin to remove
 */
export function removePin(pinId: string): Promise<"SUCCES" | undefined> {
    return request("pins/" + pinId, "DELETE")
        .then(function (response) {
            if (CheckDeleteResponse(response)) {
                console.log("Pin removed form server");
                return ("SUCCES");
            }
        })
}

type PinResponse = {
    id_str: string;
}

/**
 * Add a pin to the server
 * @param pin give the pin to add
 * @returns the id of the new pin or undefined if not succesfull
 */
export async function addPin(pin: Pin): Promise<string | undefined> {
    const body = {
        "title": pin.title,
        "lat": pin.lat,
        "lon": pin.lon,
        "text": pin.text
    }
    const data = await request("pins/", "POST", body)
        .then(function (response) {
            if (CheckPostResponse(response)) {
                const data: Promise<PinResponse> = response.json();
                console.log("Pin added to server")
                return data;
            }
        })
    if (data) {
        return data.id_str;
    }
    return undefined;
}

/**
 * Update an existing pin that's on the server
 * @param pin give the changed pin
 */
export function updatePin(pin: Pin): Promise<"SUCCES" | undefined> {
    const body = {
        "title": pin.title,
        "lat": pin.lat,
        "lon": pin.lon,
        "text": pin.text
    }
    return request("pins/" + pin.id, "PUT", body)
        .then(function (response) {
            if (CheckPutResponse(response)) {
                console.log("Pin Updated");
                return "SUCCES";
            }
        })
}

/**
 * Share pins with either a group or a class.
 * @param pinId id of the pin you want to share
 * @param shareWith either "group" or "class" depending on situation
 * @param id the id of either the group or class depending on situation
 */
export function sharePin(pinId: string, shareWith: "group" | "class", id: string) {
    const body = {
        "type": shareWith,
        "id": id
    }
    return request("pins/" + pinId + "/share", "POST", body)
        .then(function (response) {
            if (CheckPostResponse(response)) {
                return "SUCCES"
            }
        })
}