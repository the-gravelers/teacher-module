/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {getCookie} from './Cookie';

export type Method = "POST" | "GET" | "PUT" | "DELETE";

type Key = 'Content-Type' | 'Authorization';
export type Header = { key: Key, value: string };

export const JSONHeader: Header = {key: 'Content-Type', value: "application/json;charset=UTF-8"};

export type idHolder = {
    id_str: string
}

/**
 * General fetch request wrapper to make life easier
 * @param url which api to request as a string
 * @param method "POST" | "GET" | "PUT" | "DELETE"
 * @param body optional the body for the request
 * @param headers optional extra headers for the request
 * @param doStringify optional to stringify the body
 * @returns Promise<Response> just logs standard errors
 */
export async function request(url: string, method: Method, body?: any, headers?: Header[], doStringify: boolean = true): Promise<Response> {
    let accesstoken = getCookie("access_token");

    //Standard heading
    let headings: Headers = new Headers();
    headings.append('Authorization', "Bearer " + accesstoken);

    //Add extra headers if needed
    if (headers) {
        for (let i = 0; i < headers.length; i++) {
            headings.append(headers[i].key, headers[i].value)
        }
    }
    //Plain body
    let parsedBody = body;
    //If needs to stringify
    if (doStringify) {
        parsedBody = JSON.stringify(body);
    }
    //Execute the request and throw errors depending on response
    return await fetch("https://uce.science.uu.nl/api/" + url, {
        headers: headings,
        method: method,
        body: parsedBody
    });
}