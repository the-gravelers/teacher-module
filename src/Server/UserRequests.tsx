import {JSONHeader, request} from './Request';
import {CheckGetResponse, CheckPutResponse} from './ErrorHandling';
import { UserInfo } from '../Types';

/**
 * If possible gets the pins corresponding to the given class.
 * @param classId id corresponding to the class you want the pins from.
 * @returns A promise of a list of pins or empty array if request went wrong.
 */
 export async function getUserData() {
    const data: UserInfo | undefined = await request("users/info", "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                return response.json();
            }
        })
    return data;
}

export async function updateUserData(userId: string, classId : string) {
    const body = {
        class_id : classId
    }
    await request("users/" + userId, "PUT", body, [JSONHeader])
    .then(function (response) {
        if (CheckPutResponse(response)) {
            return response.json();
        }
    })
}