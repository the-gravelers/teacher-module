/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import { AuthLevel, AuthToken, UserInfo } from "../Types";
import { getCookie, setCookie } from './Cookie';
import { JSONHeader, request } from "./Request";
import { CheckPostResponse } from './ErrorHandling';


/**
 * Checks whetever or not the user is logged and if logged in but session expired refreshes it
 * @returns true if authorized user otherwise false.
 */
export async function IsAuthorized() {
    const refreshToken = getCookie("refresh_token");
    //You were logged in, but your session expired so we refresh it so you are logged in again
    if (refreshToken) {
        await refreshAuth(refreshToken);
    } else {
        return false;
    }
    return await checkTeacherPrivileges();
}

/**
 * request server for user info and checks if high enough auth_level to access website
 * @returns Promise<true | false> depending on auth_level
 */
export async function checkTeacherPrivileges() {
    const data: Response = await request("users/info", "GET", undefined, [JSONHeader]);
    if (data.status > 400) {
        return false;
    }
    const userInfo: UserInfo = await data.json();
    setCookie("user_id", userInfo.id_str, 0.25);

    return userInfo.auth_level > AuthLevel.Student;
}

/**
 * Refreshes user's authentication with the refresh token
 * @param refreshToken needed to get new access_token from server
 */
export async function refreshAuth(refreshToken: string) {
    const sessionId = getCookie("session_id");
    const body = {
        grant_type: "refresh_token",
        session_id: sessionId,
        refresh_token: refreshToken,
    }
    await request("auth/token", "POST", body, [JSONHeader])
        .then(function (response) {
            if (CheckPostResponse(response)) {
                const res: Promise<AuthToken> = response.json();
                res.then(data => {
                    setCookie("access_token", data.access_token, 0.25); //Access token last 6 hours
                    setCookie("refresh_token", data.refresh_token, 364); //Refresh token technically lasts forever but we store it a year
                    setCookie("session_id", data.session_id_str, 364); //Session id
                })
            }
        })
}