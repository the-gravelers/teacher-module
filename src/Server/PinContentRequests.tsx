/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {PinContent} from '../Types';
import {request} from './Request';
import {CheckDeleteResponse, CheckGetResponse, CheckPostResponse} from './ErrorHandling';

export type ServerPinContent = {
    id_str: string,
    content_type: string,
    content_nr: number,
    content: File,
}

export function convertContent(sPinContent: ServerPinContent, pinId: string) {
    const pinContent: PinContent = {
        content_nr: sPinContent.content_nr,
        content_type: sPinContent.content_type,
        pinId: pinId,
        content: sPinContent.content,
        contentId: sPinContent.id_str,
    }
    return pinContent;
}

async function blobToFile(theBlob: Promise<Blob>): Promise<File> {
    const temp = await theBlob;
    return new File([temp], "image");
}

/**
 * Gets the pinContent of a single pin from the server
 * @param pinId id of the pin you want to retrieve the content from
 * @param pinContentId id of the pinContent
 * @returns Promise of all the pinContentData
 */
export async function getPinContent(pinId: string, pinContentId: string): Promise<File | undefined> {
    return await request("pins/" + pinId + "/contents/" + pinContentId, "GET")
        .then(function (response) {
            if (CheckGetResponse(response)) {
                const blob: Promise<Blob> = response.blob();
                console.log("Got pin from server");
                return blobToFile(blob);
            }
        })
}

/**
 * Removes the pinContent of a pin from the server
 * @param pinContent the pinContent to remove
 */
export function removePinContent(pinId: string, contentId: string): Promise<"SUCCES" | undefined> {
    //const pin: ServerPinContent | undefined= request("pinContent/" + pinId, "GET")
    return request("pins/" + pinId + "/contents" + contentId, "DELETE")
        .then(function (response) {
            if (CheckDeleteResponse(response)) {
                console.log("PinContent from" + pinId + " removed form server");
                return ("SUCCES");
            }
        })
}

type PinResponse = {
    id_str: string;
}

/**
 * Add pinContent of a pin to the server
 * @param pinContent give the content to add
 * @returns the id of the new pinContent or undefined if not succesfull
 */
export async function addPinContent(pinContent: PinContent): Promise<string | undefined> {
    const formdata = new FormData();
    const pin_content = JSON.stringify({content_type: pinContent.content_type, content_nr: pinContent.content_nr});
    formdata.append("pin_content", pin_content);
    if (pinContent.content) {
        formdata.append("file", pinContent.content);
    }
    const body = formdata;

    const data = await request("pins/" + pinContent.pinId + "/contents", "POST", body, [], false)
        .then(function (response) {
            console.log(formdata.entries());
            if (CheckPostResponse(response)) {
                const data: Promise<PinResponse> = response.json();
                console.log("Pin content added to server")
                return data;
            }
        })
    if (data) {
        return data.id_str;
    } else {
        return undefined;
    }
}

// NOT USED ATM BUT COULD BE IN THE FUTURE
 /**
  * Update the existing content of a pin that's on the server
  * @param pinContent give the changed pinContent
  */
  /*export function updatePinContent(pinContent: PinContent): Promise<"SUCCES" | undefined> {
     const formdata = new FormData();
     const pin_content = JSON.stringify({content_type: pinContent.content_type, content_nr: pinContent.content_nr});
     formdata.append("pin_content", pin_content);
     if(pinContent.content) {
        formdata.append("file", pinContent.content);
     }
     const body = formdata;

     return request("pins/" + pinContent.pinId + "/contents/", "PUT", body, [], false)
         .then(function (response) {
             if (CheckPutResponse(response)) {
                console.log("PinContent Updated");
                 return "SUCCES";
             }
         })
 } */