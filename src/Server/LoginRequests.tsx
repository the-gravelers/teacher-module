import { SuccessToast, WarningToast } from "../Components/Toasts";
import { setCookie } from './Cookie';
import { CheckPostResponse } from './ErrorHandling';
import { AuthToken, RequestError } from "../Types";
import { JSONHeader, request } from "./Request";
import { checkTeacherPrivileges } from "./Authorization";
import { match } from "ts-pattern";

type Data =
    | { type: 'Error'; message: Promise<RequestError> }
    | { type: 'Token'; token: Promise<AuthToken> }

/**
 * Attempts to login with given credentials
 * @param email string email of the user
 * @param password string password of the user
 * @returns true if succesfully logged in else false.
 */
export async function login(email: string, password: string) {
    const body = {
        grant_type: "password",
        email: email,
        password: password
    }
    const data: Data | undefined = await request("auth/token", "POST", body, [JSONHeader])
        .then(function (response) {
            if (response.status === 400) {
                const res: Promise<RequestError> = response.json();
                return {type: 'Error', message:res} as Data;
            }
            else if (CheckPostResponse(response)) {
                SuccessToast('Logged in succesfully');
                return {type: 'Token', token: response.json()} as Data;
            }
        })
    if (data) {
        //Here we do pattern matching to determine the type we have returned.
        return match(data)
            .with({ type: 'Error' }, async (res) => {
                const message = await res.message;
                WarningToast(message.error);
                return false;
            })

            .with({ type: 'Token' }, async (res) => {
                const token = await res.token;
                setCookie("access_token", token.access_token, 0.25); //Access token last 6 hours
                setCookie("refresh_token", token.refresh_token, 364); //Refresh token technically lasts forever but we store it a year
                setCookie("session_id", token.session_id_str, 364); //Session id

                //So we can set user id for user pin requests
                await checkTeacherPrivileges()

                return true;
            })
            .exhaustive();
    }
    else {
        return false;
    }
}

/**
 * Sends a request to the server to reset your password
 * @param email the email you have forgotten the password from.
 */
export async function ForgotPassword(email: string) {
    const body = {
        email: email
    };
    await request('users/forgot-password', "POST", body, [JSONHeader])
        .then(function (response) {
            if (CheckPostResponse(response)) {
                SuccessToast('Email Send');
            }
        })
}