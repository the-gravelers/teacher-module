/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import { JSONHeader, request } from './Request';
import { CheckPostResponse } from './ErrorHandling';
import { SuccessToast, WarningToast } from "../Components/Toasts";
import { AuthLevel } from '../Types';

type RegisterInfo = {
    class_id: string,
    email: string,
    name: string,
    password: string,
    verify_password: string,
}

type RegisterMailInfo = {
    email: string,
    auth_level: AuthLevel,
    class_id: string,
    user_group_id: string,
}

type RegisterError = {
    errors: {
        email?: string[],
        name?: string[],
        password?: string[],
        verify_password?: string[],
    }
}
/**
 * Register a user and place it in a class.
 * @param userInfo the id of class to put the user in and corresponding userInfo
 */
/**
 * @param classId id corresponding to the class you want the pins from.
 * @returns A promise of a list of pins or empty array if request went wrong.
 */
export async function RegisterUser(userInfo: RegisterInfo) {
    const body = userInfo;
    const res: RegisterError | undefined = await request("users/register", "POST", body, [JSONHeader])
        .then(function (response) {
            if (CheckPostResponse(response)) {
                SuccessToast('Account Created');
            } else {
                const data: Promise<RegisterError> = response.json();
                return data;
            }
        })
    console.log(res);
    if (res) {
        if (res.errors.name) {
            WarningToast(res.errors.name[0]);
        }
        if (res.errors.email) {
            WarningToast(res.errors.email[0]);
        }
        if (res.errors.password) {
            WarningToast(res.errors.password[0]);
        }
        if (res.errors.verify_password) {
            WarningToast(res.errors.verify_password[0]);
        }

    }
}

export async function RegisterByMail(userInfo: RegisterMailInfo) {
    const body = userInfo;
    const res: RegisterError | undefined = await request("users/", "POST", body, [JSONHeader])
        .then(function (response) {
            if (CheckPostResponse(response)) {
                SuccessToast('User Created');
            } else {
                const data: Promise<RegisterError> = response.json();
                return data;
            }
        })
    console.log(res);
    if (res) {
        if (res.errors.name) {
            WarningToast(res.errors.name[0]);
        }
    }
}