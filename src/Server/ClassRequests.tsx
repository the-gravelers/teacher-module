/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import { idHolder, JSONHeader, request } from './Request';
import { Class, RequestError } from '../Types';
import { CheckDeleteResponse, CheckGetResponse, CheckPostResponse, CheckPutResponse } from './ErrorHandling';
import { ErrorToast } from '../Components/Toasts';

export type ServerClass = {
    class_id_str: string,
    course_id_str: string,
    name: string,
    pin_group_id_str: string
}

export function convertClass(sClass: ServerClass) {
    const fixedClass: Class = {
        id: sClass.class_id_str,
        name: sClass.name,
        courseId: sClass.course_id_str,
    }
    return fixedClass
}

/**
 * uploads a class to the server.
 * @param newClass the class you want to send to the server.
 * @returns the id of the new class as a string or undefined
 */
export async function postClass(newClass: Class): Promise<string | undefined> {
    const body = {
        name: newClass.name,
        course_id: newClass.courseId
    }
    const data: idHolder = await request("classes/", "POST", body)
        .then(function (response) {
            if (CheckPostResponse(response)) {
                return response.json();
            }
        })
    return data.id_str;
}

/**
 * gets all the classes corresponding to request course
 * @param courseId id of the course you want to retrieve classes from
 * @returns Promise of either an empty array or and array filled with classes
 */
export async function getClasses(courseId: string) {
    const data: ServerClass[] | undefined = await request("classes/?course_id=" + courseId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got classes from course")
                return response.json();
            }
        })
    if (data) {
        return data.map(x => convertClass(x));
    }
    return [];
}

/**
 * Gets a single Class depending on given id (Not Used ATM)
 * @param classId id of Class you want to retrieve data from
 * @returns single Class corresponding to the ID or undefined
 */
export async function getClass(classId: string): Promise<Class | undefined> {
    const data: ServerClass | undefined = await request("classes/" + classId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got requested class");
                return response.json();
            }
        })
    if (data) {
        return convertClass(data);
    }
}

/**
 * Updates the class on the server.(Not Used ATM)
 * @param changedClass the edited class.
 * @param courseId id of the course the class belongs to.
 * @returns "SUCCES" or undefined depending on if request succesfull.
 */
export function updateClass(changedClass: Class, courseId: string): Promise<"SUCCES" | undefined> {
    const body = {
        name: changedClass.name,
        course_id: courseId
    }
    return request("classes/" + changedClass.id, "PUT", body)
        .then(function (response) {
            if (CheckPutResponse(response)) {
                return "SUCCES"
            }
        })
}
/**
 * deletes a class from the server only possible if no users groups otherwise guarenteed fail
 * @param classId id of the class you want to delete.
 * @returns undefined or an error depending on if request succesfull.
 */
export async function deleteClass(classId: string) {
    const data = await request("classes/" + classId, "DELETE")
        .then(function (response) {
            if (response.status === 400) {
                const res: Promise<RequestError> = response.json();
                return res;
            }
            else if (CheckDeleteResponse(response)) {
                
            }
        })
    if (data) {
        ErrorToast(data.error);
    }
}