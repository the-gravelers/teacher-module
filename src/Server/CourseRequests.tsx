/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {Course, RequestError} from '../Types';
import {idHolder, JSONHeader, request} from './Request';
import {CheckDeleteResponse, CheckGetResponse, CheckPostResponse, CheckPutResponse} from './ErrorHandling';
import { ErrorToast } from '../Components/Toasts';

//TODO less hardcoded uniId
export const uniId = 100000;

export type ServerCourse = {
    course_id_str: string,
    university_id_str: string,
    name: string
}

export function convertCourse(sCourse: ServerCourse): Course {
    const course: Course = {
        id: sCourse.course_id_str,
        name: sCourse.name
    }
    return course;
}

/**
 * Gets all the courses corresponding to the user's University
 * @returns returns a list of courses if succesfull
 */
export async function getCourses(): Promise<Course[]> {
    const data: ServerCourse[] | undefined = await request("courses/?university_id=" + uniId, "GET")
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got courses");
                return response.json();
            }
        })
    if (data) {
        return data.map(x => convertCourse(x));
    }
    return [];
}

/**
 * Get a single course from the server (Not Used ATM)
 * @param courseId the id of the course you want
 * @returns the course if succesfull
 */
export async function getCourse(courseId: string): Promise<Course | undefined> {
    const data: ServerCourse | undefined = await request("courses/" + courseId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                console.log("Got single course");
                return response.json();
            }
        })
    if (data) {
        return convertCourse(data);
    }
}

/**
 * Uploads a course to the server
 * @param name name of the new course
 * @param uniId the id of the university to add the course to
 * @returns the new course with id or undefined
 */
export async function postCourse(name: string): Promise<Course | undefined> {
    const body = {
        "name": name,
        "university_id": uniId
    }
    const data: idHolder | undefined = await request("courses/", "POST", body)
        .then(function (response) {
            if (CheckPostResponse(response)) {
                console.log("Posted class");
                return response.json();
            }
        })
    if (data) {
        const newCourse: Course = {
            id: data.id_str,
            name: name
        }
        return newCourse;
    }
}

/**
 * Changes a course on the server
 * @param course the edited course
 * @returns "SUCCES" or undefined depending on the outcome.
 */
export async function updateCourse(course: Course) {
    const body = {
        "name": course.name,
        "university_id": uniId
    }
    return await request("courses/" + course.id, "PUT", body)
        .then(function (response) {
            if (CheckPutResponse(response)) {
                console.log("Updated class");
                return "SUCCES";
            }
        })
}
/**
 * Deletes a course from the server only succesfull if no classes in the course
 * @param courseId id of course to delete
 * @returns "SUCCES" or undefined depending on the outcome.
 */
export async function deleteCourse(courseId: string) {
    const data : RequestError | undefined = await request("courses/" + courseId, "DELETE")
        .then(function (response) {
            if(response.status === 400) {
                const res : Promise<RequestError> = response.json();
                return res;
            }
            else if (CheckDeleteResponse(response)) {

            }
        })
    if(data) {
        ErrorToast(data.error);
    }
}