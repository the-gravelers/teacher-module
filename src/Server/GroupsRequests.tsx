/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {Group, User} from '../Types';
import {JSONHeader, request} from './Request';
import {CheckDeleteResponse, CheckGetResponse, CheckPostResponse, CheckPutResponse} from './ErrorHandling';

type ServerUser = { user_id_str: string }

type dataString = { id_str: string }

export type ServerGroup = {
    class_id_str: string,
    max_user_count: number,
    members: ServerUser[],
    name: string,
    pin_group_id_str: string,
    user_group_id_str: string
}

/**
 * Converts a server group to a local version of group
 * @param serverGroup a group from a server response
 * @returns a Group type which we use locally.
 */
export function convertGroup(serverGroup: ServerGroup): Group {
    const members: string[] = []
    for (let j = 0; j < serverGroup.members.length; j++) {
        members.push(serverGroup.members[j].user_id_str)
    }
    return {
        class_id: serverGroup.class_id_str,
        id: serverGroup.user_group_id_str,
        maxMembers: serverGroup.max_user_count,
        members: members,
        name: serverGroup.name
    }
}

/**
 * Send new Group to the server
 * @param group the new group to add to the server
 * @returns the id of the new user-group
 */
export async function postGroup(group: Group) : Promise<string | undefined> {
    const body = {
        name: group.name,
        class_id: group.class_id,
        max_user_count: group.maxMembers
    }
    const data : dataString | undefined = await request("user-groups/", "POST", body, [JSONHeader])
        .then(function (response) {
            if (CheckPostResponse(response)) {
                return response.json();
            }
        })
    if(data) {
        return data.id_str
    }
}

/**
 * Gets all the user-groups corresponding to the given classId
 * @param classId id of the class you want the groups form
 * @returns list of groups or an empty list.
 */
export async function getGroups(classId: string): Promise<Group[]> {
    const groups: Group[] = [];
    const data: ServerGroup[] = await request("user-groups/?class_id=" + classId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                const res = response.json();
                console.log("Got groups")
                return res;
            }
        })
    if (data) {
        for (let i = 0; i < data.length; i++) {
            groups.push(convertGroup(data[i]));
        }
    }
    return groups
}

/**
 * Request a single group based on Id (Not used ATM)
 * @param groupId the group you want from server
 * @returns the group if exist on server or undefined
 */
export async function getGroup(groupId: string): Promise<Group | undefined> {
    const data: ServerGroup = await request("user-groups/" + groupId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                return response.json();
            }
        })
    if (data) {
        return convertGroup(data);
    }
}
/**
 * Gets the users and their names and id's from a certain group.
 * @param groupId the id of the group you want the users from.
 * @returns a list of users or undefined if something went wrong.
 */
export async function getUsersFromGroup(groupId: string) : Promise<User[] | undefined> {
    const data : User[] | undefined = await request("users/?group=" + groupId, "GET", undefined, [JSONHeader])
        .then(function (response) {
            if (CheckGetResponse(response)) {
                return response.json();
            }
        })
    return data;
}

/**
 * Changes a group that currently exist on the server (Not used ATM)
 * @param group the edited group to send to the server
 * @returns "SUCCES" | undefined depending on outcome of request
 */
export async function updateGroup(group: Group) {
    const body = {
        name: group.name,
        class_id: group.class_id,
        max_user_count: group.maxMembers
    }
    return request("user-groups/" + group.id, "PUT", body, [JSONHeader])
        .then(function (response) {
            if (CheckPutResponse(response)) {
                console.log("Group updated")
                return "SUCCES"
            }
        })
}

/**
 * Deletes a group form the server corresponding to given Id (Not used ATM)
 * @param groupId the group you want to remove from the server
 * @returns "SUCCES" | undefined depending on outcome of request
 */
export async function deleteGroup(groupId: string) {
    return request("user-groups/" + groupId, "DELETE")
        .then(function (response) {
            if (CheckDeleteResponse(response)) {
                console.log("Group deleted")
                return "SUCCES"
            }
        })
}

export async function kickMember(userId: string) {
    return request("user-groups/kick?user_id=" + userId, "POST")
        .then(function (response) {
            if (CheckPostResponse(response)) {
                console.log("Member Kicked")
                return "SUCCES"
            }
        })
}