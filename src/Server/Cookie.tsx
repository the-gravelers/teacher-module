/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/**
 * @param cname the name/key of the new cookie
 * @param cvalue the value of the new cookie
 * @param exdays the amount of days the cookie should exist
 */
export function setCookie(ckey: string, cvalue: string, exdays: number) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = ckey + "=" + cvalue + ";" + expires + ";path=/";
}


/**
 * @param key you give the key of the cookie which you have set in setCookie
 * @returns the corresponding value of the cookie if it exist otherwise returns undefined
 */
export function getCookie(key: string): string | undefined {
    let cookies = document.cookie.split("; ")
    for (let i = 0; i < cookies.length; i++) {
        let splittedCookie = cookies[i].split("=");
        if (splittedCookie[0] === key) {
            return splittedCookie[1]; //Value of the cookie
        }
    }
}