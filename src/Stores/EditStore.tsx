/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import create from 'zustand';

type editCourseManager = {
    courseId?: string;
    name?: string;
    setEditCourse: (courseId?: string, name?: string) => void;
}
/**
 * Shareable state of the course to edit for the CourseManager set in Courses
 */
export const editStore = create<editCourseManager>(set => ({
    setEditCourse: (courseId?: string, name?: string) => set(() => ({courseId: courseId, name: name}))
}))