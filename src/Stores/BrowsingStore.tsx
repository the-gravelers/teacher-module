/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import create from 'zustand';

type BrowsingStateManager = {
    activeClass?: string
    setActiveClass: (indexClass: string) => void;
    activeCourse?: string;
    setActiveCourse: (indexCourse: string) => void;
    amountOfGroups: number;
    setAmountOfGroups: (amount: number) => void;
}
/**
 * Shareable state of some browsing info which may be needed in other pages.
 */
export const browsingStore = create<BrowsingStateManager>(set => ({
    activeClass: undefined,
    setActiveClass: (indexClass) => set(state => ({activeClass: indexClass})),
    activeCourse: undefined,
    setActiveCourse: (indexCourse) => set(state => ({activeCourse: indexCourse})),
    amountOfGroups: -1,
    setAmountOfGroups: (amount) => set(state => ({amountOfGroups: amount})),
}))