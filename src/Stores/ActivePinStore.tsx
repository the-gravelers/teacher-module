/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import create from 'zustand';
import {Pin} from '../Types';
import {emptyPin} from '../Components/EmptyPin'

type ActivePin = {
    activePin: Pin;
    setActivePin: (pin: Pin) => void
}
/**
 * Shareable state of which pin to edit
 */
export const activePinStore = create<ActivePin>(set => ({
    activePin: emptyPin(),
    setActivePin: (pin) => set(state => ({activePin: pin}))
}))