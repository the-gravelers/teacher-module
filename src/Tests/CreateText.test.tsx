/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {render} from '@testing-library/react';
import {BrowserRouter as Router} from 'react-router-dom';
import {CreateText} from '../Components/CreateText';


test('does CreateText render correctly', () => {
    const {rerender, getByTestId} = render(<Router><CreateText pinText="" setPinText={(e) => null}/></Router>);

    const text = getByTestId("text") as HTMLInputElement;
    expect(text.value).toBe("");

    rerender(<Router><CreateText pinText="aba" setPinText={(e) => null}/></Router>);
    const newtext = getByTestId("text") as HTMLInputElement;
    expect(newtext.value).toBe("aba");
});