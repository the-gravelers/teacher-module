/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {fireEvent, render} from '@testing-library/react';
import {rendWrapper} from './renderWrapper';
import GroupCreator from '../Pages/GroupCreator';
import userEvent from '@testing-library/user-event';
import React from 'react';
import {Route} from 'react-router-dom';
import {ClassOverview} from '../Pages/ClassOverview';
import {nockGetClassPins} from './mocks/PinRequestMocks';
import {nockGetGroups} from './mocks/GroupRequestMocks';

test('test input fields', () => {
    //Render the GroupCreator Page
    const {getByTestId} = render(rendWrapper(<GroupCreator/>));

    ///Find the field to set the maximum members for the new group and type in it.
    const maxMembersField = getByTestId('maxMembers') as HTMLInputElement;
    expect(maxMembersField.value).toBe("6");
    userEvent.type(maxMembersField, '1');
    expect(maxMembersField.value).toBe("61");
})

test('add driver and edit new driver field', () => {
    //Render the GroupCreator Page
    const {getByTestId, getByRole} = render(rendWrapper(<GroupCreator/>));

    //Find Add Driver button and press it
    const addButton = getByRole('button', {name: 'Add Driver'});
    fireEvent.click(addButton);

    //Change the new Driver field
    const driverField = getByTestId('driverField') as HTMLInputElement;
    expect(driverField.value).toBe("");
    userEvent.type(driverField, 'a');
    expect(driverField.value).toBe("a");

    //Remove the new Driver field.
    const removeButton = getByRole('img', {name: 'remove button'});
    fireEvent.click(removeButton);

})

test('Submitting the new group', async () => {
    //Dummy data
    const classId = "1";
    const groupName = 'Group 1';

    //Render the GroupCreator Page with a route to the ClassOverview page.
    const {getByRole, getByText, getByTestId} = render(rendWrapper(<GroupCreator/>,
        <Route path="/Class/:classId" component={ClassOverview}/>
    ));

    const maxMembersField = getByTestId('maxMembers') as HTMLInputElement;

    //Mocks server request for ClassOverview page
    nockGetClassPins(undefined, []);
    nockGetGroups(undefined, [{
        class_id_str: classId,
        max_user_count: parseInt(maxMembersField.value),
        members: [],
        name: groupName,
        pin_group_id_str: '1',
        user_group_id_str: '1',
    }]);

    //Find the submit button and press it
    const submitButton = getByRole('link', {name: 'Confirm'});
    fireEvent.click(submitButton);

    //Make sure we are actually on the ClassOverview page now.
    expect(getByText('Pin list', {exact: false})).toBeInTheDocument();
    expect(getByText('Groups', {exact: false})).toBeInTheDocument();
    //TODO Fix That it actually waits for data
    //expect(getByText(groupName)).toBeInTheDocument();

})