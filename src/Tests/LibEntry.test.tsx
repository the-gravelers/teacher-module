import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import LibEntry from '../Pages/LibBook';

test("render Library Entry", () => {
    render(<BrowserRouter><LibEntry/></BrowserRouter>);
    
    //Expect title
    expect(screen.getByRole('entry')).toBeInTheDocument();
    expect(screen.getByRole('entryTitle')).toBeInTheDocument();

    //expect sections
    var sections = screen.getAllByRole('section');
    sections.map((s) => expect(s).toBeInTheDocument());

    //expect text & images content
    var text = screen.getAllByRole('text');
    var images = screen.getAllByRole('image');
    text.map((t) => expect(t).toBeInTheDocument());
    images.map((i) => expect(i).toBeInTheDocument());
});