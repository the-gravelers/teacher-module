import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import LibEntryEditor from '../Pages/LibBookEditor';
import { BrowserRouter } from 'react-router-dom';

/**
 * Adds new sections to rendered LibEntryEditor
 * @param sections number of sections to add
 */
function addLibrarySection() {
    var addSectionButton = screen.getByRole('addSectionButton');
    fireEvent.click(addSectionButton);
}

/**
 * Adds new textComponents to specific section in rendered LibEntryEditor
 * @param sectionIndex index of section to amend
 * @param texts number of texts to add to section
 */
function addText() {
    var sectionAddTextButtons = screen.queryAllByRole('sectionAddTextButton');
    fireEvent.click(sectionAddTextButtons[0]);
}

/**
 * Adds new imageComponents to specific section in rendered LibEntryEditor
 * @param sectionIndex index of section to amend
 * @param images number of images to add to section
 */
 function addImage() {
        var sectionAddTextButtons = screen.queryAllByRole('sectionAddImageButton');
        fireEvent.click(sectionAddTextButtons[0]);
}

test("render Library Entry Editor", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);

    //MAIN
    expect(screen.queryByRole('entryEdit')).toBeInTheDocument();

    //expect title
    expect(screen.queryByRole('entryEditTitle')).toBeInTheDocument();
    expect(screen.queryByRole('entryEditTitleLabel')).toBeInTheDocument();
    expect(screen.queryByRole('entryEditTitleInput')).toBeInTheDocument();

    //expect buttons
    expect(screen.queryByRole('addSectionButton')).toBeInTheDocument();
    expect(screen.queryByRole('submitButton')).toBeInTheDocument();
});

test("update entry title", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);

    var titleEdit = screen.getByRole('entryEditTitleInput');

    var titleTestString = "Entry Title Test String";

    fireEvent.change(titleEdit, { target: { value: titleTestString } });

    expect(screen.queryByText(titleTestString)).toBeInTheDocument();
});

test("click submit button", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);

    var submitButton = screen.getByRole('submitButton');
    fireEvent.click(submitButton);
});

//SECTION MANIPULATION
test("add library section", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);

    var sectionsOld = screen.queryAllByRole('sectionEdit');

    addLibrarySection();

    var sectionsNew = screen.queryAllByRole('sectionEdit');

    expect(sectionsNew.length).toEqual(sectionsOld.length+1);
});

test("update first section title", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to edit
    addLibrarySection();

    var sectionTitleEdit = screen.getByRole('sectionEditTitleInput');

    var titleTestString = "Section Title Test String";

    fireEvent.change(sectionTitleEdit, { target: { value: titleTestString } });
    
    var titleInstances = screen.getByText('˄ '+titleTestString, { exact:false });

    expect(titleInstances).toBeInTheDocument();
});

test("remove first library section", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to remove
    addLibrarySection();
    addLibrarySection();

    var sectionsOld = screen.queryAllByRole('sectionEdit');

    var sectionRemoveButtons = screen.queryAllByRole('sectionRemoveButton');
    fireEvent.click(sectionRemoveButtons[0]);

    var sectionsNew = screen.queryAllByRole('sectionEdit');

    expect(sectionsOld.length).toEqual(sectionsNew.length+1);
});

test("move first section down", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 2 sections to move
    addLibrarySection();
    addLibrarySection();
    
    var sectionsOld = screen.queryAllByRole('sectionEdit');

    var moveDownButtons = screen.queryAllByRole('sectionMoveDownButton');
    fireEvent.click(moveDownButtons[0]);

    var sectionsNew = screen.queryAllByRole('sectionEdit');

    //number of sections didn't change
    expect(sectionsNew.length===sectionsOld.length).toBe(true);

    //top sections are now switched
    expect(sectionsOld[0]).toEqual(sectionsNew[1]);
    expect(sectionsOld[1]).toEqual(sectionsNew[0]);

    //other sections remain the same
    for(var i=2; i<sectionsOld.length; i++) {
        expect(sectionsOld[i]).toEqual(sectionsNew[i]);
    };
});

test("move second section up", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 2 sections to move
    addLibrarySection();
    addLibrarySection();
    
    var sectionsOld = screen.queryAllByRole('sectionEdit');

    var moveUpButtons = screen.queryAllByRole('sectionMoveUpButton');
    fireEvent.click(moveUpButtons[1]);

    var sectionsNew = screen.queryAllByRole('sectionEdit');

    //number of sections didn't change
    expect(sectionsNew.length).toEqual(sectionsOld.length);

    //top sections are now switched
    expect(sectionsOld[0]).toEqual(sectionsNew[1]);
    expect(sectionsOld[1]).toEqual(sectionsNew[0]);

    //other sections remain the same
    for(var i=2; i<sectionsOld.length; i++) {
        expect(sectionsOld[i]).toEqual(sectionsNew[i]);
    };
});

test("try move up first section, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 2 sections to move
    addLibrarySection();
    addLibrarySection();
    
    var entryOld = screen.queryAllByRole('entryEdit');

    var moveUpButtons = screen.queryAllByRole('sectionMoveUpButton');
    fireEvent.click(moveUpButtons[0]);

    var entryNew = screen.queryAllByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

test("try move down last section, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 2 sections to move
    addLibrarySection();
    addLibrarySection();
    
    var sectionsOld = screen.queryAllByRole('sectionEdit');
    var sectionsOnPage = sectionsOld.length;

    var entryOld = screen.queryAllByRole('entryEdit');

    var moveDownButtons = screen.queryAllByRole('sectionMoveDownButton');
    fireEvent.click(moveDownButtons[sectionsOnPage-1]);

    var entryNew = screen.queryAllByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

//IMAGE MANIPULATION
test("add image to first section", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    
    var imagesOld = screen.queryAllByRole('imageEdit');
    
    addImage();
    
    var imagesNew = screen.queryAllByRole('imageEdit');
    
    //number of images increased by 1
    expect(imagesNew.length).toEqual(imagesOld.length+1);
});

test("Remove first image", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 1 image to remove
    addImage();
    
    var imagesOld = screen.queryAllByRole('imageEdit');

    var removeImageButton = screen.getByRole('imageRemoveButton');
    fireEvent.click(removeImageButton);

    var imagesNew = screen.queryAllByRole('imageEdit');

    //number of images decreased by 1
    expect(imagesNew.length).toEqual(imagesOld.length-1);
});

test("move down first image", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 images to move
    addImage();
    addImage();

    var imagesOld = screen.queryAllByRole('imageEdit');

    var imageMoveDownButtons = screen.queryAllByRole('imageMoveDownButton');
    fireEvent.click(imageMoveDownButtons[0]); 

    var imagesNew = screen.queryAllByRole('imageEdit');

    //number of images didn't change
    expect(imagesNew.length===imagesOld.length).toBe(true);

    //top images are now switched
    expect(imagesOld[0]).toEqual(imagesNew[1]);
    expect(imagesOld[1]).toEqual(imagesNew[0]);

    //other images remain the same
    for(var i=2; i<imagesOld.length; i++) {
        expect(imagesOld[i]).toEqual(imagesNew[i]);
    };
});

test("move up second image", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 images to move
    addImage();
    addImage();

    var imagesOld = screen.queryAllByRole('imageEdit');

    var imageMoveUpButtons = screen.queryAllByRole('imageMoveUpButton');
    fireEvent.click(imageMoveUpButtons[1]); 

    var imagesNew = screen.queryAllByRole('imageEdit');

    //number of images didn't change
    expect(imagesNew.length).toEqual(imagesOld.length);

    //top images are now switched
    expect(imagesOld[0]).toEqual(imagesNew[1]);
    expect(imagesOld[1]).toEqual(imagesNew[0]);

    //other images remain the same
    for(var i=2; i<imagesOld.length; i++) {
        expect(imagesOld[i]).toEqual(imagesNew[i]);
    };
});

test("try move up first image, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 images to move
    addImage();
    addImage();

    var entryOld = screen.queryAllByRole('entryEdit');

    var imageMoveUpButtons = screen.queryAllByRole('imageMoveUpButton');
    fireEvent.click(imageMoveUpButtons[0]); 

    var entryNew = screen.queryAllByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

test("try move down last image, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 images to move
    addImage();
    addImage();

    var imagesOld = screen.queryAllByRole('imageEdit');
    var imagesOnPage = imagesOld.length;

    var entryOld = screen.queryAllByRole('entryEdit');

    var imageMoveDownButtons = screen.queryAllByRole('imageMoveDownButton');
    fireEvent.click(imageMoveDownButtons[imagesOnPage-1]); 

    var entryNew = screen.queryAllByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

//test("change first image", () => {
//    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
//    //add at least 1 section to add image to
//    addLibrarySection();
//    //add at least 1 image to change
//    addImage();
//    
//    var imageInputs = screen.queryAllByRole('imageEditInput');
//    
//    const file = new File(['PlaceHolder'], 'PlaceHolder.png', { type: 'image/png' });
//    
//    fireEvent.change(imageInputs[0], file);
//});

test("change first image unsuccesfully", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 1 image to change
    addImage();
    
    var imageInputs = screen.queryAllByRole('imageEditInput');

    var entryOld = screen.getByRole('entryEdit');
    
    fireEvent.change(imageInputs[0], {target: {files: null}});

    var entryNew = screen.getByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

//TEXT MANIPULATION
test("add text to first section", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add text to
    addLibrarySection();
    
    var textsOld = screen.queryAllByRole('textEdit');
    var sectionsOld = screen.queryAllByRole('sectionEdit');

    addText();

    var textsNew = screen.queryAllByRole('textEdit');
    var sectionsNew = screen.queryAllByRole('sectionEdit');

    //number of texts increased by 1
    expect(textsNew.length===textsOld.length+1).toBe(true);

    //top section is changed
    expect(sectionsOld[0]).toEqual(sectionsNew[0]);

    //other sections remain the same
    for(var i=1; i<textsOld.length; i++) {
        expect(sectionsOld[i]).toEqual(sectionsNew[i]);
    };
});

test("Remove first text", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 1 text to remove
    addText();
    
    var textsOld = screen.queryAllByRole('textEdit');

    var removeImageButtons = screen.queryAllByRole('textRemoveButton');
    fireEvent.click(removeImageButtons[0]);

    var textsNew = screen.queryAllByRole('textEdit');

    //number of images decreased by 1
    expect(textsNew.length).toEqual(textsOld.length-1);

    //all subsequent images remain the same
    for(var i=0; i<textsNew.length; i++) {
        expect(textsOld[i+1]).toEqual(textsNew[i]);
    };
});

test("move down first text", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 texts to move
    addText();
    addText();

    var textsOld = screen.queryAllByRole('textEdit');

    var textMoveDownButtons = screen.queryAllByRole('textMoveDownButton');
    fireEvent.click(textMoveDownButtons[0]); 

    var textsNew = screen.queryAllByRole('textEdit');

    //number of images didn't change
    expect(textsNew.length).toEqual(textsOld.length);

    //top texts are now switched
    expect(textsOld[0]).toEqual(textsNew[1]);
    expect(textsOld[1]).toEqual(textsNew[0]);

    //other texts remain the same
    for(var i=2; i<textsOld.length; i++) {
        expect(textsOld[i]).toEqual(textsNew[i]);
    };
});

test("move up second text", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 texts to move
    addText();
    addText();

    var textsOld = screen.queryAllByRole('textEdit');

    var textMoveUpButtons = screen.queryAllByRole('textMoveUpButton');
    fireEvent.click(textMoveUpButtons[1]); 

    var textsNew = screen.queryAllByRole('textEdit');

    //number of texts didn't change
    expect(textsNew.length).toEqual(textsOld.length);

    //top texts are now switched
    expect(textsOld[0]).toEqual(textsNew[1]);
    expect(textsOld[1]).toEqual(textsNew[0]);

    //other texts remain the same
    for(var i=2; i<textsOld.length; i++) {
        expect(textsOld[i]).toEqual(textsNew[i]);
    };
});

test("try move up first text, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 texts to move
    addText();
    addText();

    var entryOld = screen.getByRole('entryEdit');

    var textMoveUpButtons = screen.queryAllByRole('textMoveUpButton');
    fireEvent.click(textMoveUpButtons[0]); 

    var entryNew = screen.getByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

test("try move down last text, fails", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 2 texts to move
    addText();
    addText();

    var textsOld = screen.queryAllByRole('textEdit');
    var entryOld = screen.getByRole('entryEdit');

    var textMoveDownButtons = screen.queryAllByRole('textMoveDownButton');
    var textsOnPage = textsOld.length;
    fireEvent.click(textMoveDownButtons[textsOnPage-1]); 

    var entryNew = screen.getByRole('entryEdit');

    //nothing changes
    expect(entryOld).toEqual(entryNew);
});

test("Change first textarea", () => {
    render(<BrowserRouter><LibEntryEditor/></BrowserRouter>);
    //add at least 1 section to add image to
    addLibrarySection();
    //add at least 1 text to change
    addText();

    var testString = 'New Test Text';

    var textsOld = screen.queryAllByRole('textArea');
    
    fireEvent.change(textsOld[0], { target: { value: testString } });

    expect(screen.queryByDisplayValue(testString)).toBeInTheDocument();
});