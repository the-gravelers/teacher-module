/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {fireEvent, render} from '@testing-library/react';
import {rendWrapper} from './renderWrapper';
import userEvent from '@testing-library/user-event';
import {CourseManager} from '../Pages/CourseManager';
import {Route} from 'react-router-dom';
import Courses from '../Pages/Courses';
import {nockGetCourses, nockPostCourse, nockUpdateCourse} from './mocks/CourseRequestMocks';
import {ServerCourse, uniId} from '../Server/CourseRequests';
import {nockGetClasses} from './mocks/ClassRequestMocks';

//DUMMY DATA
const courseGeo: ServerCourse = {
    course_id_str: "1",
    university_id_str: uniId.toString(),
    name: "GEO3-1210"
}
//END OF DUMMY DATA

test('Can you to in input fields', () => {
    const {getByTestId, getByText} = render(rendWrapper(<CourseManager/>));

    //Check that we are in course creator screen
    getByText('Course Creator');

    //Find courseName input field and fake type in it.
    const nameField = getByTestId("courseName") as HTMLInputElement;
    expect(nameField.value).toBe("");
    userEvent.type(nameField, 'a');
    expect(nameField.value).toBe("a");
})

test('Try to create new course with no name', async () => {
    const {getByRole, getByText} = render(rendWrapper(<CourseManager/>));

    //Check that we are in course creator screen
    getByText('Course Creator');

    //Submit the new course
    const submitButton = getByRole('link', {name: "Create Course"});
    fireEvent.click(submitButton);

    //Check that we are in course creator screen
    getByText('Course Creator');
})

test('Create new course with filled in name', async () => {
    const {getByTestId, getByText, getByRole} = render(rendWrapper(<CourseManager/>,
        <Route path="/Courses" component={Courses}/>
    ));

    //Check that we are in course creator screen
    getByText('Course Creator');

    //Find courseName input field and fake type in it.
    const nameField = getByTestId("courseName") as HTMLInputElement;
    expect(nameField.value).toBe("");
    userEvent.type(nameField, courseGeo.name);
    expect(nameField.value).toBe(courseGeo.name);

    //Fake server requests
    nockPostCourse();
    nockGetCourses([courseGeo]);
    nockGetClasses(courseGeo.course_id_str, []);

    //Submit the new course with the given name 
    const submitButton = getByRole('link', {name: "Create Course"});
    userEvent.click(submitButton);

    getByText('Course List', {exact: false});
    //TODO Fix That it actually waits for data
    // getByText(courseGeo.name);
})

test('Changing an existing course', async () => {
    const {getByTestId, getByText, getByRole} = render(rendWrapper(
        <CourseManager courseId={courseGeo.course_id_str} name={courseGeo.name}/>,
        <Route path="/Courses" component={Courses}/>
    ));

    //Check that we are in course creator screen
    getByText('Course Creator');

    //Find courseName input field and fake type in it.
    const nameField = getByTestId("courseName") as HTMLInputElement;
    expect(nameField.value).toBe(courseGeo.name);
    userEvent.type(nameField, 'a');
    expect(nameField.value).toBe(courseGeo.name + "a");

    //Fake server requests
    nockUpdateCourse(courseGeo.course_id_str);
    nockGetCourses([{
        course_id_str: courseGeo.course_id_str,
        university_id_str: courseGeo.university_id_str,
        name: courseGeo.name + "a"
    }]);
    nockGetClasses(courseGeo.course_id_str, []);

    //Submit the changes made to the course 
    const submitButton = getByRole('link', {name: "Update Course"});
    userEvent.click(submitButton);

    getByText('Course List', {exact: false})
    //TODO Fix That it actually waits for data
    //expect(getByText(courseGeo.name + 'a')).toBeInTheDocument();
})