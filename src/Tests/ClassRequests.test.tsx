/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {convertClass, getClass, ServerClass, updateClass} from "../Server/ClassRequests"
import {nockGetClass, nockUpdateClass} from "./mocks/ClassRequestMocks";

//DUMMY DATA
const classId = "1";

const class2021Geo: ServerClass = {
    class_id_str: "1",
    course_id_str: "1",
    name: "Class of 2021/22",
    pin_group_id_str: "1",
}
//END OF DUMMY DATA

test('Get individual class request', async () => {
    nockGetClass(classId, class2021Geo)
    const serverresponse = await getClass(classId);
    expect(serverresponse).toStrictEqual(convertClass(class2021Geo))
})

test('Update a class', async () => {
    nockUpdateClass(classId);
    const serverresponse = await updateClass(convertClass(class2021Geo), class2021Geo.course_id_str);
    expect(serverresponse).toBe("SUCCES")
})