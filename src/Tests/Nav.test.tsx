/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import NavBar from '../Components/NavBar';

test('Check if links present in NavBar', () => {
    const comp = renderer.create(<Router><NavBar/></Router>);
    let tree = comp.toJSON();
    expect(tree).toMatchSnapshot();
});