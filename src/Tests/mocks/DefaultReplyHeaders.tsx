/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

export const defaultReplyHeaders = {
    'access-control-allow-origin': '*',
    'access-control-allow-headers': 'Authorization',
    'access-control-allow-credentials': 'true'
}