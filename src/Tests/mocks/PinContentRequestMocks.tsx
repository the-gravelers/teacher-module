/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import nock from 'nock';
import {ServerPin, ServerPinPreview} from '../../Server/PinRequests';
import {defaultReplyHeaders} from './DefaultReplyHeaders';

function nockGetPinContentOptions(pinId: string | undefined, contentId: string | undefined) {
  nock('https://uce.science.uu.nl')
      .defaultReplyHeaders(defaultReplyHeaders)
      .options('/api/pins/' + pinId + "/contents/" + contentId)
      .reply(204, 'No Content')
}

export function nockGetPinContent(pinId: string | undefined, contentId: string | undefined, reply: File[]) {
  nockGetPinContentOptions(pinId, contentId);
  nock('https://uce.science.uu.nl')
      .defaultReplyHeaders(defaultReplyHeaders)
      .get('/api/pins/' + pinId + "/contents/" + contentId)
      .reply(200, reply);
}