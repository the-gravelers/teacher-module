/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {defaultReplyHeaders} from './DefaultReplyHeaders';
import nock from 'nock';
import {ServerClass} from '../../Server/ClassRequests';

function nockPostClassOptions() {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/classes/')
        .reply(204, 'No Content')
}

export function nockPostClass() {
    nockPostClassOptions();
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .post('/api/classes/')
        .reply(201, {"id": 123, "id_str": "123"})
}

function nockGetClassesOptions(courseId: string) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options("/api/classes/?course_id=" + courseId)
        .reply(204, 'No Content')
}

export function nockGetClasses(courseId: string, reply: ServerClass[]) {
    nockGetClassesOptions(courseId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get("/api/classes/?course_id=" + courseId)
        .reply(200, reply);
}

function nockClassOptions(classId: string) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options("/api/classes/" + classId)
        .reply(204, 'No Content')
}

export function nockRemoveClass(classId: string) {
    nockClassOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .delete("/api/classes/" + classId)
        .reply(200);
}

export function nockGetClass(classId: string, reply: ServerClass) {
    nockClassOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get("/api/classes/" + classId)
        .reply(200, reply);
}

export function nockUpdateClass(classId: string) {
    nockClassOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .put("/api/classes/" + classId)
        .reply(200);
}