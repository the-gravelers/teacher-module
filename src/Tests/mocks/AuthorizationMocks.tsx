/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import nock from 'nock'
import {AuthToken, UserInfo} from '../../Types';
import {defaultReplyHeaders} from './DefaultReplyHeaders';

function nockLoginOptions() {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/auth/token')
        .reply(204, 'No Content')
}

export function nockLogin(reply: AuthToken) {
    nockLoginOptions();
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .post('/api/auth/token')
        .reply(200, reply)
}

function nockUserInfoOptions() {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/users/info')
        .reply(204, 'No Content')
}

export function nockUserInfo(reply: UserInfo) {
    nockUserInfoOptions();
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/users/info')
        .reply(200, reply)
}