/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import nock from "nock";
import {ServerCourse, uniId} from "../../Server/CourseRequests";
import {defaultReplyHeaders} from './DefaultReplyHeaders';

function nockGetCoursesOtions() {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options("/api/courses/?university_id=" + uniId)
        .reply(204, 'No Content')
}

export function nockGetCourses(reply: ServerCourse[]) {
    nockGetCoursesOtions();
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get("/api/courses/?university_id=" + uniId)
        .reply(200, reply);
}

function nockPostCourseOptions() {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options("/api/courses/")
        .reply(204, 'No Content')
}

export function nockPostCourse() {
    nockPostCourseOptions();
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .post("/api/courses/")
        .reply(201, {id_str: "1"});
}

function nockCourseOptions(courseId: string) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options("/api/courses/" + courseId)
        .reply(204, 'No Content')
}

export function nockUpdateCourse(courseId: string) {
    nockCourseOptions(courseId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .put("/api/courses/" + courseId)
        .reply(200)
}

export function nockGetCourse(courseId: string, reply: ServerCourse) {
    nockCourseOptions(courseId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get("/api/courses/" + courseId)
        .reply(200, reply)
}

export function nockRemoveCourse(courseId: string) {
    nockCourseOptions(courseId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .delete("/api/courses/" + courseId)
        .reply(200)
}