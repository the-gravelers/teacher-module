/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import nock from 'nock'
import {ServerGroup} from '../../Server/GroupsRequests';
import { User } from '../../Types';
import {defaultReplyHeaders} from './DefaultReplyHeaders';

function nockGetGroupsOptions(classId: string | undefined) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/user-groups/?class_id=' + classId)
        .reply(204, 'No Content')

}

export function nockGetGroups(classId: string | undefined, reply: ServerGroup[]) {
    nockGetGroupsOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/user-groups/?class_id=' + classId)
        .reply(200, reply);
}

function nockGetUsersFromGroupOptions(groupId : string) {
    nock('https://uce.science.uu.nl')
    .defaultReplyHeaders(defaultReplyHeaders)
    .options('/api/users/?group=' + groupId)
    .reply(204, 'No Content')
}

export function nockGetUsersFromGroup(groupId : string, reply: User[]) {
    nockGetUsersFromGroupOptions(groupId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/users/?group=' + groupId)
        .reply(200, reply);
}