/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import nock from 'nock';
import {ServerPin, ServerPinPreview} from '../../Server/PinRequests';
import {defaultReplyHeaders} from './DefaultReplyHeaders';

function nockGetClassPinsOptions(classId: string | undefined) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/pins/?class=' + classId)
        .reply(204, 'No Content')
}

export function nockGetClassPins(classId: string | undefined, reply: ServerPinPreview[]) {
    nockGetClassPinsOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/pins/?class=' + classId)
        .reply(200, reply);
}

function nockGetUserPinsOptions(userId: string) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/pins/?user=' + userId)
        .reply(204, 'No Content')
}

export function nockGetUserPins(userId: string, reply: ServerPinPreview[]) {
    nockGetUserPinsOptions(userId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/pins/?user=' + userId)
        .reply(200, reply);
}

function nockGetAllClassPinsOptions(classId: string | undefined) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/pins/?class_all=' + classId)
        .reply(204, 'No Content')
}

export function nockGetAllClassPins(classId: string | undefined, reply: ServerPinPreview[]) {
    nockGetAllClassPinsOptions(classId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/pins/?class_all=' + classId)
        .reply(200, reply);
}

function nockPinsOptions(pinId: string) {
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .options('/api/pins/' + pinId)
        .reply(204, 'No Content')
}

export function nockRemovePin(pinId: string) {
    nockPinsOptions(pinId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .delete('/api/pins/' + pinId)
        .reply(200);
}

export function nockGetPin(pinId: string, Pin: ServerPin) {
    nockPinsOptions(pinId);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .get('/api/pins/' + pinId)
        .reply(200, Pin);
}

export function nockAddPin(pinId_str: string) {
    nockPinsOptions(pinId_str);
    nock('https://uce.science.uu.nl')
        .defaultReplyHeaders(defaultReplyHeaders)
        .post('/api/pins/')
        .reply(200, pinId_str);
}
