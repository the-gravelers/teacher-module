/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {QueryClient, QueryClientProvider} from 'react-query';

/**
 * This is for using the renderHook a simple wrapper you can give
 * @param param0 Object which will be parsed as anything
 * @returns The object given wrapped in a router and queryclientprovider
 */
export const wrapper = ({children}: any) => {
    const queryClient = new QueryClient();

    return (
        <Router>
            <QueryClientProvider client={queryClient}>
                {children}
            </QueryClientProvider>
        </Router>
    )
}
/**
 * Wrapper for rendering in tests to reduce boiler plate
 * @param children The element(s) you want to render in a test
 * @param routes optional routes for if you need navigation in the test
 * @returns Your JSX element(s) wrapped in a router and queryclientprovider
 */
export const rendWrapper = (children: JSX.Element[] | JSX.Element | undefined, routes?: JSX.Element) => {
    const queryClient = new QueryClient();

    return (
        <QueryClientProvider client={queryClient}>
            <Router>
                {routes}
                {(children)}
            </Router>
        </QueryClientProvider>

    )
}