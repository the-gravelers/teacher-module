/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {render} from '@testing-library/react';
import {rendWrapper} from './renderWrapper';
import userEvent from '@testing-library/user-event';
import Register from '../Pages/Register';

test('Can you to in input fields', () => {
    //Render the Register page
    const {getByTestId} = render(rendWrapper(<Register/>));

    //Test changing bus places field
    const fullNameInput = getByTestId('fullName') as HTMLInputElement;
    expect(fullNameInput.value).toBe("");
    userEvent.type(fullNameInput, 'a');
    expect(fullNameInput.value).toBe("a");

    //Test changing bus amount field
    const emailInput = getByTestId('email') as HTMLInputElement;
    expect(emailInput.value).toBe("");
    userEvent.type(emailInput, 'a');
    expect(emailInput.value).toBe("a");

    //Test changing bus amount field
    const passwordInput = getByTestId('password') as HTMLInputElement;
    expect(passwordInput.value).toBe("");
    userEvent.type(passwordInput, '1');
    expect(passwordInput.value).toBe("1");

})