/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {fireEvent, render, screen} from '@testing-library/react';
import {ExtendableList} from '../Components/ExtendableList';

test('folding list and unfolding it', () => {
    const {getByText} = render(<ExtendableList header={<h1>Article</h1>}><p>P1</p><p>P2</p></ExtendableList>)
    //Extend the article to see the children
    fireEvent.click(getByText("Article"));
    getByText("P1");
    getByText("P2");

    //Unextend the article and make sure the children are no longer visible
    fireEvent.click(getByText("Article"));
    const para1 = screen.queryByText('P1');
    expect(para1).toBeNull() //Doesn't exist 
    const para2 = screen.queryByText('P2');
    expect(para2).toBeNull() //Doesn't exist 
});