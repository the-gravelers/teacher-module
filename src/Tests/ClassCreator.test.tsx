/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import { fireEvent, render, waitFor } from '@testing-library/react';
import ClassCreator from '../Pages/ClassCreator';
import { rendWrapper } from './renderWrapper';
import userEvent from '@testing-library/user-event';
import { nockGetClassPins } from './mocks/PinRequestMocks';
import { Route } from 'react-router-dom';
import { ClassOverview } from '../Pages/ClassOverview';
import { nockPostClass } from './mocks/ClassRequestMocks';
import { nockGetGroups } from './mocks/GroupRequestMocks';

//DUMMY DATA
const activeCourse = "1";
//END DUMMY DATA

test('Can you to in input fields', () => {
    const { getByTestId } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //Test writing in the Class name input field
    const classNameInput = getByTestId('className') as HTMLInputElement;
    expect(classNameInput.value).toBe("");
    userEvent.type(classNameInput, 'a');
    expect(classNameInput.value).toBe("a");

    //Test changing bus places field
    const BusPlaces = getByTestId('busPlaces') as HTMLInputElement;
    expect(BusPlaces.value).toBe("6");
    userEvent.type(BusPlaces, '1');
    expect(BusPlaces.value).toBe("61");

    //Test changing bus amount field
    const BusCount = getByTestId('busCount') as HTMLInputElement;
    expect(BusCount.value).toBe("1");
    userEvent.type(BusCount, '1');
    expect(BusCount.value).toBe("11");

    //Test changing bus amount field
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    expect(driverAmount.value).toBe("0");
    userEvent.type(driverAmount, '1');
    expect(driverAmount.value).toBe("1");

})

test('Add bus variant', () => {
    const { getByText, getByRole } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //Get button to add a extra bus variant and press it
    const addButton = getByRole('button', { name: 'Add bus variant' });
    fireEvent.click(addButton);

    //New bus variant should now exist
    getByText('Bus variant 2', { exact: false });
})

test('Confirm going to drivers form', () => {
    const placesInBus = 6;
    const drivers = 2;

    const { getByText, getByRole, getByTestId } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the busplaces and driverAmount input fields
    const BusPlaces = getByTestId('busPlaces') as HTMLInputElement;
    fireEvent.change(BusPlaces, { target: { value: placesInBus.toString() } })
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    fireEvent.change(driverAmount, { target: { value: drivers.toString() } })

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers forms

    //Make sure all is displayed correctly
    getByText("Group 1 with a maximum of " + placesInBus + " members");
    for (let i = 1; i <= drivers; i++) {
        getByText("Driver " + i, { exact: false });
    }
})

test('Going to drivers form and editing driver name field', () => {
    const placesInBus = 6;
    const drivers = 1;

    const { getByRole, getByTestId } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the busplaces and driverAmount input fields
    const BusPlaces = getByTestId('busPlaces') as HTMLInputElement;
    fireEvent.change(BusPlaces, { target: { value: placesInBus.toString() } })
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    fireEvent.change(driverAmount, { target: { value: drivers.toString() } })

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers forms

    //Edit the only driver field by fake typing in the field.
    const driverInput = getByTestId('driverName') as HTMLInputElement;
    userEvent.type(driverInput, 'a');
    expect(driverInput.value).toBe("a");
})

test('Going to drivers form with empty places and empty count dont crash', () => {
    const { getByTestId, getByRole } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the busplaces and busCount input fields
    const BusPlaces = getByTestId('busPlaces') as HTMLInputElement;
    fireEvent.change(BusPlaces, { target: { value: "" } })
    const BusCount = getByTestId('busCount') as HTMLInputElement;
    fireEvent.change(BusCount, { target: { value: "" } })

    //This needed otherwise it instantly creates the class with no groups which is not the goal of this this
    const addButton = getByRole('button', { name: 'Add bus variant' });
    fireEvent.click(addButton);

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers forms
})

test('Going to drivers form and then adding and removing a extra driver', () => {
    const placesInBus = 6;
    const drivers = 2;

    const { queryByText, getAllByRole, getByText, getByRole, getByTestId } = render(rendWrapper(<ClassCreator
        activeCourse={activeCourse} />));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the busplaces and driverAmount input fields
    const BusPlaces = getByTestId('busPlaces') as HTMLInputElement;
    fireEvent.change(BusPlaces, { target: { value: placesInBus.toString() } })
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    fireEvent.change(driverAmount, { target: { value: drivers.toString() } })

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers form

    //Check that all corresponding driver fields are present
    getByText("Group 1 with a maximum of " + placesInBus + " members");
    for (let i = 1; i <= drivers; i++) {
        getByText("Driver " + i, { exact: false });
    }

    //Press add driver button and check if input field is added.
    const addDriverButton = getByRole('button', { name: 'Add Driver' });
    fireEvent.click(addDriverButton);
    getByText("Group 1 with a maximum of " + placesInBus + " members");
    for (let i = 1; i <= drivers + 1; i++) {
        getByText("Driver " + i, { exact: false });
    }

    //Press button to remove last driver field and confirm it worked
    const removeDriverField = getAllByRole('img', { name: "remove button" });
    fireEvent.click(removeDriverField[drivers]);
    getByText("Group 1 with a maximum of " + placesInBus + " members");
    for (let i = 1; i <= drivers; i++) {
        getByText("Driver " + i, { exact: false });
    }
    expect(queryByText("Driver " + (drivers + 1), { exact: false })).toBeNull(); //Check it isn't visible anymore

})

test('Going to drivers form with 2 bus variants and being able to go forward and back', () => {
    const drivers = 0;

    const { getByText, getByRole, getByTestId } = render(rendWrapper(<ClassCreator activeCourse={activeCourse} />));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the driverAmount input field
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    fireEvent.change(driverAmount, { target: { value: drivers.toString() } })

    //Add bus variant and confirm it worked
    const addButton = getByRole('button', { name: 'Add bus variant' });
    fireEvent.click(addButton);
    getByText('Bus variant 2', { exact: false });

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers form

    //Go to the driver input of the next bus
    const nextButton = getByRole('button', { name: 'Next' });
    fireEvent.click(nextButton);

    //Go back to the driver input of the first bus
    const previousButton = getByRole('button', { name: 'Previous' });
    fireEvent.click(previousButton);
})

test('Going to drivers form and finishing it', async () => {
    const drivers = 0;
    const classId = 123;

    //Render the ClassCreator, but this time also with a possible route
    const { getByText, getByRole, getByTestId } = render(rendWrapper(
        <ClassCreator activeCourse={activeCourse} />,
        <Route path="/Class/:classId" component={ClassOverview} />
    ));

    //You need a name to go to the drivers form
    const classNameInput = getByTestId('className') as HTMLInputElement;
    userEvent.type(classNameInput, 'a');

    //Set the driverAmount input field
    const driverAmount = getByTestId('driverAmount') as HTMLInputElement;
    fireEvent.change(driverAmount, { target: { value: drivers.toString() } })

    //Confirm bus variant values and by pressing confirm button
    const confirmButton = getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton); //Go to drivers form

    //Fake server responses
    nockPostClass();
    nockGetClassPins(classId.toString(), []);
    nockGetGroups(classId.toString(), []);

    //Finish the driver form
    const finishButton = getByRole('button', { name: 'Finish' });
    fireEvent.click(finishButton);

    //Check that automatically get redirected to newly created class
    await waitFor(() => {
        expect(getByText('Pin list', { exact: false })).toBeInTheDocument();
        expect(getByText('Groups', { exact: false })).toBeInTheDocument();
    })

})

test('Dont crash when no active class', () => {
    render(rendWrapper(<ClassCreator />));
})