import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Library from '../Pages/Library';
import LibOverview from '../Components/Library/LibOverview';
import { SampleLibraryData } from '../Components/Library/SampleEntry';

test("Render Library", () => {
    render(<BrowserRouter><Library/></BrowserRouter>);
});

test("Click 'new entry'-button", () => {
    render(<BrowserRouter><Library/></BrowserRouter>);

    var newEntryButton = screen.getByRole('newLibEntryButton');
    fireEvent.click(newEntryButton);
});

test ("Run search", () => {
    render(<BrowserRouter><LibOverview data={SampleLibraryData}/></BrowserRouter>);

    var demoEntries = screen.getAllByText(/Demo/, {exact:false});
    expect(demoEntries.length === 2).toBe(true);

    var searchBar = screen.getByRole('SearchBar');

    fireEvent.change(searchBar, "copy");

    demoEntries = screen.getAllByText(/Demo/, {exact:false});
    expect(demoEntries.length === 2).toBe(true);
});

test("Click first entry", () => {
    render(<BrowserRouter><LibOverview data={SampleLibraryData}/></BrowserRouter>);

    var tableRows = screen.getAllByRole('tableEntry');
    fireEvent.click(tableRows[0]);
})