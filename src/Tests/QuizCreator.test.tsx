/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import { fireEvent, render } from '@testing-library/react';
import { rendWrapper } from './renderWrapper';
import userEvent from '@testing-library/user-event';
import CreateQuiz from '../Pages/QuizCreator';

test('Can you fill in input fields', () => {
    //Render the CreateQuiz page
    const { getAllByTestId, getByTestId } = render(rendWrapper(<CreateQuiz />));

    //Type in quizTitle input field
    const titleInput = getByTestId('quizTitle') as HTMLInputElement;
    expect(titleInput.value).toBe("");
    userEvent.type(titleInput, 'a');
    expect(titleInput.value).toBe("a");

    //Type in question input field
    const questionInput = getByTestId('question') as HTMLInputElement;
    expect(questionInput.value).toBe("");
    userEvent.type(questionInput, 'a');
    expect(questionInput.value).toBe("a");

    //Type in wrong answer input field
    const wrongAnswersInput = getAllByTestId('wrongAnswer') as HTMLInputElement[];
    expect(wrongAnswersInput[0].value).toBe("");
    userEvent.type(wrongAnswersInput[0], 'a');
    expect(wrongAnswersInput[0].value).toBe("a");

    //Type in correctAnswer input field
    const correctAnswerInput = getByTestId('correctAnswer') as HTMLInputElement;
    expect(correctAnswerInput.value).toBe("");
    userEvent.type(correctAnswerInput, 'a');
    expect(correctAnswerInput.value).toBe("a");
})

test('Add possible wrong answer', async () => {
    //Render the CreateQuiz page
    const { getByRole, getAllByTestId } = render(rendWrapper(<CreateQuiz />));

    //Gets the amount of wrong answer fields at the moment.
    const wrongAnswersInputold = getAllByTestId('wrongAnswer') as HTMLInputElement[];
    const oldLength = wrongAnswersInputold.length;

    //Find and press the add answer button
    const addButton = getByRole('button', { name: 'Add answer' });
    fireEvent.click(addButton);

    //Get all the wrong answer input fields
    const wrongAnswersInputnew = getAllByTestId('wrongAnswer') as HTMLInputElement[];

    //There should now be one more than before.
    expect(wrongAnswersInputnew.length === (oldLength + 1)).toBe(true);
})

test('Remove possible wrong answer', async () => {
    //Render the CreateQuiz page
    const { getAllByTestId } = render(rendWrapper(<CreateQuiz />));

    //Gets the amount of wrong answer fields at the moment.
    const wrongAnswersInputold = getAllByTestId('wrongAnswer') as HTMLInputElement[];
    const oldLength = wrongAnswersInputold.length;

    //Find and press a remove wrong answer button
    const removeButton = getAllByTestId('removeWrongAnswer');
    fireEvent.click(removeButton[0]);

    //Get all the wrong answer input fields
    const wrongAnswersInputnew = getAllByTestId('wrongAnswer') as HTMLInputElement[];

    //There should now be one less than before.
    expect(wrongAnswersInputnew.length === (oldLength - 1)).toBe(true);
})

test('Cannot remove question if there is only one', () => {
    //Render the CreateQuiz page
    const { getByText, getByTestId } = render(rendWrapper(<CreateQuiz />));

    //Check if we see a question
    getByText('Question 1');

    //Find remove button and press it
    const removeButton = getByTestId('removeQuestion');
    fireEvent.click(removeButton);

    //The question should still be there since it should stop it form deleting
    getByText('Question 1');
})

test('Add extra question then remove it', () => {
    //Render the CreateQuiz page
    const { getByText, getByRole, getAllByTestId, queryByText } = render(rendWrapper(<CreateQuiz />));
  
    //Check if we see a question
    getByText('Question 1');

    //Find and press the add question button
    const addQuestionButton = getByRole('button', { name: '+' });
    fireEvent.click(addQuestionButton);

    //There should now be two questions visible.
    getByText('Question 1');
    getByText('Question 2');

    //Find and press a remove question button
    const removeButton = getAllByTestId('removeQuestion');
    fireEvent.click(removeButton[0]);

    //There should now only be one question visible again and the other one should be gone
    getByText('Question 1');
    expect(queryByText('Question 2')).toBeNull()
})

test('Add extra question then select it', () => {
    //Render the CreateQuiz page
    const { getByText, getByRole } = render(rendWrapper(<CreateQuiz />));

    //Check if we see a question
    getByText('Question 1');
    
    //Find and press the add answer button
    const addQuestionButton = getByRole('button', { name: '+' });
    fireEvent.click(addQuestionButton);

    //There should now be two questions visible.
    getByText('Question 1');
    const quest2 = getByText('Question 2');

    //We press on the second question to go to that one.
    fireEvent.click(quest2);

    getByText('Question: 2');
})