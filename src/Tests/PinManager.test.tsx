/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import PinManager from '../Pages/PinManager';
import { PinList } from '../Components/PinList';
import {CreateImage} from '../Components/CreateImage';
import userEvent from '@testing-library/user-event';
import {rendWrapper} from './renderWrapper';
import { Pin, PinContent } from '../Types';
import {Route} from 'react-router-dom';
import {ServerPin, ServerPinPreview} from '../Server/PinRequests';
import {emptyPin, emptyPinContent} from '../Components/EmptyPin';
import {nockAddPin, nockGetClassPins} from "./mocks/PinRequestMocks";
import {nockGetPinContent} from "./mocks/PinContentRequestMocks";

//DUMMY DATA
const classId = "10000";

const Pin1: ServerPinPreview = {
    id_str: "0",
    title: "Pin 1",
    text: "Text of Pin 1",
    last_edited: "1623422938"
}
const Pin2: ServerPinPreview = {
    id_str: "1",
    title: "Pin 2",
    text: "Text of Pin 2",
    last_edited: "1623422558"
}

const rfile = new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'});
let content : PinContent = {
    pinId: "1",
    content_type: "image", //ImageType | VideoType,
    content_nr: 1,
    contentId: "1",
    content: rfile,
}
let dummyPin : Pin = {
    id: "0",
    title: "Pin 1",
    utm: {Northing:"10023", Easting:"10752", ZoneNumber:"71", ZoneLetter:"Z"},
    lat: "45.4564",
    lon: "22.5421",
    last_edited: "1623422938",
    text: "Text of Pin 1",
    contents: [content]
}
const dummyServerPins = [Pin1, Pin2];

//End of dummy data

window.alert = jest.fn();
afterEach(() => {
    jest.clearAllMocks();
});

test('can you type in the input fields', () => {
    const {getByTestId} = render(rendWrapper(<PinManager pin={emptyPin()}/>)); //getAllByRole, getByRole, getByText, getByLabelText, queryByTestId, getByTestId

    //find title inputField and fire userEvent to type "a"
    const title = getByTestId("title") as HTMLInputElement;
    expect(title.value).toBe("");
    userEvent.type(title, 'a');
    expect(title.value).toBe("a");

    //find latitude inputField and fire userEvent to type "1"
    const lat = getByTestId("lat") as HTMLInputElement;
    expect(lat.value).toBe("");
    userEvent.type(lat, '1');
    expect(lat.value).toBe("1");
    //check if the inputField gets cleared correctly
    userEvent.clear(screen.getByTestId("lat"));
    expect(screen.getByTestId("lat")).toHaveAttribute('value', '')
    //
    userEvent.type(lat, '12.345678');
    expect(lat.value).toBe("12.345678");
    userEvent.clear(lat);
    //
    userEvent.type(lat, '12.3456789');
    expect(lat.value).toBe("12.345678");
    userEvent.clear(lat);
    //
    userEvent.type(lat, '123');
    expect(lat.value).toBe("12");
    userEvent.clear(lat);
    //
    userEvent.type(lat, '-98');
    expect(lat.value).toBe("-9");
    userEvent.clear(lat);
    //
    userEvent.type(lat, '-90');
    expect(lat.value).toBe("-90");
    userEvent.clear(lat);

    //find longitude inputField and fire userEvent to type "2"
    const long = getByTestId("long") as HTMLInputElement;
    expect(long.value).toBe("");
    userEvent.type(long, '2');
    expect(long.value).toBe("2");
    //check if the inputField gets cleared correctly
    userEvent.clear(screen.getByTestId("long"));
    expect(screen.getByTestId("long")).toHaveAttribute('value', '')
    //
    userEvent.type(long, '12.345678');
    expect(long.value).toBe("12.345678");
    userEvent.clear(long);
    //
    userEvent.type(long, '12.3456789');
    expect(long.value).toBe("12.345678");
    userEvent.clear(long);
    //
    userEvent.type(long, '180');
    expect(long.value).toBe("180");
    userEvent.clear(long);
    //
    userEvent.type(long, '181');
    expect(long.value).toBe("18");
    userEvent.clear(long);
    //
    userEvent.type(long, '-181');
    expect(long.value).toBe("-18");
    userEvent.clear(long);
    //
    userEvent.type(long, '-180');
    expect(long.value).toBe("-180");
    userEvent.clear(long);
});

test('switching from lat/long to UTM', async () => {
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    expect(screen.getByPlaceholderText("Lat")).toBeInTheDocument();
    const select = screen.getByTestId("select");
    fireEvent.change(select, {
        target: { value: "UTM" },
      });
    expect(screen.getByPlaceholderText("Easting")).toBeInTheDocument();
    fireEvent.change(select, {
        target: { value: "Lat/Long" },
      });
    expect(screen.getByPlaceholderText("Lat")).toBeInTheDocument();
});

test('convert values from lat/long to UTM', async () => {
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    userEvent.type(screen.getByTestId("lat"), '45.455');
    userEvent.type(screen.getByTestId("long"), '23.744');
    const select = screen.getByTestId("select");
    fireEvent.change(select, {
        target: { value: "UTM" },
    });
    expect(screen.getByTestId("easting")).toHaveValue("714549.4491448777");
    expect(screen.getByTestId("northing")).toHaveValue("5037160.106739692");
    expect(screen.getByTestId("zoneNumber")).toHaveValue("34");
    expect(screen.getByTestId("zoneLetter")).toHaveValue("T");
    
    userEvent.clear(screen.getByTestId("easting"));
    userEvent.clear(screen.getByTestId("northing"));
    userEvent.clear(screen.getByTestId("zoneNumber"));
    userEvent.clear(screen.getByTestId("zoneLetter"));
    userEvent.type(screen.getByTestId("easting"), "813571.4387");
    userEvent.type(screen.getByTestId("northing"), "813571.4387");
    userEvent.type(screen.getByTestId("zoneNumber"), "32");
    userEvent.type(screen.getByTestId("zoneLetter"), "Z");
    fireEvent.change(select, {
        target: { value: "Lat/Long" },
    });
    expect(screen.getByTestId("lat")).toHaveValue("7.351230");
    expect(screen.getByTestId("long")).toHaveValue("11.840053");

});

test('test the submit with empty pin', async () => {
    const {getByRole} =
    render(rendWrapper(<PinManager pin={emptyPin()}/>,
        <Route path={"/Class/" + classId}>
            <PinList classId={classId}/>
        </Route>
    ));
    const submitButton = screen.getByTestId("submit");
    userEvent.click(submitButton);
    await waitFor(() => {
        //expect(screen.getByRole('alert')).toBeInTheDocument;
        //expect(screen.getByText('Latitude and Longitude field can not be empty')).toBeVisible;
        const alertMock = jest.spyOn(window,'alert');
        expect(alertMock).toHaveBeenCalledTimes(1);
    });
});

/* NEEDS BUGFIXING
*/
test('test the submit with dummy data', async () => {
    const file = new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'});
    nockGetClassPins(classId, dummyServerPins);
    nockGetPinContent(dummyPin.id, emptyPinContent().contentId, [file]);
    const {getByText} =
        render(rendWrapper(<PinManager pin={dummyPin}/>,
            <Route path={"/Class/" + classId}>
                <PinList classId={classId}/>
            </Route>
        ));

    //Get the submit button and press it
    //const PinLinks = getByRole('link', {name : "Create Pin"});
    //fireEvent.click(PinLinks);
    const submitButton = screen.getByTestId("submit");
    userEvent.click(submitButton);

    //Wait till linking is finished
    //await waitFor(() => {
        //Check if all data is filled in form the pin
        //expect(getByText('Pin 1')).toBeInTheDocument();})
});

//create mock function to simulate window.URL.createObjectURL, as jest doesn't implement this method yet
window.URL.createObjectURL = jest.fn();
afterEach(() => {
    jest.clearAllMocks();
});

test('test upload a single file', () => {
    //create new file
    const file = new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'});
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    //find image uploader and fire userEvent to upload the file
    const input = screen.getByLabelText(/Add Image/) as HTMLInputElement;
    userEvent.upload(input, file);
    if (input.files === null) return;
    else {
        expect(input.files).toHaveLength(1)
        expect(input.files[0]).toStrictEqual(file)
    }
    //throws an expception if the element cannot be found
    const image = screen.getAllByAltText("");
    expect(image).toHaveLength(1);
    expect(image[0]).toHaveAttribute('alt', "");
});
//Covers ln 29-39, 73-75 of CreateImage (doesn't show on coverige)
test('test deleting an image', () => {
    //render(rendWrapper(<CreateImage content={[emptyPinContent()]} content_str={["image/png"]} setContent_str={(e) => null} setContent={(e) => null}/>));
    const file = new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'});
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    //find image uploader and fire userEvent to upload two files
    const input = screen.getByLabelText(/Add Image/) as HTMLInputElement;
    userEvent.upload(input, file);
    //check if the image gets removed correctly
    const removeButton = screen.getByRole('button', {name: "minus_button"});
    userEvent.click(removeButton);
    //the nextbutton shouldn't be visible when removing an image
    //const nextPrevButtons = screen.getAllByType('button');
    const nextButton = screen.queryByTestId('next')
    expect(nextButton).toBeNull();
});

test('test upload multiple files', () => {
    //create new list of files
    const files = [
        new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'}),
        new File(['PlaceHolder2'], 'PlaceHolder2.png', {type: 'image2/png'}),
    ];
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    //find image uploader and fire userEvent to upload two files
    const input = screen.getByLabelText(/Add Image/) as HTMLInputElement;
    userEvent.upload(input, files);
    if (input.files === null) return;
    else {
        expect(input.files).toHaveLength(2)
        expect(input.files[0]).toStrictEqual(files[0])
        expect(input.files[1]).toStrictEqual(files[1])
    }
    //throws an expception if the element cannot be found
    const image = screen.getAllByAltText("");
    expect(image).toHaveLength(1); //only one image is shown at a time
    expect(image[0]).toHaveAttribute('alt', "");
});
//Covers ln 45-48, 79-71 for next
//and ln 54-58, 64-67 for prev of CreateImage (doesn't show on coverige)
test('scrolling through the images', () => {
    //render(rendWrapper(<CreateImage content={[emptyPinContent()]} content_str={["image/png", "image2/png"]} setContent_str={(e) => null} setContent={(e) => null}/>));
    const files = [
        new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'}),
        new File(['PlaceHolder2'], 'PlaceHolder2.png', {type: 'image2/png'}),
    ];
    render(rendWrapper(<PinManager pin={emptyPin()}/>));
    //find image uploader and fire userEvent to upload two files
    const input = screen.getByLabelText(/Add Image/) as HTMLInputElement;
    userEvent.upload(input, files);
    //test if nextbutton selects the next image
    const image1 = screen.getByAltText("");
    const nextButton = screen.getByRole('button', {name: 'arrow_right'});
    userEvent.click(nextButton);
    const image2 = screen.getByAltText("");
    const src = image1.getAttribute('type');
    expect(image2).not.toHaveAttribute('type', src);

    //test if prevButton selects the previous image
    const prevButton = screen.getByRole('button', {name: 'arrow_left'});
    userEvent.click(prevButton);
    screen.getByAltText("");
    const src2 = image2.getAttribute('type');
    expect(image2).not.toHaveAttribute('type', src2);
});

test('display the content of existing pin', async () => {
    render(rendWrapper(<PinManager pin={dummyPin}/>));
    await waitFor(() => {
    const image = screen.getAllByAltText("");
    expect(image).toBeInTheDocument;
    });
});

test('typing in textField', () => {
    render(rendWrapper(<PinManager pin={emptyPin()}/>));

    const newtext = screen.getByTestId("text") as HTMLInputElement;
    expect(newtext.value).toBe("");
    userEvent.type(screen.getByTestId("text"), 'a');
    expect(screen.getByTestId("text")).toHaveValue("a");
});

/*test('sending pin to server', () =>{
*    const { result, waitFor } = render(rendWrapper(<PinManager pin={dummyPin}/>));
*    const submitButton = screen.getByTestId("submit");
*    userEvent.click(submitButton);
*    nockAddPin(dummyPin.id)
*    return waitFor(() => {
*        const res = result.current;
*        const pins = res.data;
*        //console.log(pins);
*        return (res.isSuccess && expect(pins).toEqual(dummyPins));
*    })
*    expect(reply).toBe(dummyPin.id);
*
}); */