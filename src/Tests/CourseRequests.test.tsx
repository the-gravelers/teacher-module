/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {convertCourse, getCourse, ServerCourse, uniId} from "../Server/CourseRequests";
import {nockGetCourse} from "./mocks/CourseRequestMocks";

//DUMMY DATA
const courseGeo: ServerCourse = {
    course_id_str: "1",
    university_id_str: uniId.toString(),
    name: "GEO3-1210"
}
//END OF DUMMY DATA

test('Get individual course request test', async () => {
    nockGetCourse(courseGeo.course_id_str, courseGeo)
    const serverresponse = await getCourse(courseGeo.course_id_str);
    expect(serverresponse).toStrictEqual(convertCourse(courseGeo))
})