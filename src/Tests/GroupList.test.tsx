/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {GroupList, useFetchGroups} from '../Components/GroupList';
import {rendWrapper, wrapper} from './renderWrapper';
import {renderHook} from '@testing-library/react-hooks'
import {convertGroup, ServerGroup} from '../Server/GroupsRequests';
import {fireEvent, render, waitFor} from '@testing-library/react';
import {nockGetGroups, nockGetUsersFromGroup} from './mocks/GroupRequestMocks';
import { User } from '../Types';

//TEMPORARY DUMMY DATA
const dummyUsers : User[]= [{id_str: "10", name: "Henry"}, {id_str: "11", name:"Henk"}, {id_str: "12", name:"Ron"}]

const group1: ServerGroup = {
    name: "Vlinders",
    members: [{user_id_str: dummyUsers[0].id_str}, {user_id_str: dummyUsers[1].id_str}, {user_id_str: dummyUsers[2].id_str}],
    max_user_count: 6,
    class_id_str: "0",
    pin_group_id_str: "0",
    user_group_id_str: "0"
}
const group2: ServerGroup = {
    name: "Rupsen",
    members: [],
    max_user_count: 6,
    class_id_str: "0",
    pin_group_id_str: "1",
    user_group_id_str: "1"
}
const unassigned: ServerGroup = {
    name: "Unassigned",
    members: [],
    max_user_count: Number.MAX_SAFE_INTEGER,
    class_id_str: "0",
    pin_group_id_str: "2",
    user_group_id_str: "2"
}

const dummyGroupsServer = [group1, group2, unassigned];
let dummyGroups = [convertGroup(group1), convertGroup(group2), convertGroup(unassigned)];
dummyGroups[0].users = dummyUsers;
dummyGroups[1].users = [];
dummyGroups[2].users = [];

const classId = "10000";
//END OF DUMMY DATA

test('getGroups succes case server response', async () => {
    nockGetGroups(classId, dummyGroupsServer);

    for(let i=0; i<dummyGroupsServer.length; i++) {
        if(dummyGroupsServer[i].user_group_id_str === '0') {
            nockGetUsersFromGroup(dummyGroupsServer[i].user_group_id_str, dummyUsers);
        }
        else {
            nockGetUsersFromGroup(dummyGroupsServer[i].user_group_id_str, []);
        }
    }

    const {result, waitFor} = renderHook(() => useFetchGroups(classId), {wrapper});

    await waitFor(() => {
        const res = result.current;
        const groups = res.data;
        expect(res.isSuccess).toEqual(true);
        expect(groups).toEqual(dummyGroups);
    })
});

test('open a group to display the members', async () => {
    //mock the server request for the GroupList component
    nockGetGroups(classId, dummyGroupsServer);
    for(let i=0; i<dummyGroupsServer.length; i++) {
        nockGetUsersFromGroup(dummyGroupsServer[i].user_group_id_str, dummyUsers);
    }

    //Render the GroupList component
    const {getAllByRole, getByText} = render(rendWrapper(<GroupList classId={classId}/>));

    //Make sure the dummy data is loaded and on display
    await waitFor(() => {
        expect(getByText(group1.name, {exact: false})).toBeInTheDocument();
        expect(getByText(group2.name, {exact: false})).toBeInTheDocument();
        expect(getByText(unassigned.name, {exact: false})).toBeInTheDocument();
    })

    //Press the group header to view it's members
    const groupHeaders = getAllByRole('showDetails');
    fireEvent.click(groupHeaders[0]); //Open group one

    //Check if all members are shown
    for (let i = 0; i < dummyUsers.length; i++) {
        expect(getByText(dummyUsers[i].name, {exact: false})).toBeInTheDocument();
    }
})