/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {fireEvent, getByText, render, waitFor} from '@testing-library/react';
import {rendWrapper} from './renderWrapper';
import userEvent from '@testing-library/user-event';
import Login from '../Pages/Login';
import {nockLogin, nockUserInfo} from './mocks/AuthorizationMocks';
import {AuthToken, UserInfo} from '../Types';
import {Route} from 'react-router-dom';
import Courses from '../Pages/Courses';
import {nockGetCourses} from './mocks/CourseRequestMocks';
import {PrivateRoute} from "../PrivateRoute";

//DUMMY DATA
const token: AuthToken = {
    access_token: "1",
    refresh_token: "1",
    expires_in: 10000,
    session_id_str: '1',
    token_type: 'bearer',
}

const teacherAcc: UserInfo = {
    auth_level: 3,
    class_id_str: "1",
    email: "henk@krol.hotmail",
    id_str: "1",
    name: "Henk Krol",
}

const studentAcc: UserInfo = {
    auth_level: 1,
    class_id_str: "1",
    email: "jan@uu.gmail",
    id_str: "1",
    name: "Jan Pieters",
}
//END DUMMY DATA

test('Can you to in input fields', () => {
    //Render the Login page

    //const {getByTestId} = render(rendWrapper(<div></div>));
    const {getByTestId} = render(rendWrapper(<Login/>));
    //Test changing the username input field
    const userNameInput = getByTestId('email') as HTMLInputElement;
    expect(userNameInput.value).toBe("");
    userEvent.type(userNameInput, 'a');
    expect(userNameInput.value).toBe("a");

    //Test changing the password input field
    const passwordInput = getByTestId('password') as HTMLInputElement;
    expect(passwordInput.value).toBe("");
    userEvent.type(passwordInput, '1');
    expect(passwordInput.value).toBe("1");

})

test('Login as Teacher', async () => {
    //Render the Login page and a route to the Courses Page
    //TODO Fix it so actually use PrivateRoute it works in reality, but not in the test atm.
    const {getByRole, getByText} = render(rendWrapper(
        <Login/>,
        <Route path="/Courses" component={Courses}/>
    ));

    //mock the server request for logging in.
    nockLogin(token);
    nockUserInfo(teacherAcc);

    //mock the server request for the Courses Page
    nockGetCourses([]);

    //Find the submit button to Login
    const submitButton = getByRole('button', {name: 'Login'});
    fireEvent.click(submitButton);

    //mock the server request for auth level checking
    nockLogin(token);
    nockUserInfo(teacherAcc);

    //Make sure we are on the Courses page.
    await waitFor(() => {
        getByText('Course List')
    })

})

test('Login as Student', async () => {
    //Render the Login page and a route to the Courses Page
    const {getByRole, getAllByText} = render(rendWrapper(
        <Login/>,
        <PrivateRoute path="/Courses" component={Courses}/>
    ));

    //mock the server request for logging in.
    nockLogin(token);
    nockUserInfo(studentAcc);

    //Find the submit button to Login
    const submitButton = getByRole('button', {name: 'Login'});
    fireEvent.click(submitButton);

    //mock the server request for auth level checking
    nockUserInfo(teacherAcc);

    //mock the server request for the Courses Page
    nockGetCourses([]);

    //Expect to be blocked because to low auth level so redirected to the login page.
    await waitFor(() => {
        getAllByText('Login')
    })

})