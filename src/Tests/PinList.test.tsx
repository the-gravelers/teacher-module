/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { rendWrapper, wrapper } from './renderWrapper';
import { fixDates, PinList, useFetchPins } from '../Components/PinList';
import { convertPin, convertPinPreview, ServerPin, ServerPinPreview } from '../Server/PinRequests';
import { renderHook } from '@testing-library/react-hooks';
import { setCookie } from '../Server/Cookie';
import { nockGetAllClassPins, nockGetClassPins, nockGetPin, nockGetUserPins, nockRemovePin } from './mocks/PinRequestMocks';
import nock from 'nock';
import { Route } from 'react-router-dom';
import PinCreator from '../Pages/PinManager';
import userEvent from '@testing-library/user-event';
import { emptyPin } from '../Components/EmptyPin';
import { activePinStore } from '../Stores/ActivePinStore';

//DUMMY DATA
const classId = "10000";
const userId = "1";

const Pin1: ServerPinPreview = {
    id_str: "0",
    title: "Pin 1",
    text: "Text of Pin 1",
    last_edited: "1623422938"
}
const Pin2: ServerPinPreview = {
    id_str: "1",
    title: "Pin 2",
    text: "Text of Pin 2",
    last_edited: "1623422558"
}

const Pin1Full: ServerPin = {
    id_str: Pin1.id_str,
    title: Pin1.title,
    text: Pin1.text,
    utm: { Northing: "1351", Easting: "2315", ZoneNumber: "54", ZoneLetter: "Z" },
    last_edited: Pin1.last_edited,
    contents: [],
    latitude_str: "55.420",
    longitude_str: "69.732",
    user_id_str: userId,
}

const dummyServerPins = [Pin1, Pin2];
const dummyPins = fixDates([convertPinPreview(Pin1), convertPinPreview(Pin2)]);
//END OF DUMMY DATA

test('GetClassPins succes case server response', async () => {
    //mock server request for getting a class.
    nockGetClassPins(classId, dummyServerPins);

    //Make request for the classes
    const { result, waitFor } = renderHook(() => useFetchPins("Class", classId), { wrapper });

    //Check if the two results match
    await waitFor(() => {
        const res = result.current;
        const pins = res.data;
        expect(res.isSuccess).toEqual(true);
        expect(pins).toEqual(dummyPins);
    })
});

test('GetUserPins succes case server resposne', async () => {
    //mock server request for getting the user pins
    setCookie("user_id", userId, 1);
    nockGetUserPins(userId, dummyServerPins);

    //Make request for the pins of the Teacher
    const { result, waitFor } = renderHook(() => useFetchPins("Teacher", userId), { wrapper });

    //Check if the two results match
    await waitFor(() => {
        const res = result.current;
        const pins = res.data;
        expect(res.isSuccess).toEqual(true);
        expect(pins).toEqual(dummyPins);
    })
})

test('Swap pin view mode', () => {
    //Nock the server requests for each view mode.
    nockGetClassPins(classId, dummyServerPins);
    nockGetUserPins(userId, dummyServerPins);
    nockGetAllClassPins(classId, dummyServerPins);

    const { getByRole, getAllByRole } = render(rendWrapper(<PinList classId={classId} />));
    const selectMenu = getByRole("viewMode");
    const options = getAllByRole('option') as HTMLOptionElement[];

    //Check base value true others false;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value === 'Class') {
            expect(options[i].selected).toBeTruthy();
        } else {
            expect(options[i].selected).toBeFalsy();
        }
    }

    //Set the view mode to Teacher.
    fireEvent.change(selectMenu, { target: { value: 'Teacher' } })

    //Expect That the view mode is now Teacher.
    for (let i = 0; i < options.length; i++) {
        if (options[i].value === 'Teacher') {
            expect(options[i].selected).toBeTruthy();
        } else {
            expect(options[i].selected).toBeFalsy();
        }
    }

    //Set the view mode to All.
    fireEvent.change(selectMenu, { target: { value: 'All' } })

    //Expect That the view mode is now All.
    for (let i = 0; i < options.length; i++) {
        if (options[i].value === 'All') {
            expect(options[i].selected).toBeTruthy();
        } else {
            expect(options[i].selected).toBeFalsy();
        }
    }
});

test('Can you remove a pin from the pinlist', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and wait for the dummy data to load in.
    const { getByText, getByRole, getAllByRole } = render(rendWrapper(<PinList classId={classId} />));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    const allBoxes = getAllByRole('checkbox'); //Find all checkboxes
    //allBoxes[0] is the checkAllBox
    fireEvent.click(allBoxes[1]); //Check the box of Pin 1

    //fake the delete pin server reply.
    nockRemovePin(Pin1.id_str);
    //react query output will be invalidated so is gonna refetch so now return without Pin1
    nockGetClassPins(classId, [Pin2]);

    //Find the delete button and press it
    const delButton = getByRole('button', { name: "Delete" })
    fireEvent.click(delButton);

    //The pin should now no longer exist so not visible.
    await waitFor(() => {
        const pin1 = screen.queryByText('Pin 1');
        expect(pin1).toBeNull() //Check if doesn't exist anymore
    })

});

test('Check if checkall box works', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and wait for the dummy data to load in.
    const { getByText, getByRole, getAllByRole } = render(rendWrapper(<PinList classId={classId} />));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    const allBoxes = getAllByRole('checkbox'); //Find all checkboxes
    fireEvent.click(allBoxes[0]);     //allBoxes[0] is the checkAllBox

    //fake the delete pin server reply.
    nockRemovePin(Pin1.id_str);
    nockRemovePin(Pin2.id_str);
    //react query output will be invalidated so is gonna refetch so now return without Pin1
    nockGetClassPins(classId, []);

    //Find the delete button and press it.
    const delButton = getByRole('button', { name: "Delete" })
    fireEvent.click(delButton);

    //Expect both pins to no longer exist so not visible.
    await waitFor(() => {
        const pin1 = screen.queryByText('Pin 1');
        expect(pin1).toBeNull() //Check if doesn't exist anymore
        const pin2 = screen.queryByText('Pin 2');
        expect(pin2).toBeNull() //Check if doesn't exist anymore
    })
});

test('Can you select a pin then unselect it and if you press delete it wont get removed', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and wait for the dummy data to load in.
    const { getByText, getByRole, getAllByRole } = render(rendWrapper(<PinList classId={classId} />));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    const allBoxes = getAllByRole('checkbox'); //Find all checkboxes
    //allBoxes[0] is the checkAllBox
    fireEvent.click(allBoxes[1]); //check the box
    fireEvent.click(allBoxes[1]); //uncheck the box

    //Fake the delete pin server reply.
    nockRemovePin(Pin1.id_str);
    //React query output will be invalidated if it actually deletes the pin so is gonna refetch so now return without Pin1
    nockGetClassPins(classId, [Pin2]);

    //We press the delete button and expect it to do nothing.
    const delButton = getByRole('button', { name: "Delete" })
    fireEvent.click(delButton); //See if it gets removed from purge list

    //The pin should still exist since we unchecked the checkbox
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
    })

    //Clear the remove nock if the test succeeds
    nock.cleanAll()
});

test('Opening and closing the details of a pin', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and wait for the dummy data to load in.
    const { getByText, queryByText, getAllByRole } = render(rendWrapper(<PinList classId={classId} />));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    //Find the expand button and press it
    const expandButtons = getAllByRole('showDetails');
    fireEvent.click(expandButtons[0]);

    //We expect to see the details of the pin which should be the text.
    getByText(Pin1.text);

    //Click the expand button again details should now be hidden.
    fireEvent.click(expandButtons[0]);

    //Check the details are not visible anymore
    expect(queryByText(Pin1.text)).toBeNull();
})

test('Click on add pin button and go to the Pinmanager and see empty fields', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and a route to the PinCreator and wait for the dummy data to load in.
    const { getByText, getByTestId, getByRole } =
        render(rendWrapper(<PinList classId={classId} />,
            <Route path="/Pin">
                <PinCreator pin={emptyPin()} />
            </Route>
        ));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    //Get the addPin button and press it
    const PinLinks = getByRole('link', { name: "Add pin" });
    fireEvent.click(PinLinks);

    //Wait till linking is finished
    await waitFor(() => {
        //Check if all data is filled in form the pin
        expect(getByText('Make a pin')).toBeInTheDocument();
        const title = getByTestId("title") as HTMLInputElement;
        expect(title.value).toBe("");
        const lat = getByTestId("lat") as HTMLInputElement;
        expect(lat.value).toBe("");
        const long = getByTestId("long") as HTMLInputElement;
        expect(long.value).toBe("");
    })
});

test('Click on a pin and go to PinManager and see the values of the pin in the input fields', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and a route to the PinCreator and wait for the dummy data to load in.
    const { getByText, getByTestId, getAllByTestId } =
        render(rendWrapper(<PinList classId={classId} />,
            <Route path="/Pin">
                <PinCreator pin={convertPin(Pin1Full)} />
            </Route>
        ));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    //Mock the server request for getting the a pin.
    nockGetPin(Pin1.id_str, Pin1Full);

    const PinLinks = getAllByTestId('pinName');
    //PinLinks[0] = Pin1, PinLinks[1] = Pin2
    fireEvent.click(PinLinks[0]);

    //Wait till linking is finished
    await waitFor(() => {
        //Check if all data is filled in form the pin
        expect(getByText('Make a pin')).toBeInTheDocument();
        const title = getByTestId("title") as HTMLInputElement;
        expect(title.value).toBe(Pin1Full.title);
        const lat = getByTestId("lat") as HTMLInputElement;
        expect(lat.value).toBe(Pin1Full.latitude_str);
        const long = getByTestId("long") as HTMLInputElement;
        expect(long.value).toBe(Pin1Full.longitude_str);
    })
});

test('test searchbar input', async () => {
    //Mock the server request for getting the pins from the class.
    nockGetClassPins(classId, dummyServerPins);

    //Render the PinList component and wait for the dummy data to load in.
    const { getByText, queryByText, getByPlaceholderText } = render(rendWrapper(<PinList classId={classId} />));
    await waitFor(() => {
        expect(getByText('Pin 1')).toBeInTheDocument();
        expect(getByText('Pin 2')).toBeInTheDocument();
    })

    //Type in the search bar and expect to only see the pins which match the search request
    const search = getByPlaceholderText("Search pins") as HTMLInputElement;
    userEvent.type(search, "Pin 1");
    expect(queryByText("Pin 2")).toBeNull();
});