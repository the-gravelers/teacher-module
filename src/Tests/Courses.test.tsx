/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React from 'react';
import {fireEvent, render, waitFor} from '@testing-library/react';
import {Route} from 'react-router-dom';
import Courses from '../Pages/Courses';
import {Class, Course} from '../Types';
import {rendWrapper} from './renderWrapper';
import {nockGetCourses, nockRemoveCourse} from './mocks/CourseRequestMocks';
import {convertCourse, ServerCourse, uniId} from '../Server/CourseRequests';
import {convertClass, ServerClass} from '../Server/ClassRequests';
import {nockGetClassPins} from './mocks/PinRequestMocks';
import {ClassOverview} from '../Pages/ClassOverview';
import {CourseManager} from '../Pages/CourseManager';
import {nockGetClasses, nockRemoveClass} from './mocks/ClassRequestMocks';
import {nockGetGroups} from './mocks/GroupRequestMocks';

//DUMMY DATA
const courseGeo: ServerCourse = {
    course_id_str: "1",
    university_id_str: uniId.toString(),
    name: "GEO3-1210"
}
const courseEarth: ServerCourse = {
    course_id_str: "2",
    university_id_str: uniId.toString(),
    name: "GEO4-1517A"
}
const class2021Geo: ServerClass = {
    class_id_str: "1",
    course_id_str: courseGeo.course_id_str,
    name: "Class of 2021/22",
    pin_group_id_str: "1",
}
const class2022Geo: ServerClass = {
    class_id_str: "2",
    course_id_str: courseGeo.course_id_str,
    name: "Class of 2022/23",
    pin_group_id_str: "2",
}

const dummyServerCourses: ServerCourse[] = [courseGeo, courseEarth];
convertCourse(courseGeo);
convertCourse(courseEarth);

const dummyServerClasses: ServerClass[] = [class2021Geo, class2022Geo];
convertClass(class2021Geo);
convertClass(class2022Geo);
//END OF DUMMY DATA

async function ConfirmDummyDataLoaded(getByText: any) {
    await waitFor(() => {
        expect(getByText(courseGeo.name)).toBeInTheDocument();
        expect(getByText(courseEarth.name)).toBeInTheDocument();
        expect(getByText(class2021Geo.name)).toBeInTheDocument();
        expect(getByText(class2022Geo.name)).toBeInTheDocument();
    })
}

test('Courses on Display', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Wait till dummy data loaded and make sure it is displayed
    const {getByText} = render(rendWrapper(<Courses/>));
    await ConfirmDummyDataLoaded(getByText);
});

test('Select a different course', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Wait till dummy data loaded and make sure it is displayed
    const {getByText, getByRole} = render(rendWrapper(<Courses/>));
    await ConfirmDummyDataLoaded(getByText);

    nockGetClasses(courseEarth.course_id_str, [])

    const earthCourse = getByRole('button', {name: courseEarth.name});
    fireEvent.click(earthCourse);
});

test('Can Nav to a Class', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Wait till dummy data loaded and make sure it is displayed
    const {getByText, getByRole} = render(rendWrapper(
        <Courses/>,
        <Route path="/Class/:classId" component={ClassOverview}/>
    ));
    await ConfirmDummyDataLoaded(getByText);

    //mocks the server request for the classoverview page.
    nockGetClassPins(class2021Geo.class_id_str, []);
    nockGetGroups(class2021Geo.class_id_str.toString(), []);

    //Find the Geo2021 class and press it to link to the overview page
    const Geo2021 = getByRole('link', {name: class2021Geo.name})
    fireEvent.click(Geo2021);

    //Expect to be in the classoverview page
    await waitFor(() => {
        expect(getByText('Pin list', {exact: false})).toBeInTheDocument();
        expect(getByText('Groups', {exact: false})).toBeInTheDocument();
    })
})

test('Can Nav to CourseManager to make a new class', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Render the courses page and a route to an individual course
    const {getByText, getByRole} = render(rendWrapper(
        <Courses/>,
        <Route path="/Course" component={CourseManager}/>
    ));
    await ConfirmDummyDataLoaded(getByText);

    //Find the new course button and press it.
    const addButton = getByRole('link', {name: "New Course"});
    fireEvent.click(addButton);

    //Expect to be in Course Creator page.
    getByText('Course Creator');
})

test('Deleting a class', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    const {getByText, getAllByRole, queryByText} = render(rendWrapper(<Courses/>));
    await ConfirmDummyDataLoaded(getByText);

    //Should automatically refetch after deleting the class so mock the requests again
    nockGetClasses(courseGeo.course_id_str, [class2022Geo]);
    nockRemoveClass(class2021Geo.class_id_str);

    //Find the remove button for the first class and press it.
    const classRemoveButtons = getAllByRole('img', {name: 'removeTrashcan'});
    fireEvent.click(classRemoveButtons[0]);

    //Expect the class to be no longer visible since it is removed
    await waitFor(() => {
        expect(queryByText(class2021Geo.name)).toBeNull();
    })

})

test('Deleting a course', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    const {getByText, getByRole, queryByText} = render(rendWrapper(<Courses/>));
    await ConfirmDummyDataLoaded(getByText);

    //Should automatically refetch after deleting the course
    nockRemoveCourse(courseGeo.course_id_str);
    nockGetCourses([courseEarth]);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Find the remove button for the first course and press it.
    const courseDeleteButton = getByRole('button', {name: 'Delete'});
    fireEvent.click(courseDeleteButton);

    //Expect the course to be no longer visible since it is removed
    await waitFor(() => {
        expect(queryByText(courseGeo.name)).toBeNull();
    })
})

test('Can Nav to CourseManager to update a class', async () => {
    //mocks the server request for the courses page.
    nockGetCourses(dummyServerCourses);
    nockGetClasses(courseGeo.course_id_str, dummyServerClasses);

    //Render the Courses page and route to the CourseManager
    const {getByText, getByRole, getByTestId} = render(rendWrapper(
        <Courses/>,
        <Route path="/Course">
            <CourseManager courseId={courseGeo.course_id_str} name={courseGeo.name}/>
        </Route>
    ));
    await ConfirmDummyDataLoaded(getByText);

    //Find button to edit the course and press it.
    const changeCourseButton = getByRole('link', {name: "Change Name"});
    fireEvent.click(changeCourseButton);

    //Expect to see the name of the course we just pressed so we can edit it.
    await waitFor(() => {
        getByText('Course Creator');
        const nameField = getByTestId("courseName") as HTMLInputElement;
        expect(nameField.value).toBe(courseGeo.name);
    })

})