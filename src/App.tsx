/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React from 'react';
import NavBar from './Components/NavBar'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import ClassCreator from './Pages/ClassCreator';
import PinCreator from './Pages/PinManager';
import Library from './Pages/Library';
import LibBook from './Pages/LibBook';
import LibBookEditor from './Pages/LibBookEditor';
import {theme} from './Theme';
import {ThemeProvider} from 'styled-components';
import {ClassOverview} from './Pages/ClassOverview';
import {CourseManager} from './Pages/CourseManager';
import Courses from './Pages/Courses';
import GroupCreator from './Pages/GroupCreator';
import QuizCreator from './Pages/QuizCreator';
import Login from './Pages/Login';
import Register from './Pages/Register';
import {Helmet} from "react-helmet";
import {ToastContainer} from './Components/Toasts';
import {activePinStore} from './Stores/ActivePinStore';
import {browsingStore} from './Stores/BrowsingStore';
import {editStore} from './Stores/EditStore';
import {PrivateRoute} from "./PrivateRoute";

function App() {
    const activePin = activePinStore((state) => state.activePin);
    const activeCourse = browsingStore((state) => state.activeCourse);
    const editPinName = editStore((state) => state.name);
    const editPinCourse = editStore((state) => state.courseId);

    return (
        <ThemeProvider theme={theme}>
            <Helmet>
                <title>TeacherModule</title>
                <style>{'body { background-color: #E9E9E9; }'}</style>
            </Helmet>
            <ToastContainer/>
            <Router>
                <NavBar/>
                <Switch>
                    <PrivateRoute path="/" exact component={Courses}/>

                    <Route path="/Login" exact component={Login}/>
                    <Route path="/Register/:classId" component={Register}/>

                    <PrivateRoute path="/Courses" component={Courses}/>
                    <PrivateRoute path="/Course">
                        <CourseManager courseId={editPinCourse} name={editPinName}/>
                    </PrivateRoute>

                    <PrivateRoute path="/Class/:classId" component={ClassOverview}/>
                    <PrivateRoute path="/CreateClass">
                        <ClassCreator activeCourse={activeCourse}/>
                    </PrivateRoute>

                    <PrivateRoute path="/Library" component={Library}/>
                    <PrivateRoute path="/LibBook/:bookId" component={LibBook}/>
                    <PrivateRoute path="/LibBookEditor/:bookId" component={LibBookEditor}/>

                    <PrivateRoute path="/Pin">
                        <PinCreator pin={activePin}/>
                    </PrivateRoute>

                    <PrivateRoute path="/Group" component={GroupCreator}/>
                    <PrivateRoute path="/Quiz" component={QuizCreator}/>

                </Switch>
            </Router>
        </ThemeProvider>
    );
}

export default App;
