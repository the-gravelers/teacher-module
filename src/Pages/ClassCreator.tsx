/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { Article, ArticleTitle } from '../Components/StyledComponents';
import {
    DelButtonImage,
    FakeForm,
    FormHeader,
    Input,
    Label,
    NumberField,
    SubmitButton
} from '../Components/FormComponents';
import { postClass } from '../Server/ClassRequests';
import { AuthLevel, Class, Group } from '../Types';
import { theme } from '../Theme';
import { FlexButton } from '../Components/FlexListComponents';
import delButtonImg from '../Images/minus_centered.png';
import { browsingStore } from '../Stores/BrowsingStore';
import { useMutation, useQueryClient } from 'react-query';
import { postGroup } from '../Server/GroupsRequests';
import { useHistory } from 'react-router-dom';
import { RegisterByMail } from '../Server/RegisterRequests';
import { SuccessToast, WarningToast } from '../Components/Toasts';

/**
 * Class creation page
 */
interface PageProps {
    activeCourse?: string
}

function ClassCreator(props: PageProps) {
    const history = useHistory();

    const { activeCourse } = props; //unbind props

    if (!activeCourse) {
        history.push('/courses');
        return <div>No active course</div>
    }

    return (
        <Article>
            <ArticleTitle>Make a new class</ArticleTitle>
            <ClassForm activeCourse={activeCourse} />
        </Article>
    );
}

type Bus = {
    places: string,
    count: string
}

interface Props {
    activeCourse: string
}

/**
 * Page to create a Class with corresponding groups
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
function ClassForm(props: Props) {
    const { activeCourse } = props //unbind props

    const history = useHistory();

    //Name of the class
    const [name, setName] = useState("");

    const [busAssignment, setBusAssignment] = useState(true);
    const [busses, setBusses] = useState<Bus[]>([{ count: "1", places: "6" }])
    const [driverAmount, setDriverAmount] = useState(0);

    const [activeIndex, setActiveIndex] = useState(0);
    const [groups, setGroups] = useState<Group[]>([]);
    const setActiveClass = browsingStore((state) => state.setActiveClass);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    const groupAdd = useMutation((newGroup: Group) => postGroup(newGroup), {
        onSuccess: (id, newGroup) => {
            queryClient.invalidateQueries('groups')
            for (let i = 0; i < newGroup.members.length; i++) {
                if (id) {
                    RegisterByMail({
                        auth_level: AuthLevel.Student,
                        class_id: newGroup.class_id,
                        email: newGroup.members[i],
                        user_group_id: id,
                    })
                }
            }
        }
    })
    const classAdd = useMutation((newClass: Class) => postClass(newClass), {
        onSuccess: (data) => {
            if (data) {
                //Invalids the current list of pins so it retrieves again
                queryClient.invalidateQueries('classes');
                setActiveClass(data);

                SuccessToast('New class created')

                //Navigates to the new class
                history.push("/Class/" + data);

                //Uploads groups to the new class.
                for (let i = 0; i < groups.length; i++) {
                    groups[i].class_id = data;
                    groupAdd.mutate(groups[i]);
                }
            }
        }
    });

    const AssignBusses = (event: React.FormEvent) => {
        //Make sure there is a class name filled in.
        if (name !== "") {
            //You can make the field blank which isn't easily fixable. So if the field is empty we set the value to 0
            busses.map((bus, busIdx) => {
                if (!parseInt(bus.count)) {
                    let copy = [...busses];
                    copy[busIdx].count = "0";
                    setBusses(copy);
                }
                if (!parseInt(bus.places)) {
                    let copy = [...busses];
                    copy[busIdx].places = "0";
                    setBusses(copy);
                }
                return null;
            })
            //TODO No empty class name

            //Build the necesarry groups
            let index: number = 0;
            let membersToSet: string[] = [];
            let groupsToBe = groups;
            for (let i = 0; i < driverAmount; i++) {
                membersToSet.push("");
            }
            busses.map((bus, busIdx) => {
                let count = parseInt(bus.count);
                let places = parseInt(bus.places);
                for (let i = 0; i < count; i++) {
                    groupsToBe.push({
                        id: ((index + i).toString()),
                        name: "Group " + (index + i + 1),
                        maxMembers: places,
                        members: membersToSet,
                        class_id: "-1",
                    })
                }
                index += count;
                return null;
            })
            setGroups(groupsToBe);

            //Set to false since no longer doing busses
            setBusAssignment(false);

            //Check if any busses
            let count = 0;
            for (let i = 0; i < busses.length; i++) {
                count += parseInt(busses[i].count);
            }
            if (count === 0) {
                classAdd.mutate({ id: activeCourse, name: name, courseId: activeCourse })
            }
        } else {
            WarningToast('Please fill in a name for the class');
        }
    }

    //Add a bus variant
    const AddBusType = (e: React.MouseEvent) => {
        e.preventDefault();
        const copy = [...busses];
        copy.push({ count: "1", places: "6" });
        setBusses(copy);
    }
    //Change places for a bus variant
    const changeBusPlaces = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const copy = [...busses];
        copy[index].places = (e.target.value);
        setBusses(copy);
    }
    //Change amount of avaible bus from variant
    const changeBusCount = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const copy = [...busses];
        copy[index].count = e.target.value;
        setBusses(copy);
    }
    //Change a driver field
    const editDriver = useCallback((e: React.ChangeEvent<HTMLInputElement>, groupIdx: number, memberIdx: number) => {
        const copy = [...groups];
        const membersCopy = [...copy[groupIdx].members];
        membersCopy[memberIdx] = e.target.value;
        copy[groupIdx].members = membersCopy;
        setGroups(copy);
    }, [groups])
    //Remove a driver field
    const removeDriver = useCallback((e: React.MouseEvent, groupIdx: number, memberIdx: number) => {
        const copy = [...groups];
        const membersCopy = [...copy[groupIdx].members];
        membersCopy.splice(memberIdx, 1);
        copy[groupIdx].members = membersCopy;
        setGroups(copy);
    }, [groups])
    //Add a driver field
    const addDriver = useCallback((e: React.MouseEvent, groupIdx) => {
        const copy = [...groups];
        const membersCopy = [...copy[groupIdx].members];
        membersCopy.push("");
        copy[groupIdx].members = membersCopy;
        setGroups(copy);
    }, [groups])
    //Display previous group
    const previousGroup = (e: React.MouseEvent) => {
        e.preventDefault();
        if (activeIndex > 0) {
            setActiveIndex(activeIndex - 1)
        }
    }
    //Display next group
    const nextGroup = (e: React.MouseEvent) => {
        e.preventDefault();
        setActiveIndex(activeIndex + 1)
    }
    //Finish making the new Class
    const Done = () => {
        console.log(groups);
        //Add everything to server and redirect
        classAdd.mutate({ name: name, id: activeCourse, courseId: activeCourse });
    }

    if (busAssignment) {
        /*
        * Bus Assignment form
        */
        return (
            <FakeForm>
                <Input placeholder="Class name" value={name} onChange={(e) => setName(e.target.value)} data-testid="className"/>
                {busses.map((bus, busIdx) =>
                    <div key={busIdx}>
                        <Label>Bus variant {busIdx + 1}:</Label>
                        <BusDisplay>
                            <Label htmlFor="busPlaces">Amount of places in the bus:</Label>
                            <CustomNumberField type="number" min="0" step="1" value={bus.places}
                                onChange={e => changeBusPlaces(e, busIdx)} data-testid="busPlaces" />
                            <Label htmlFor="busCount">How many of this bus variant available:</Label>
                            <CustomNumberField type="number" min="0" step="1" value={bus.count}
                                onChange={e => changeBusCount(e, busIdx)} data-testid="busCount" />
                        </BusDisplay>
                    </div>
                )}
                <FlexButton onClick={AddBusType} customColor={theme.mainYellow} alignment="center">Add bus
                    variant</FlexButton>
                <Label>Amount of drivers which are also team members in each bus:</Label>
                <NumberField type="number" min="0" step="1" value={driverAmount}
                    onChange={e => setDriverAmount(parseInt(e.target.value))} data-testid="driverAmount" />
                <SubmitButton type="submit" onClick={AssignBusses}>Confirm</SubmitButton>
            </FakeForm>
        );
    } else {
        /*
        * Driver Assignment form
        */
        return (
            <FakeForm>
                <Input placeholder="Class name" value={name} onChange={(e) => setName(e.target.value)} />
                {groups.map((group, groupIdx) => {
                    if (groupIdx !== activeIndex) {
                        return null
                    }
                    return (
                        <div key={groupIdx}>
                            <FormHeader>{group.name} with a maximum of {group.maxMembers} members</FormHeader>
                            {group.members.map((member, memberIdx) =>
                                <DriverField key={memberIdx}>
                                    <Label>Driver {memberIdx + 1}:</Label>
                                    <StyledInput required placeholder="An email" value={member}
                                        onChange={e => editDriver(e, groupIdx, memberIdx)}
                                        data-testid="driverName" />
                                    <DelButtonImage src={delButtonImg} alt="remove button"
                                        onClick={e => removeDriver(e, groupIdx, memberIdx)} />
                                </DriverField>
                            )}
                            <FlexButton customColor={theme.mainYellow} alignment="center"
                                onClick={e => addDriver(e, groupIdx)}>Add Driver</FlexButton>
                        </div>
                    );
                })

                }
                <br />
                <StyledButton onClick={previousGroup}>Previous</StyledButton>
                {(activeIndex + 1) === groups.length ? <StyledButton onClick={Done}>Finish</StyledButton> :
                    <StyledButton onClick={nextGroup}>Next</StyledButton>}
            </FakeForm>
        );

    }
}

//TODO resizing goes badly fix
const DriverField = styled.div`
  display: grid;
  margin: 1% 25%;
  grid-template-columns: 15% 80% 5%;
`
const BusDisplay = styled.div`
  display: grid;
  margin: 1% 35%;
  grid-template-columns: 80% 20%;
  column-gap: 3%;
  row-gap: 2%;

`
const StyledInput = styled.input`
  border-radius: 3px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border: 1px solid grey;
  outline: none;
  color: black;
  background: #FFFFFF;
`
const CustomNumberField = styled.input`
  border-radius: 3px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  padding: 5px;
  border: 1px solid grey;
  outline: none;
  color: black;
  margin: 1%;
  background: #FFFFFF;
  place-self: center;
  text-align: center;
`
const StyledButton = styled(SubmitButton)`
  display: inline;
`

export default ClassCreator;