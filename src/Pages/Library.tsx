/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React from 'react';
import { SampleLibraryData } from '../Components/Library/SampleEntry';
import { StyledLink } from '../Components/Library/StyledLibComponents';
import LibOverview from '../Components/Library/LibOverview';

interface Props {
  //Is just short for properties which each Component must have which are Read-Only
}

/**
 * Empty placeholder
 */
function handleOnClick() {
  
}

/**
 * Gets new ID for new Library Entry from server / create new entry on server and request ID, to pass to editor
 * @returns new Entry Id
 */
function getNewId() : string {
  //TODO: get new ID for new Library Entry from server / create new entry on server, and request ID

  return '1';
}

/**
 * Retrieves all library entries from server, renders a table of these
 * @returns Library Main Page JSX
 */
function Library(props: Props) {
  
  return (
    <div>
        <h1>Library</h1>
        <StyledLink 
          type={'button'}
          to={'/LibEntryEditor/'+ getNewId()}
          role={'newLibEntryButton'}
          onClick={() => handleOnClick()}
        >New Entry</StyledLink>

        <LibOverview data={SampleLibraryData}/>
    </div>
  );
}

export default Library