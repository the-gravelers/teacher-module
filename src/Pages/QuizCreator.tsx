/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react';
import styled from 'styled-components';
import {Article} from '../Components/StyledComponents'
import {FakeForm, FormHeader, Input, Label} from '../Components/FormComponents'
import {FlexButton} from '../Components/FlexListComponents';
import {theme} from '../Theme';
import {Question} from '../Components/Question';

export type QuestionState = {
    text: string,
    answers: string[],
    corAnswer: string
}

//Always return a new empty question object
const emptyQuestion = (): QuestionState => {
    return ({
        text: "",
        answers: ["", "", ""],
        corAnswer: ""
    })

}

//TODO Fix some Styling details

interface Props {

}

/**
 * Page where you can create a quiz. (Cannot send to server yet)
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
function CreateQuiz(props: Props) {

    const [activeIndex, setIndex] = useState(0);
    const [questions, setQuestions] = useState<QuestionState[]>([emptyQuestion()]);

    const QuestionList = questions.map((question, qId) =>
        <div key={qId}>
            <FlexButton alignment="left" customColor={theme.mainRed} data-testid="removeQuestion" onClick={_e => {
                let copyQuiz = [...questions];
                if (copyQuiz.length > 1) {
                    copyQuiz.splice(qId, 1);
                    if (activeIndex > copyQuiz.length - 1 || activeIndex > qId) {
                        setIndex(activeIndex - 1);
                    }
                }
                setQuestions(copyQuiz);
            }}>-</FlexButton>
            <FlexButton alignment="right" customColor={theme.mainYellow}
                        onClick={(_e) => setIndex(qId)}>Question {qId + 1}</FlexButton>
        </div>
    );
    return (
        <Article>
            <SideBar>
                <FormHeader>Question {activeIndex + 1} / {questions.length}</FormHeader>
                {QuestionList}
                <FlexButton alignment="right" customColor={theme.mainItemColor} onClick={(_e) => {
                    let copyQuiz = [...questions];
                    copyQuiz.push(emptyQuestion());
                    setIndex(copyQuiz.length - 1);
                    setQuestions(copyQuiz);
                }}>+</FlexButton>
            </SideBar>
            <RightSide>
                <FormHeader>Create Quiz</FormHeader>
                <FakeForm>
                    <Input placeholder="Title quiz" data-testid="quizTitle"/>
                    <Label>Question: {activeIndex + 1}</Label>
                    {questions.map((question, idx) => {
                        //Only display the active question
                        if (idx !== activeIndex) {
                            return null
                        }
                        return (
                        <Question key={idx} setQuestions={setQuestions} index={idx} questions={questions}/>
                        )
                    })}<br/>
                    <FlexButton alignment="center" customColor={theme.mainYellow} onClick={(_e) => {/* TODO Implement upload quiz */
                    }}>Finish Quiz</FlexButton>
                </FakeForm>
            </RightSide>
        </Article>
    );
}

const SideBar = styled.ul`
  padding-top: 20px;
  float: left;
  height: 84vh; //TODO calc this
`
const RightSide = styled.div`
  padding-top: 20px;
  align-content: center;
  float: center;
`

export default CreateQuiz;