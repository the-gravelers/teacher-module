/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useState, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import LibEntryEditComponent from '../Components/Library/LibBookEditComponent';
import { LibraryBook } from '../Types';

interface Props {
    
}

/**
 * Retrieves LibraryData from server, based on EntryId passed by URL. Holds this data as its state, and re-renders 
 * (its changed components) on all updates.
 * @returns Library Entry Editor JSX, containing the data of this entry. 
 */
function LibEntryEditor(props: Props) {
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { entryId } = useParams() as any as { entryId: number };
    
    //TODO: retrieve data from server, based on EntryId passed by URL
    const [data, setData] = useState<LibraryBook>(NewLibraryBook());

    const forceUpdate = useCallback((newData: LibraryBook) => setData(newData), []);

    return(
        <LibEntryEditComponent key={"LibEntryEditor"} data={data} setBook={forceUpdate}></LibEntryEditComponent>
    );
}

/**
 * An empty Library book
 * @returns a LibraryBook with ID = 0, and other properties empty
 */
function NewLibraryBook() : LibraryBook {
    const result : LibraryBook = {
        id: 0, 
        title: "", 
        publishedBy: "", 
        lastEdited: 0, 
        sections: []
    }

    return result;
}

export default LibEntryEditor;