/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useState } from 'react';
import { Article, ArticleTitle } from '../Components/StyledComponents'
import { DelButtonImage, FakeForm, Input, Label, NumberField } from '../Components/FormComponents'
import { AuthLevel, Group } from '../Types';
import { FlexButton } from '../Components/FlexListComponents';
import { theme } from '../Theme';
import delButtonImg from '../Images/minus_centered.png';
import { GridLink } from '../Components/GridComponents';
import { browsingStore } from '../Stores/BrowsingStore';
import { postGroup } from '../Server/GroupsRequests';
import { useMutation, useQueryClient } from 'react-query';
import { ErrorToast, SuccessToast, WarningToast } from '../Components/Toasts';
import { RegisterByMail } from '../Server/RegisterRequests';

interface Props {

}

/**
 * Page where you can register a new Group for a Class
 * @param _props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
function GroupCreator(_props: Props) {

    const [drivers, setDrivers] = useState<string[]>([]);
    const [maxMembers, setMaxMembers] = useState(6);

    const activeClass = browsingStore((state) => state.activeClass);
    const amountOfGroups = browsingStore((state) => state.amountOfGroups);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    const groupPost = useMutation((newGroup: Group) => postGroup(newGroup), {
        onSuccess: (id) => {
            SuccessToast('Group created');
            queryClient.invalidateQueries('groups');
            if (id && activeClass) {
                for (let i = 0; i < drivers.length; i++) {
                    RegisterByMail({
                        auth_level: AuthLevel.Student,
                        class_id: activeClass,
                        email: drivers[i],
                        user_group_id: id,
                    })
                }
            }
            else if (!activeClass) {
                ErrorToast('No active class so could not register accounts');
            }
            else {
                ErrorToast('Could not create user since incorrect server response')
            }
        }
    })

    //Submit the newGroup to the server
    const handleSubmit = (_event: React.FormEvent) => {
        if (isNaN(maxMembers)) {
            setMaxMembers(0);
        }
        if (activeClass) {
            const newGroup: Group = {
                name: "Group: " + (amountOfGroups + 1),
                members: drivers,
                maxMembers: maxMembers,
                id: "-1",
                class_id: activeClass,
            }
            groupPost.mutate(newGroup);
        } else {
            WarningToast("No class selected");
        }

        //TODO create members account

    }
    //Is called to change the driver inputfield.
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>, index: number) => {
        let copy = [...drivers];
        copy[index] = event.target.value;
        setDrivers(copy);
    }
    //Is called to add a driver to the new group
    const addDriver = (_event: React.MouseEvent) => {
        let copy = [...drivers];
        copy.push("");
        setDrivers(copy);
    }
    //Is called to remove a driver from the new group
    const removeDriver = (_event: React.MouseEvent, memberIdx: number) => {
        const copy = [...drivers];
        copy.splice(memberIdx, 1);
        setDrivers(copy);
    }

    return (
        <Article>
            <ArticleTitle>Making a new group</ArticleTitle>
            <FakeForm>
                <Label htmlFor="maxMembers">Maximum number of members in group:</Label>
                <NumberField type="number" min="0" step="1" value={maxMembers}
                    onChange={e => setMaxMembers(parseInt(e.target.value))} data-testid="maxMembers" />
                {drivers.map((driver, driverIdx) => {
                    return (
                        <div key={driverIdx}>
                            <Label>Driver {driverIdx + 1}:</Label>
                            <Input required placeholder="An email" value={driver}
                                onChange={e => handleChange(e, driverIdx)} data-testid="driverField" />
                            <DelButtonImage src={delButtonImg} alt="remove button"
                                onClick={e => removeDriver(e, driverIdx)} />
                        </div>
                    );
                })}
                <br />
                <FlexButton customColor={theme.mainYellow} alignment="center" onClick={addDriver}>Add
                    Driver</FlexButton>
                <br /> <br />
                <GridLink to={"/Class/" + activeClass} onClick={handleSubmit}
                    backgroundcolor={theme.mainYellow}>Confirm</GridLink>
            </FakeForm>
        </Article>
    );
}

export default GroupCreator;