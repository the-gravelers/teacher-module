/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useState } from 'react';
import styled from 'styled-components';
import { theme } from '../Theme'
import { Article } from '../Components/StyledComponents'
import { FakeForm, FormHeader, Input, Logo } from '../Components/FormComponents';
import { useHistory } from 'react-router-dom';
import appLogo from '../Images/UCE_logo.png';
import { ForgotPassword, login } from '../Server/LoginRequests';
import { PopUpWindow } from '../Components/PopUpWindow';

interface Props {

}

/**
 * Page to Login and get authentification token.
 * @param props empty props to make it obvious that it's an functional component
 * @returns A JSX element
 */
function Login(props: Props) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [display, setDisplay] = useState<'none' | 'block'>('none');
    const history = useHistory();

    /**
     * Submits the data in the input fields and tries to authenticate you
     * @event e mouse event of clicking on the button
     */
    async function submit(e: React.MouseEvent<HTMLButtonElement>) {
        const succes: boolean = await login(email, password);
        if (succes) {
            //Link to main page if logged in
            history.push('/Courses');
        }
    }

    function DisplayPopUp(event: any) {
        event.stopPropagation();
        setDisplay('block');
    }

    function HidePopUp(event: any) {
        event.stopPropagation();
        setDisplay('none')
    }

    return (
        <Article onClick={e => HidePopUp(e)}>
            <FakeForm >
                <PopUpWindow DisplayPopUp={DisplayPopUp} HidePopUp={HidePopUp} display={display} size={{width: 400, height: 100}}>
                    <div>Enter your email:</div>
                    <Input name="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" />
                    <SubmitButton onClick={e => {
                        ForgotPassword(email);
                        HidePopUp(e);
                    }}>Send</SubmitButton>
                </PopUpWindow>
                <Logo src={appLogo} alt="The app logo" />
                <FormHeader>Login</FormHeader>
                <Input name="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email"
                    data-testid="email" />
                <br />
                <Input name="password" type="password" value={password} onChange={e => setPassword(e.target.value)}
                    placeholder="Password" data-testid="password" />
                <SubmitButton onClick={submit}>Login</SubmitButton>
                <TinyLink onClick={e => DisplayPopUp(e)}>Forgot password?</TinyLink>
            </FakeForm>
        </Article>
    );
}

const SubmitButton = styled.button`
    background-color: ${theme.mainYellow};
    color: ${theme.mainBlack};
    display: block;
    width: 100px;
    margin: 3px auto;
    padding: 3px;
    font-size: 1.17em;
    font-weight: bold;
    float: center;
    text-decoration: none;
    cursor: pointer;
    border: none;
    align-self: center;
    border-radius: 5px;
    border: 1px solid lightgrey;
`

const TinyLink = styled.div`
    text-align: center;
    font-weight: bold;
    :hover {color: blue;}
`

export default Login