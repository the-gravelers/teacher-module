/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import React, {useState} from 'react';
import {Article} from '../Components/StyledComponents'
import {FakeForm, FormHeader, Input, Logo, SubmitButton} from '../Components/FormComponents';
import appLogo from '../Images/UCE_logo.png';
import {useParams} from 'react-router-dom';
import { RegisterUser } from '../Server/RegisterRequests';

interface Props {

}

/**
 * Page to Register a student account (Not done)
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
function Register(props: Props) {
    //gets the id from the URL
    const {classId} = useParams() as any as { classId: string };

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [verifyPassword, setVerifyPassword] = useState("");

    return (
        <Article>
            <FakeForm>
                <Logo src={appLogo} alt="The app logo"/>
                <FormHeader>Register</FormHeader>
                <Input value={name} onChange={e => setName(e.target.value)} placeholder="Full name" data-testid="fullName"/>
                <br/>
                <Input value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" data-testid="email"/>
                <br/>
                <Input type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Password" data-testid="password"/>
                <br/>
                <Input type="password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)}
                       placeholder="Repeat your password" data-testid="verifyPassword"/>
                <SubmitButton onClick={_e => RegisterUser({
                    class_id: classId,
                    name: name,
                    email: email,
                    password: password,
                    verify_password: verifyPassword,
                })}>Make account</SubmitButton>
            </FakeForm>
        </Article>
    );
}

export default Register