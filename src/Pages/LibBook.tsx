/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
/* eslint-disable jsx-a11y/aria-role */
import { LibrarySection } from '../Types';
import SectionComponent from '../Components/Library/SectionComponent';
import React, { useState } from 'react';
import { EntryDiv } from '../Components/Library/StyledLibComponents';

import { useParams } from 'react-router-dom';

import { SampleEntry } from '../Components/Library/SampleEntry';

interface Props{
    //Is just short for properties which each Component must have which are Read-Only
}

/**
 * Retrieves LibraryData from server, based on EntryId passed by URL
 * @returns Library Entry JSX, containing all LibraryData
 */
function LibEntry(props: Props) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { entryId } = useParams() as any as { entryId: number };

    //TODO: retrieve data from server
    const dummyData = SampleEntry();

    const [title, ] = useState<string>(dummyData.title);
    const [sections, ] = useState<LibrarySection[]>(dummyData.sections);

    return (
        <EntryDiv role={'entry'}>
            <h2 role={'entryTitle'}>{title}</h2>
                {sections.map((section, i) => <SectionComponent section={section}
                                                                key={i}
                                              ></SectionComponent>)}
        </EntryDiv>
    );
}

export default LibEntry