/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useEffect, useState } from 'react';
import { FakeForm, Input } from '../Components/FormComponents';
import { Article, ArticleTitle } from '../Components/StyledComponents';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Course } from '../Types';
import { postCourse, updateCourse } from '../Server/CourseRequests';
import { useMutation, useQueryClient } from 'react-query';
import { SuccessToast, WarningToast } from '../Components/Toasts';

interface Props {
    courseId?: string,
    name?: string
}

/**
 * Page where you can create a new course or change an existing one
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
export function CourseManager(props: Props) {

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    const [name, setName] = useState<string>("");

    //If changing existing course change the state
    useEffect(() => {
        if (props.name)
            setName(props.name);
    }, [props.courseId, props.name])

    //Add a course to the server
    const courseAdd = useMutation((name: string) => postCourse(name), {
        onSuccess: () => {
            queryClient.invalidateQueries('courses');
            SuccessToast('Course created')
        }
    })

    //Change a course on the server
    const courseUpdate = useMutation((changedCourse: Course) => updateCourse(changedCourse), {
        onSuccess: () => {
            queryClient.invalidateQueries('courses');
            SuccessToast('Course updated')
        }
    })

    //Submitting changes to the server on press
    function Submit(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
        if (name !== "") {
            if (props.courseId) {
                //If updating existing class
                courseUpdate.mutate({ name: name, id: props.courseId });
            } else {
                //When making new class
                courseAdd.mutate(name);
            }
        } else {
            e.preventDefault();
            WarningToast("Please fill in a name");
        }
    }

    return (
        <Article>
            <ArticleTitle>Course Creator</ArticleTitle>
            <FakeForm>
                <Input value={name} onChange={e => setName(e.target.value)} placeholder="Name of the course" data-testid="courseName" />
                <br /> <br />
                <StyledLink to="/Courses"
                    onClick={Submit}>{!props.courseId ? 'Create Course' : 'Update Course'}</StyledLink>
            </FakeForm>
        </Article>
    );
}

const StyledLink = styled(Link)`
  background-color: ${props => props.theme.mainYellow};
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  text-align: center;
  padding: 5px;
  margin: 1% 1%;
  border-radius: 8px;
  border: 1px solid lightgrey;
  color: ${props => props.theme.ButtonLinkColor};
`
