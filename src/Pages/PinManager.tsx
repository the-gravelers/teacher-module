/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
//React
import React, {ChangeEvent, useCallback, useEffect, useState} from 'react'
import {Link} from 'react-router-dom';
import styled from 'styled-components';
//Components
import {Article, ArticleTitle} from '../Components/StyledComponents';
import {FakeForm, Input, InputField, Label} from '../Components/FormComponents'
import {CreateText} from '../Components/CreateText';
import {CreateImage} from '../Components/CreateImage';
//Stores and more
import { Pin, PinContent, Utm } from '../Types'
import { browsingStore } from '../Stores/BrowsingStore';
import { addPin, sharePin, updatePin } from '../Server/PinRequests';
import { addPinContent, removePinContent } from '../Server/PinContentRequests';
import { useQueryClient, useMutation } from 'react-query';
import { SuccessToast, WarningToast } from '../Components/Toasts';

type shareType = {
    pinId: string,
    currentClass: string
}

interface Props {
    pin: Pin
}

/**
 * Page where you can create a pin or edit one and send it to the server.
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
function PinCreator(props: Props) {

    //Unbind the properties
    const {pin} = props;

    //states for Pin values

    // create a seperate form component
    // image handling in a seperate component

    const [pinTitle, setTitle] = useState<string>(pin.title);
    const [pinLat, setLat] = useState<string>(pin.lat);
    const [pinLong, setLong] = useState<string>(pin.lon);
    const [pinUtm, setPinUtm] = useState<Utm>({Easting: "", Northing: "", ZoneNumber: "",ZoneLetter: ""});
    const [pinText, setPinText] = useState<string>(pin.text);
    const [pinContent_str, setPinContent_str] = useState<string[]>([]);
    const [pinContent, setPinContent] = useState<PinContent[]>([]);
    const [coordUtm, setCoordUtm] = useState<Boolean>(false);
    //helper states
    const [filesSelected, setFilesSelected] = useState<File[]>([]);
    const activeClass = browsingStore((state) => state.activeClass);
    const oldContentIds : string[] = pinContent.map(x => x.contentId ?? "");
    console.log(oldContentIds);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    const setImages = useCallback((pincontent: PinContent[]) => {
        //set the URL strings of the files
        let list: string[] = [];
        for (let i = 0; i < pincontent.length; i++) {
            list.push(window.URL.createObjectURL(pincontent[i].content));
        }
        setPinContent_str(list);
    }, [setPinContent_str])

    useEffect(() => {
        setPinContent(pin.contents);
    }, [pin])

    //set Pin values to currentPin for edit pin
    useEffect(() => {
        if(pin.id !== "-1"){
            setImages(pin.contents);
            //setPinContent(pin.contents);
        } 
    }, [pin, setImages]);

    /** Share a pin with a class. */
    const pinShare = useMutation((content: shareType) => sharePin(content.pinId, "class", content.currentClass), {
        onSuccess: () => {
            SuccessToast('Shared pin with class')
            queryClient.invalidateQueries('pins');
        }
    });

    /** Add a pin to the server */
    const pinAdd = useMutation((pin: Pin) => addPin(pin), {
        onSuccess: (pinId) => { //if successfull return the pinId, the pinId is used to make the pincontent of that pin
            if (pinId) {
                //Invalids the current list of pins so it retrieves again
                queryClient.invalidateQueries('pins');
                if (activeClass) {
                    pinShare.mutate(({pinId: pinId, currentClass: activeClass}));
                } else {
                    WarningToast("No active class")
                }
                //upload pincontent
                for (let i = 0; i < filesSelected.length; i++) {
                    let PinContent1: PinContent = {
                        pinId: pinId,
                        contentId: pinId + i.toString() + 1,
                        content: filesSelected[i],
                        content_type: "image",
                        content_nr: i
                    }
                    pinContentAdd.mutate(PinContent1);
                }
                SuccessToast('Pin created')
            }
        }
    })

    /** Changes an existing pin on the server */
    const pinUpdate = useMutation((pin: Pin) => updatePin(pin), {
        onSuccess: () => {
            //Invalids the current list of pins so it retrieves again
            queryClient.invalidateQueries('pins');
            SuccessToast('Updated pin')
        }
    })

    const pinContentRemove = useMutation((contentId: string) => removePinContent(pin.id, contentId))

    /** Add pinContent to the server */
    const pinContentAdd = useMutation((pinContent: PinContent) => addPinContent(pinContent))

    const handleSubmit = (event: React.FormEvent) => {
        if (pinLat !== "" && pinLong !== "") {
            //console.log(utmCords);
            let Pin1: Pin = { //pinId = random value
                title: pinTitle,
                lat: pinLat,
                lon: pinLong,
                text: pinText,
                contents: pinContent,
                utm: pinUtm,
                last_edited: "",
                id: "-1"
            }
            //Make the changes on the server
            if (pin.id === "-1") {
                pinAdd.mutate(Pin1);
            } else {
                Pin1.id = pin.id;
                pinUpdate.mutate(Pin1);
                CheckContentChanges();
            }
        } else {
            //Stop the linking
            event.preventDefault();
            alert("Latitude and Longitude field can not be empty");
        }
    }
    const CheckContentChanges = useCallback(() => {
        const present: string[] = [];
        //Find which content is new and add it to the server
        for (let i = 0; i < pinContent.length; i++) {
            let id = pinContent[i].contentId
            if (!id) {
                let tempContent: PinContent = {
                    pinId: pin.id, contentId: undefined, content: filesSelected[i], content_type: "image", content_nr: i
                }
                pinContentAdd.mutate(tempContent)
            } else if (id) {
                present.push(id);
            }
        }
        //Check if any old content has been removed and remove it from the server
        console.log('oldContent');
        console.log(oldContentIds)
        console.log('newContent')
        console.log(pinContent)
        for (let i = 0; i < oldContentIds.length; i++) {
            if (!present.includes(oldContentIds[i])) {
                pinContentRemove.mutate(oldContentIds[i]);
            }
        }
    }, [pinContent, oldContentIds, pin.id, filesSelected, pinContentAdd, pinContentRemove])


    const handleAddImage = function (e: ChangeEvent<HTMLInputElement>) {
        const fileList = e.target.files;
        if (!fileList) return;

        const contentCopy : PinContent[] = [...pinContent];
        const tempFileList: File[] = [];
        //set files for append data in submit method
        for (let i = 0; i < fileList.length; i++) {
            tempFileList.push(fileList[i]);
            contentCopy.push({
                content: fileList[i],
                contentId: undefined,
                content_nr: i,
                content_type: 'image',
                pinId: pin.id,
            })
        }
        setFilesSelected(tempFileList);
        setPinContent(contentCopy);

        //create an URL for each file and add it to pinContent
        const values = [...pinContent_str];
        //let tempStringList: [string];
        let j = 0;
        let tempStringList = [values[0]];
        tempFileList.forEach(e => {
            tempStringList[j] = window.URL.createObjectURL(e);
            j++
        });
        setPinContent_str(tempStringList);
    }

    /**
     * Checks the input of Latitude and Longitude for validity.
     * @param e the onChange event of an Input field.
     * @param isLat wheter you want to change the Lat or Long
     */
    const onCoordsChange = function (e: ChangeEvent<HTMLInputElement>, isLat: boolean) {
        //Check that input only contains numbers, dots and minus-sign.
        if (e.target.value.match("^[0-9.-]*$") !== null) {
            const splitted = e.target.value.split('.');
            //Check that there are never more than 6 numbers behind coords
            if (splitted.length <= 1 || splitted[1].length <= 6) {
                if (isLat) {
                    const beforeComa = splitted[0];
                    //Allow empty or - sign
                    if (beforeComa === "" || beforeComa === "-") {
                        setLat(e.target.value);
                    } else {
                        const parsedValue = parseInt(beforeComa);
                        //Limit between 90 and -90
                        if (parsedValue <= 90 && parsedValue >= -90) {
                            setLat(e.target.value);
                        }
                    }
                } else {
                    const beforeComa = splitted[0];
                    //Allow empty or - sign
                    if (beforeComa === "" || beforeComa === "-") {
                        setLong(e.target.value);
                    } else {
                        const parsedValue = parseInt(beforeComa);
                        //Limit between 180 and -180
                        if (parsedValue <= 180 && parsedValue >= -180) {
                            setLong(e.target.value);
                        }
                    }
                }
            }
        }
    }
    /**
    * Checks the input of Latitude and Longitude for validity.
    * @param e the onChange event of an Input field.
    * @param isLat wheter you want to change the Lat or Long
    */
    const onUtmChange = function (e: ChangeEvent<HTMLInputElement>, type: string) {
        let east = pinUtm.Easting;
        let north = pinUtm.Northing;
        let number = pinUtm.ZoneNumber;
        let letter = pinUtm.ZoneLetter;
        let copy: string;
        if (type === "Easting"){
            copy = east;
            copy = e.target.value;
            setPinUtm({Easting:copy,Northing:north,ZoneNumber:number,ZoneLetter:letter});
        }
        else if (type === "Northing"){
            copy = north;
            copy = e.target.value;
            setPinUtm({Easting:east,Northing:copy,ZoneNumber:number,ZoneLetter:letter});
        }
        else if (type === "ZoneNumber"){
            copy = number;
            copy = e.target.value;
            setPinUtm({Easting:east,Northing:north,ZoneNumber:copy,ZoneLetter:letter});
        }
        else {
            copy = letter;
            copy = e.target.value;
            setPinUtm({Easting:east,Northing:north,ZoneNumber:number,ZoneLetter:copy});
        }
    }

    /**
     * Sets the visible pins
     * @event React.ChangeEvent is the onChange event of the select value
     */
    function changeCoords(e: React.ChangeEvent<HTMLSelectElement>) {
        var utmObj = require('utm-latlng');
        var utm = new utmObj(); //Default Ellipsoid is 'WGS 84'
        if(e.target.value === "Lat/Long"){
            setCoordUtm(false);
            let latLongCords = utm.convertUtmToLatLng(pinUtm.Easting, pinUtm.Northing, pinUtm.ZoneNumber, pinUtm.ZoneLetter);
            if(pinUtm.Easting === "" && pinUtm.Northing === "" && pinUtm.ZoneNumber === "" && pinUtm.ZoneLetter === ""){
                setLat("");
                setLong("");
            }
            else{
                let roundedLat = Number(latLongCords.lat).toFixed(6);
                let roundedLng = Number(latLongCords.lng).toFixed(6);
                setLat(roundedLat);
                setLong(roundedLng);
            }   
        } 
        else{
            setCoordUtm(true);
            let precision = 10;
            //var utmCords: Utm;
            let utmCords = utm.convertLatLngToUtm(pinLat, pinLong, precision);
            //console.log(utmCords);
            //console.log(pinLat);
            if(pinLat === "" && pinLong === "") setPinUtm({Easting:"", Northing:"", ZoneNumber:"", ZoneLetter:""});
            else setPinUtm(utmCords);
        }
    }

    return (
        <PinArticle>
            <EmptyDiv/>
            <PinArticleTitle>Make a pin</PinArticleTitle>
            <EmptyDiv/>
            <LeftBox>
                <SelectBox role="coordsConverter" onChange={changeCoords} data-testid="select">
                    <option value="Lat/Long">Lat/Long</option>
                    <option value="UTM">UTM</option>
                </SelectBox>
                {!coordUtm && <Label>Pin Latitude:</Label>}
                {!coordUtm && <Input placeholder="Lat" data-testid="lat" value={pinLat}
                                     onChange={e => onCoordsChange(e, true)}/>}
                {!coordUtm && <Label>Pin Longitude:</Label>}
                {!coordUtm && <Input placeholder="Long" data-testid="long" value={pinLong}
                                     onChange={e => onCoordsChange(e, false)}/>}
                {coordUtm && <UtmInput placeholder="Easting" data-testid="easting" value={pinUtm.Easting}
                                       onChange={e => onUtmChange(e, "Easting")}/>}
                {coordUtm && <UtmInput placeholder="Northing" data-testid="northing" value={pinUtm.Northing}
                                       onChange={e => onUtmChange(e, "Northing")}/>}
                {coordUtm && <Input placeholder="ZoneNumber" data-testid="zoneNumber" value={pinUtm.ZoneNumber}
                                    onChange={e => onUtmChange(e, "ZoneNumber")}/>}
                {coordUtm && <Input placeholder="ZoneLetter" data-testid="zoneLetter" value={pinUtm.ZoneLetter}
                                    onChange={e => onUtmChange(e, "ZoneLetter")}/>}
            </LeftBox>
            <Form2>
                <TitleInput placeholder="Title" data-testid="title" value={pinTitle}
                            onChange={e => setTitle(e.target.value)}/>
                <CreateText pinText={pinText} setPinText={setPinText}/>
                <Label htmlFor="file-uploader" data-testid="file-uploader">Add Image</Label>
                <InputField id="file-uploader" data-testid="file-uploader" type="file" accept="image" multiple onChange={e => handleAddImage(e)}/>
                <CreateImage content_str={pinContent_str} setContent_str={setPinContent_str} content={pinContent}
                             setContent={setPinContent}/>
            </Form2>
            <EmptyDiv/>
            <BottomBox>
                <CancelButton to={"/Class/" + activeClass} data-testid="cancel" type="button">Cancel</CancelButton>
                <ConfirmButton to={"/Class/" + activeClass} data-testid="submit" type="submit"
                               onClick={handleSubmit}>{pin.id === "-1" ? 'Create Pin' : 'Update Pin'}</ConfirmButton>
            </BottomBox>
        </PinArticle>
    );
}
export default PinCreator;

//setup PinArticle as grid
//columns consists of LeftBox 33% and Form2 67% 
//rows consists of PinArticleTitle and Form2, BottomBox is defined inside of Form2
const PinArticle = styled(Article)`
  display: grid;
  grid-template-columns: 25% 50% 25%;
  grid-template-rows: 10% 80% 10%;
  //grid-auto-rows: minmax(50px,auto);
  justify-items: stretch;
  align-content: stretch;
  row-gap: 0px;
`
const PinArticleTitle = styled(ArticleTitle)`
  align-content: start;
  justify-content: center;
`
const LeftBox = styled(FakeForm)`
  display: grid;
  justify-self: start;
  align-content: start;
  text-align: center;
`
const SelectBox = styled.select`
  text-align: center;
  margin: 15px;
  width: 45%;
  place-self: center;
`
const Form2 = styled(FakeForm)`
  text-align: center;
  display: inline-block;
  margin: 2px;
  padding: 3px;
`
const UtmInput = styled(Input)`
  width: 68%;
`
const BottomBox = styled.div`
  display: grid;
  margin: 5px;
  grid-column: 1/4;
  text-align: center;
  //justify-items:center;
  align-items: center;
  grid-template-columns:50% 50%;

`
//styling for Title in the grid, currently hardcoded
const TitleInput = styled(Input)`
  //width: 250px;
  width: 35%;
`
const EmptyDiv = styled.div`
  display: inline;
`

//styling for buttons
const CancelButton = styled(Link)`
  margin: 10px;
  padding: 3px 10px;
  color: grey;
  border: 2px solid grey;
  font-weight: bold;
  border-radius: 8px;
  width: 75px;
  justify-self: end;
`
const ConfirmButton = styled(Link)`
  margin: 10px;
  padding: 5px 10px;
  color: ${props => props.theme.ButtonLinkColor};
  background-color: ${props => props.theme.mainYellow};
  border: none;
  font-weight: bold;
  border-radius: 8px;
  width: 100px;
  justify-self: start;
`