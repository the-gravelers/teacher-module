/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Article } from '../Components/StyledComponents';
import { useParams } from 'react-router-dom';
import { PinList } from '../Components/PinList';
import { GroupList } from '../Components/GroupList';
import { browsingStore } from '../Stores/BrowsingStore';
import { SuccessToast } from '../Components/Toasts';
import { SubmitButton } from '../Components/FormComponents';
import { getUserData, updateUserData } from '../Server/UserRequests';
import { useQuery } from 'react-query';

interface Props {
}

/**
 * Page display of looking at an individual class with it's pins and groups
 * @param props empty props to make it obvious that it's an JSX component
 * @returns A JSX element
 */
export function ClassOverview(props: Props) {
  //gets the id from the URL
  const { classId } = useParams() as any as { classId: string };

  const setActiveClass = browsingStore((state) => state.setActiveClass);
  useEffect(() => {
    setActiveClass(classId);
  }, [classId, setActiveClass]);

  //The user info
  const { data: userInfo } = useQuery(['userInfo'], async () => {
    const temp = await getUserData();
    return temp;
  })

  return (
    <Article>
      <Grid>
        <TopSection>
          { userInfo && userInfo.class_id_str !== classId ?
            <SubmitButton onClick={_e => updateUserData(userInfo.id_str,classId)}>Make this your active class</SubmitButton>
          : <div>This is your active class</div>
          }
          <TinyLink
            onClick={() => {
              //TODO fix this link on actual deply
              //navigator.clipboard.writeText("https://uce-teacher-module.netlify.app/Register/" + classId)
              navigator.clipboard.writeText("localhost:3000/Register/" + classId)
              SuccessToast('Copied link to clipboard')
            }
            }
          >Invite students to this class
          </TinyLink>
        </TopSection>
        <SectionTitle area="header1">Pin list</SectionTitle>
        <LeftSection>
          <PinList classId={classId} />
        </LeftSection>

        <SectionTitle area="header2">Groups</SectionTitle>
        <RightSection>
          <GroupList classId={classId} />
        </RightSection>
      </Grid>
    </Article>
  );
}

const Grid = styled.div`
  margin: 0% 4%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas:
  "topsection topsection"
  "header1  header2"
  "main1    main2";
  column-gap: 20%;
  height: 100%;
`

const TopSection = styled.div`
  grid-area: topsection;
  display: grid;
  grid-template-columns: 1fr 1fr;
  text-align: center;
`
const TinyLink = styled.div`
  text-align: center;
  font-weight: bold;
  color: black;
  :hover {
    color: blue;
  }
`
const LeftSection = styled.section`
  grid-area: main1;
  align-content: start;
`
const RightSection = styled.section`
  grid-area: main2;
  align-content: start;
`
const SectionTitle = styled.h3<{ area: string }>`
  grid-area: ${props => props.area};
  text-align: center;
`
