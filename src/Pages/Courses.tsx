/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useState } from 'react';
import { Article } from '../Components/StyledComponents'
import { GridLink, InternalLink } from '../Components/GridComponents'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { theme } from '../Theme';
import { deleteCourse, getCourses } from '../Server/CourseRequests';
import { deleteClass, getClasses } from '../Server/ClassRequests';
import { browsingStore } from '../Stores/BrowsingStore';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { editStore } from '../Stores/EditStore';
import { DeleteButton } from '../Components/FlexListComponents';
import trashImg from '../Images/trash.png';
import { SuccessToast } from '../Components/Toasts';

interface Props {

}

/**
 * The courses of the university and the classes belonging to selected course
 * @param props empty props to make it obvious that it's an JSX Component
 * @returns A JSX element
 */
function Courses(props: Props) {
    const [activeIndex, setActiveIndex] = useState(0);

    //This is the global activeCourse get and setter
    const activeCourse = browsingStore((state) => state.activeCourse)
    const setActiveCourse = browsingStore((state) => state.setActiveCourse)

    //For editing a course name setting the course that should be edited
    const setEditCourse = editStore((state) => state.setEditCourse);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    //The courses from server
    const { data: courses } = useQuery(['courses'], async () => {
        const temp = await getCourses();
        if (temp && temp.length > 0) {
            setActiveCourse(temp[0].id);
        }
        return temp;
    })

    //The classes from server
    const { data: classes } = useQuery(['classes', activeCourse], async () => {
        if (courses) {
            return getClasses(courses[activeIndex].id);
        }
    }, {
        enabled: !!courses
    })

    //Remove course form the server
    const courseRemove = useMutation((courseId: string) => deleteCourse(courseId), {
        onSuccess: () => {
            SuccessToast("Course deleted");

            //Invalids the current list of courses so it retrieves again
            queryClient.invalidateQueries('courses');
        }
    })

    //Remove classes form the server
    const classRemove = useMutation((classId: string) => deleteClass(classId), {
        onSuccess: () => {
            SuccessToast("Class deleted");
            
            //Invalids the current list of classes so it retrieves again
            queryClient.invalidateQueries(['classes', activeCourse]);
        }
    })

    /**
     * Set the activeIndex and activeCourse to the newly selected one.
     * @param courseId the id of the course to set active
     * @param courseIdx the new active index or place of course in array
     */
    function changeActiveCourse(courseId: string, courseIdx: number) {
        setActiveIndex(courseIdx);
        setActiveCourse(courseId);
    }

    return (
        <Article>
            <Grid>
                <Header area="header1">Course List</Header>
                <LeftSide>
                    {courses ? courses.map((x, courseIdx) =>
                        <CourseDisplay active={activeIndex === courseIdx}
                            key={courseIdx}
                            onClick={(_e) => changeActiveCourse(x.id, courseIdx)}>{x.name}
                        </CourseDisplay>) : 'Loading...'}
                </LeftSide>
                <Header area="header2">Classes</Header>
                <RightSide>
                    <ClassHolder>
                        {classes ? classes.map((x, classIdx) =>
                            <ClassItem key={classIdx}>
                                <InternalLink to={`/Class/${x.id}`}>{x.name}</InternalLink>
                                <TrashIt onClick={_e => classRemove.mutate(x.id)} src={trashImg} alt="removeTrashcan" />
                            </ClassItem>)
                            : 'Loading...'
                        }
                    </ClassHolder>
                    <ItemLink backgroundcolor={theme.mainYellow} to="/CreateClass">Add Class</ItemLink>
                </RightSide>
                <ButtonsHolder>
                    <GridLink backgroundcolor={theme.mainYellow} to="/Course" onClick={() => setEditCourse()}>New Course</GridLink>
                    {courses ?
                        <GridLink backgroundcolor={theme.mainYellow} to="/Course"
                            onClick={() => setEditCourse(courses[activeIndex].id, courses[activeIndex].name)}>Change
                            Name
                        </GridLink> : 'Loading...'}
                    <DeleteButton onClick={_e => {
                        if (activeCourse) {
                            courseRemove.mutate(activeCourse)
                        }
                    }} alignment={"left"}>Delete</DeleteButton>
                </ButtonsHolder>
            </Grid>
        </Article>
    );
}

const Grid = styled.div`
  margin: 0% 4%;
  display: grid;
  grid-template-areas:
  "header1 header2"
  "main1 main2"
  "footer .";
  grid-template-columns: 2fr 1fr;
  grid-template-rows: 10%;
  column-gap: 5%;
  height: 100%;
`
const Header = styled.h3<{ area: string }>`
  grid-area: ${props => props.area};
  text-align: left;
`
const LeftSide = styled.ul`
  grid-area: main1;
  display: grid;
  grid-template-columns: auto auto;
  row-gap: 1%;
  column-gap: 1%;
  overflow-y: auto;
  align-content: start;
  margin: 0px;
  padding: 0px;
`
const RightSide = styled.ul`
  grid-area: main2;
  display: grid;
  grid-template-areas:
  "mainright"
  "footerright";
  grid-template-columns: auto;
  grid-template-rows: auto;
  row-gap: 1%;
  overflow-y: auto;
  margin: 0px;
  padding: 0px;
`
const ButtonsHolder = styled.div`
  grid-area: footer;
  align-content: end;
  margin: 1% 0px;
  padding: 0px;
`
const CourseDisplay = styled.button<{ active: boolean }>`
  background-color: ${props => props.theme.mainItemColor};
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  cursor: pointer;
  outline: none;
  padding: ${props => props.active ? "8px" : "9px"};
  border-radius: 8px;
  border: ${props => props.active ? "2px solid #307D91" : "1px solid lightgrey"};
`
const ClassHolder = styled.div`
  grid-area: mainright;
  display: grid;
  grid-template-columns: auto;
  row-gap: 2%;
  overflow-y: auto;
  align-content: start;
  margin: 0px;
  padding: 0px;
`
const ItemLink = styled(Link) <{ backgroundcolor: string }>`
  grid-area: footerright;
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  text-align: center;
  padding: 5px;
  margin: 1% 1%;
  border-radius: 8px;
  border: 1px solid lightgrey;
  align-self: start;
  color: ${props => props.theme.ButtonLinkColor};
  background-color: ${props => props.backgroundcolor};
`
const ClassItem = styled.div`
  display: grid;
  background-color: ${theme.mainItemColor};
  padding: 5px;
  border-radius: 8px;
  border: 1px solid lightgrey;
  grid-template-columns: 1fr auto;
`
const TrashIt = styled.img`
  height: 100%;
  width: auto;
`

export default Courses;