
/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import {OmitNative, RouteProps} from "react-router";
import React, {useEffect, useState} from "react";
import {IsAuthorized} from "./Server/Authorization";
import {ErrorToast} from "./Components/Toasts";
import {Redirect, Route} from "react-router-dom";
import {omit} from "lodash";


type Props<T extends {} = {}, Path extends string = string> = RouteProps<Path> & OmitNative<T, keyof RouteProps>;

/**
 * Will generate the route for the given component
 * @param initialized a boolean wheter or not the child component 
 * @param props the props of a route component
 * @returns The routes and possibily if initialized the component
 */
function RenderRoute(initialized: boolean = false, props: Props) {
    let comp = null;
    if (initialized) {
        comp = props.component;
    }

    if (props.children) {
        return (
            <Route
                {...omit(props, ["component"])}
                // @ts-ignore
                component={comp}
            >
                {props.children}
            </Route>
        )
    } else {
        return (
            <Route
                {...omit(props, ["component"])}
                // @ts-ignore
                component={comp}
            />
        )
    }
}
/**
 * Same as a normal route but you need to be authenticated to access it.
 * @param props the properties which you would also give to a Route from react-router
 * @returns A route or a redirect to the login page if unauthenticated
 */
export function PrivateRoute(props: Props) {
    const [redirect, setRedirect] = useState<JSX.Element | null>(null);
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    useEffect(() => {
        (async () => {
            const isAuthorized = await IsAuthorized();

            if (!isAuthorized) {
                ErrorToast("Unauthorized")
                setRedirect(
                    <Redirect to={{pathname: "/Login"}}/>
                )
                return;
            }
            setIsAuthenticated(true);

        })();
    }, [])

    if (!!redirect) {
        return redirect;
    }

    // authenticated flow
    if (isAuthenticated) {
        return RenderRoute(true, props)
    }

    // initial (possible unauthenticated) flow
    return RenderRoute(false, props)
}
