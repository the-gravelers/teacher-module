/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

export type Course = {
    id: string,
    name: string,
}

export type Class = {
    id: string,
    name: string,
    courseId: string,
}

export type PinPreview = {
    id: string,
    title: string,
    text: string,
    last_edited: string,
}
export type Pin = {
  id: string,
  title: string,
  utm: Utm, //{string, string, string, string},
  lat: string,
  lon: string,
  last_edited: string,
  text: string,
  contents: PinContent[],
} 

export type Group = {
    id: string,
    name: string,
    members: string[],
    maxMembers: number,
    class_id: string,
    users?: User[],
}

export type Question = {
    id_str: string,
    text: string,
    answers: string[],
    correctAnswer: string,
}

export type PinContent = {
    pinId: string,
    content_type: string, //ImageType | VideoType,
    content_nr: number,
    contentId: string | undefined,
    content: File,
}
type ContentType = "text" | "image";

export type TextType = {
    id: number,
    text: string,
    type: ContentType,
    index: number,
}

export type ImageType = {
    id: number,
    previewUrl: string,
    file: File,
    type: ContentType,
    index: number,
}

export type VideoType = {
    id: string,
    previewUrl: string,
    file: File,
    type: string,
    index: number,
}

export type LibraryTableData = {
    id: number,
    title: string,
    publishedBy: string,
    lastEdited: number,
}

export type LibrarySection = {
    id: number,
    index: number,
    title: string,
    content: (TextType | ImageType)[],
}

export type LibraryBook = {
    id: number,
    title: string,
    publishedBy: string,
    lastEdited: number,
    sections: LibrarySection[],
}

export type AuthToken = {
    access_token: string,
    expires_in: number,
    refresh_token: string,
    session_id_str: string,
    token_type: string,
}

export enum AuthLevel {
    Admin = 4,
    Teacher = 3,
    TeachingAssistant = 2,
    Student = 1
}

export type UserInfo = {
  id_str: string,
  name: string,
  email: string,
  auth_level: number,
  class_id_str: string,
}

export type User = {
    id_str: string,
    name: string,
}

export type Utm = {
  Easting: string,
  Northing: string,
  ZoneNumber: string,
  ZoneLetter: string,
}

export type RequestError = {
    error: string,
}