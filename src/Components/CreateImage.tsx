/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react'
import styled from 'styled-components';
import {FakeForm} from './FormComponents';
import Minus from '../Images/minus_red.png';
//import { activePinStore } from '../Stores/ActivePinStore';
import ArrowLeft from '../Images/arrow_left.png';
import ArrowRight from '../Images/arrow_right2.png';
import {PinContent} from '../Types';

interface CreateImageProps {
    content_str: string[]
    content: PinContent[]
    setContent_str: (whatever: string[]) => void
    setContent: (newContent: PinContent[]) => void
}

export function CreateImage(props: CreateImageProps) {
    const {setContent_str, setContent, content_str, content} = props;
    const [index, setIndex] = useState<number>(0);

    //remove the current image from pinContent
    function handleRemove(i: number) {
        //Set string image urls
        const values = [...content_str];
        values.splice(i, 1);
        setContent_str(values);

        //Set the actual content
        const copy: PinContent[] = [...content];
        copy.splice(i, 1);
        setContent(copy);

        if (i === (content_str.length - 1)) i--;
        setIndex(i);
    }

    //update the index to +1
    function nextImage(i: number) {
        //let newIndex = index;
        if (i < (content_str.length - 1)){ i++;
        setIndex(i);
        console.log(content_str);
        console.log(content_str.length);
        }
    }

    //update the index to -1
    function prevImage(i: number) {
        if (i > 0) {
            setIndex(i - 1);
        }
        console.log(content_str);
        console.log(content_str.length);
        console.log(content);
    }

    return (
        <ImageForm>
            <Div1 key={index}>
                {content_str.length > 1 &&
                <NextButton type="button" data-testid="prev"> <img src={ArrowLeft} alt="arrow_left" width="20"
                                                                   height="20"
                                                                   onClick={() => prevImage(index)}/></NextButton>}
                <img src={"" + content_str[index]} key={index} alt={""} style={{maxHeight: "160px"}}/>
                {content_str.length > 1 &&
                <NextButton type="button" data-testid="next" name="next"> <img src={ArrowRight} alt="arrow_right"
                                                                               width="20" height="20"
                                                                               onClick={() => nextImage(index)}/></NextButton>}
                {content_str.length > 0 &&
                <RemoveButton type="button"> <img src={Minus} alt="minus_button" width="20" height="20"
                                                  onClick={() => handleRemove(index)}/></RemoveButton>}
            </Div1>
        </ImageForm>
    );
}

const ImageForm = styled(FakeForm)`
    text-align:center;
    display:block;
    padding:3px;
    position:relative;
`
//Styling buttons
const RemoveButton = styled.button`
  margin: 3px;
  padding: 3px;
  border: none;
  background-color: transparent;
  vertical-align: top;
`
const NextButton = styled.button`
  margin: 3px;
  padding: 3px;
  border: none;
  background-color: transparent;
  align-items: center;
  vertical-align: 450%;
`
const Div1 = styled.div`
    font-size: 0;
    border: 0;
`