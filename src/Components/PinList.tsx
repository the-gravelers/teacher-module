/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { PinPreview } from '../Types';
import { CustomLink, DeleteButton, FlexButton } from './FlexListComponents';
import { theme } from '../Theme';
import { activePinStore } from '../Stores/ActivePinStore';
import { emptyPin, emptyPinPreview } from './EmptyPin';
import { getClassPins, getUserPins, removePin } from '../Server/PinRequests';
import { format, fromUnixTime } from 'date-fns';
import { PinDisplay } from './PinDisplay';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { CheckBox } from './CheckBox';
import SearchBar from './SearchBar';
import { SharePinModal } from './SharePinModal';
import { SuccessToast, WarningToast } from './Toasts';

type SelectOption = "All" | "Teacher" | "Class";

interface pinListProps {
    classId: string
}

/**
 * Displays a list of pins and the buttons to interact with them
 * @param props the class were you want to get the pins from
 * @returns A JSX element
 */
export function PinList(props: pinListProps) {
    const { classId } = props //Unbind the props

    //Keep track of all id's of currently selected pins
    const [selectedPins, setSelectedPins] = useState<string[]>([]);
    const [allChecked, setAllChecked] = useState<boolean>(false);
    const [display, setDisplay] = useState<'none' | 'block'>('none');

    //Get the shared state
    const setActivePin = activePinStore((state) => state.setActivePin)

    //Keeps track of which pins to show
    const [toShow, setToShow] = useState<SelectOption>("Class");

    const [pinList, setPinList] = useState<PinPreview[]>([emptyPinPreview()]);
    const [filterPinList, setFilterPinList] = useState<PinPreview[]>([emptyPinPreview()]);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();
    /**
     * Removes the selected pins from the server
     */
    async function RemoveSelected() {
        //Loops through all the id's and makes for each one server request to remove it
        for (let i = 0; i < selectedPins.length; i++) {
            pinRemove.mutate(selectedPins[i]);
        }
        //Resets boxes checked
        setSelectedPins([]);
    }

    //Mutation to remove pin form the server
    const pinRemove = useMutation((toRemove: string) => removePin(toRemove), {
        onSuccess: () => {
            //Invalids the current list of pins so it retrieves again
            queryClient.invalidateQueries('pins');
            SuccessToast('Pin removed');
        }
    })

    /**
     * Sets the visible pins
     * @event React.ChangeEvent is the onChange event of the select value
     */
    function changeViewMode(e: React.ChangeEvent<HTMLSelectElement>) {
        if (e.target.value === "All" || e.target.value === "Teacher" || e.target.value === "Class") {
            setToShow(e.target.value);
        }
    }

    /**
     * toggles all the checkboxes to the opposite state
     * @param pins the list of all the pins
     */
    function SelectAll(pins: PinPreview[]) {
        if (pins) {
            //If all things should be checked marked loop through them all and select them all
            if (allChecked) {
                //console.log("selecting all pins");
                const temp: string[] = [];
                for (let i = 0; i < pins.length; i++) {
                    temp.push(pins[i].id);
                }
                setSelectedPins(temp);
            }
            //Removes everything from selected pins
            else {
                //console.log("removing all pins");
                setSelectedPins([]);
            }
        }
    }

    function DisplayPopUp(event: any) {
        event.stopPropagation();
        setDisplay('block');
    }

    function HidePopUp(event: any) {
        event.stopPropagation();
        setDisplay('none')
    }

    return (
        <div>
            <SharePinModal display={display} DisplayPopUp={DisplayPopUp} HidePopUp={HidePopUp} selectedPins={selectedPins} classId={classId} />
            <Grid>
                <Header>
                    <SearchBar
                        alignment={'center'}
                        placeholder={'Search pins'}
                        data={pinList}
                        dataToShow={setFilterPinList}
                        data-testid={"search"}
                    />
                    <SelectAllBox><CheckBox id="selectAll" name="selectAll"
                        handleClick={(e) => setAllChecked(!allChecked)} isChecked={allChecked} />Select
                        All</SelectAllBox>
                    <SelectBox role="viewMode" onChange={changeViewMode}>
                        <option value="Class">Class</option>
                        <option value="Teacher">Teacher</option>
                        <option value="All">All</option>
                    </SelectBox>
                </Header>
                <PinGrid>
                    {PinsData(toShow, allChecked, classId)}
                </PinGrid>
                <ButtonHolder>
                    <CustomLink onClick={() => setActivePin(emptyPin())} to="/Pin">Add pin</CustomLink>
                    <DeleteButton alignment="left" name="Delete" onClick={(e) => RemoveSelected()}>Delete</DeleteButton>
                    <FlexButton onClick={e => selectedPins.length > 0 ? DisplayPopUp(e) : WarningToast('Please select pin(s)')} alignment="left" customColor={theme.mainYellow}>Share</FlexButton>
                </ButtonHolder>
            </Grid>
        </div>
    );

    function PinsData(toShow: SelectOption, allChecked: boolean, classId: string) {
        const { data: pins, isLoading, isError } = useFetchPins(toShow, classId);

        //If select all is check marked call function to set the boxes
        useEffect(() => {
            if (pins) {
                setPinList(pins);
                setFilterPinList(pins);
                SelectAll(pins);
            }
        }, [pins, allChecked])

        //If still loading
        if (isLoading) {
            return <div>Loading...</div>
        }
        //If error along the way
        if (isError) {
            return <div>Error getting groups</div>
        }

        /**
         * Add or removes an id from the list of selected pins
         * @event React.ChangeEvent change of state of checkbox
         * @param pinId the id of the pin to either remove or add
         */
        function ToggleChecked(e: React.ChangeEvent<HTMLInputElement>, pinId: string) {
            const { checked } = e.target;
            setSelectedPins([...selectedPins, pinId]);
            if (!checked) {
                setSelectedPins(selectedPins.filter(item => item !== pinId));
            }
        }

        return (
            pins && filterPinList.map((pin, pinIdx) =>
                <PinItem key={pin.id}>
                    <CheckBox id={pinIdx.toString()} name={pinIdx.toString()}
                        handleClick={(e) => ToggleChecked(e, pin.id)}
                        isChecked={selectedPins.includes(pin.id.toString())} />
                    <PinDisplay pin={pin} />
                </PinItem>
            )
        )

    }
}

export function useFetchPins(toShow: SelectOption, classId: string) {
    //Gets the pins from the server corresponding to the state
    return useQuery(['pins', toShow, classId], async () => {
        let rPins: PinPreview[] = [];
        if (toShow === "Teacher") {
            rPins = await getUserPins();
        } else if (toShow === "Class") {
            rPins = await getClassPins(classId, "class")
        } else if (toShow === "All") {
            //TODO (api) implement it but have to wait on API
            rPins = await getClassPins(classId, "class_all");
        }
        return fixDates(rPins);
    })
}

/**
 * Makes the last_edited dates of a list of pins readable for humans.
 * @param pins list of preview of pins.
 * @returns the same list of pins but with humanreadable dates.
 */
export function fixDates(pins: PinPreview[]): PinPreview[] {
    let copy = [...pins];
    for (let i = 0; i < copy.length; i++) {
        let parsedUnix = fromUnixTime(parseInt(copy[i].last_edited));
        const formatted_date = format(parsedUnix, 'dd/MM/yyyy HH:mm');
        copy[i].last_edited = formatted_date;
    }
    return copy;
}

const Grid = styled.div`
  display: grid;
  grid-template-columns: auto;
  grid-template-areas:
  "header"
  "main"
  "footer";
  align-content: start;
  overflow-y: auto;
  height: 78vh; //TODO Try to fix this terror
`
const Header = styled.div`
  grid-area: header;
  text-align: right;
  display: grid;
  grid-template-columns: 80% 20%;
`
const PinGrid = styled.div`
  grid-area: main;
  display: grid;
  align-content: start;
  overflow-y: auto;
  height: 100%;
`
const ButtonHolder = styled.div`
  grid-area: footer;
`
const PinItem = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  column-gap: 1%;
`
const SelectBox = styled.select`
  text-align: center;
`
const SelectAllBox = styled.div`
  text-align: left;
  display: grid;
  grid-template-columns: auto 1fr;
  column-gap: 1%;
`