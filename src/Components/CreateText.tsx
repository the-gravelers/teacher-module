/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';
import {TextField} from './FormComponents';
import React, {useCallback, useEffect} from 'react'
import {activePinStore} from '../Stores/ActivePinStore';

interface CreateTextProps {
    pinText: string
    setPinText: (whatever: string) => void
}

export function CreateText(props: CreateTextProps) {
    const {pinText, setPinText} = props;
    const activePin = activePinStore((state) => state.activePin);

    //set text to currentPin value for edit pin
    useEffect(() => {
        setPinText(activePin.text);
    }, [activePin, setPinText]);

    //update state after change in the textField
    const handleAddChange = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => {
        let copy = pinText;
        copy = event.target.value;
        setPinText(copy);
    }, [pinText, setPinText])

    return (
        <div>
            <TextField1
                placeholder="Text"
                data-testid="text"
                value={pinText}
                onChange={e => handleAddChange(e)}
            >
            </TextField1>
        </div>
    );
}

//fixed size of textfield
const TextField1 = styled(TextField)`
  margin: 5px;
  width: 450px;
  //width:55%;
  height: 250px;
`