/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';

type props = {
    id: string,
    name: string,
    handleClick: React.ChangeEventHandler<HTMLInputElement>,
    isChecked: boolean
}

/**
 * Input field which is always of the type checkbox with some styling
 * @param props id:string, name:string, handleClick on click event, isChecked boolean
 * @returns JSX Element
 */
export function CheckBox(props: props) {
    const {id, name, handleClick, isChecked} = props
    return (
        <StyledInput id={id} name={name} type="checkbox" onChange={handleClick} checked={isChecked}/>
    );
};

const StyledInput = styled.input`
  margin: auto 0px;
  outline: none;
  cursor: pointer;
  width: 20px;
  height: 20px;
`