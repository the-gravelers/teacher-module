import { useMemo } from "react";
import ReactDOM from "react-dom";
import { Helmet } from "react-helmet";
import styled from "styled-components";

interface props {
    children: JSX.Element[] | JSX.Element;
    display: 'none' | 'block',
    DisplayPopUp: (e: any) => void,
    HidePopUp: (e: any) => void,
    size: { width: number, height: number }

}
export function PopUpWindow(props: props) {
    const { children, display, DisplayPopUp, HidePopUp, size } = props;


    const modalRoot = useMemo(() => {
        const root = document.getElementById('modal-root') as HTMLElement
        if (root === null) {
            const body = document.getElementsByTagName('body')
            const div = document.createElement('div')
            body[0].appendChild(div);
            return div;
        }
        else {
            return root;
        }
    }, [])
    return (
        <div>
            {display === 'block' &&
                <Helmet>
                    <style>{'#root { filter: blur(3px); }'}</style>
                </Helmet>
            }
            {ReactDOM.createPortal(
                <Modal displayed={display} onClick={e => DisplayPopUp(e)} size={size}>
                    <CloseButton onClick={e => HidePopUp(e)}>&#10006;</CloseButton>
                    {children}
                </Modal>,
                modalRoot
            )}
        </div>
    );
}
const Modal = styled.div<{ displayed: 'none' | 'block', size: { width: number, height: number } }>`
    display: ${props => props.displayed};
    text-align:center;
    position:absolute;
    top:50%;
    left:50%;
    width:${props => props.size.width + 'px'};
    height:${props => props.size.height + 'px'};
    margin-left:${props => -(props.size.width / 2) + 'px'};  
    margin-top:${props => -(props.size.height / 2) + 'px'};  
    z-index: 100;
    background-color: white;
    border: 3px solid grey;
    filter: blur(0px);
    border-radius: 15px;

`
const CloseButton = styled.div`
    margin-right:3px;
    float:right;
    cursor:pointer;
    font-size:16px; font-weight:bold;
`