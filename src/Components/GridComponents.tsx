/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';
import {Link} from 'react-router-dom';

//Various styled html elements usually meant for Grids

export const GridLink = styled(Link)<{ backgroundcolor: string }>`
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  border: none;
  padding: 5px;
  float: right;
  text-align: center;
  border-radius: 8px;
  margin: 0px 1%;
  border: 1px solid lightgrey;
  color: ${props => props.theme.ButtonLinkColor};
  background-color: ${props => props.backgroundcolor};
`
export const InternalLink = styled(Link)`
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  color: ${props => props.theme.ButtonLinkColor};
`