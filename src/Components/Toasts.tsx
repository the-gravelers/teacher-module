/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {toast, ToastContainer as TContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const ToastContainer = () => {
    return (
        <TContainer
            autoClose={3000}
            hideProgressBar={true}
            newestOnTop={false}
            closeOnClick
            draggable
            pauseOnHover
        />
    );
}

export const SuccessToast = (text: string) => {
    toast.success(text);
}

export const WarningToast = (text: string) => {
    toast.warning(text);
}

export const ErrorToast = (text: string) => {
    toast.error(text);
}

export const InfoToast = (text: string) => {
    toast.info(text);
}