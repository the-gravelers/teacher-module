/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react';

/**
 * Will display the text if open below the header in a plain div
 * @param header any JSX.Element which should be the item you press.
 * @param text any string which will be the text displayed
 * @returns the header element and text if open otherwise just the header
 */
export function OpenableText(header: JSX.Element, text: string): JSX.Element[] {
    const [open, toggleOpen] = useState(false);
    return ([
        <div key={1} onClick={() => toggleOpen(!open)} style={{cursor: 'pointer'}}>
            {header}
        </div>,
        <div key={2}>{open && (text)}</div>
    ])
}