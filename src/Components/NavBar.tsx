/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';
import appLogo from '../Images/UCE_logo.png';
import {Link} from 'react-router-dom';

//TODO repsonsive to resizing make logo only icon and if really small remove

//This contains the top bar
function NavBar() {
    return (
        <NavWrap>
            <NavLogo to="/Login"> <Logo src={appLogo} alt="The app log"/></NavLogo>
            {/*<NavLogo src={appLogo} alt="The app logo" />*/}
            <NavLink to="/Courses">Courses</NavLink>
            <NavLink to="/Library">Library</NavLink>
        </NavWrap>
    );
}

//Below you will see styled components which are styled html elements
const NavWrap = styled.nav`
  background-color: ${props => props.theme.mainYellow};
  display: flex;
  list-style-type: none;
  margin: 0px;
  padding: 0px;
  width: 100%;
  height: 8vh;
`
const NavLogo = styled(Link)`
  margin-left: 1.5%;
  height: auto;
  width: auto;
  margin-right: auto;
`
const Logo = styled.img`
  height: 100%;
  width: auto;
`
const NavLink = styled(Link)`
  color: ${props => props.theme.ButtonLinkColor};
  font-size: 1.17em;
  margin: auto 1%;
  font-weight: bold;
  text-decoration: none;
`

//The export is so when someone imports they get this as default if they don't specify.
export default NavBar