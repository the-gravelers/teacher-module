/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import {Pin, PinContent, PinPreview} from '../Types';

export const emptyPin = (): Pin => {
    return ({
        id: "-1",
        title: "",
        utm: {Easting:"", Northing:"", ZoneNumber:"", ZoneLetter:""},
        lat: "",
        lon: "",
        text: "",
        contents: [],
        last_edited: ""
    })
}
export const emptyPinPreview = (): PinPreview => {
    return ({
        id: "-1",
        title: "",
        text: "",
        last_edited: ""
    })
}

const file = new File(['PlaceHolder'], 'PlaceHolder.png', {type: 'image/png'});
export const emptyPinContent = (): PinContent => {
    return ({
        pinId: "-1",
        contentId: "",
        content: file,
        content_type: "",
        content_nr: 0
    })
}