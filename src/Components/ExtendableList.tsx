/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react';
import styled from 'styled-components';

interface Props {
    children: JSX.Element[];
    header: JSX.Element;
}

/** Make a clickable item which will show/hide children when pressed
 * @param props give the parent element and array of children which will be displayed if extended
 * @returns given parent element and depending on state also the children.
 */
export function ExtendableList(props: Props) {
    const [open, toggleOpen] = useState(false);
    return (
        <Container>
            <div onClick={() => toggleOpen(!open)} style={{cursor: 'pointer'}}>
                {props.header}
            </div>
            {open && (
                props.children
            )}
        </Container>
    )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
`