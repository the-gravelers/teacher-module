import { useState } from "react";
import { useQuery } from "react-query";
import styled from "styled-components";
import { getClasses } from "../Server/ClassRequests";
import { getCourses } from "../Server/CourseRequests";
import { sharePin } from "../Server/PinRequests";
import { SubmitButton } from "./FormComponents";
import { PopUpWindow } from "./PopUpWindow";

interface Props {
    DisplayPopUp: (e: any) => void,
    HidePopUp: (e: any) => void,
    display: 'none' | 'block',
    selectedPins: string[],
    classId: string,
}

export function SharePinModal(props: Props) {
    //unbind props
    const { DisplayPopUp, HidePopUp, display, selectedPins, classId } = props;

    const [activeCourse, setActiveCourse] = useState<string | undefined>(undefined);

    const { data: courses } = useQuery(['courses'], async () => {
        const temp = await getCourses();
        return temp;
    })

    //The classes from server
    const { data: classes } = useQuery(['classes', activeCourse], async () => {
        if (activeCourse) {
            return getClasses(activeCourse);
        }
    }, {
        enabled: !!courses
    })

    function shareWith(event: React.MouseEvent<HTMLButtonElement, MouseEvent>, classId: string) {
        HidePopUp(event)
        for (let i = 0; i < selectedPins.length; i++) {
            sharePin(selectedPins[i], 'class', classId);
        }
    }

    return (
        <PopUpWindow DisplayPopUp={DisplayPopUp} HidePopUp={HidePopUp} display={display} size={{ width: 550, height: 400 }}>
            <Grid>
                <SubmitButton onClick={e => activeCourse ? setActiveCourse(undefined) : HidePopUp(e)}>Back</SubmitButton>
                <div>{activeCourse ? "Select a class to share with:" : "Select a course:"}</div>
                {activeCourse ?
                    classes ? classes.map((x, classIdx) =>
                        x.id !== classId &&
                        <Display key={classIdx} onClick={e => shareWith(e, x.id)}>
                            {x.name}
                        </Display>)
                        : 'Loading...'
                    :
                    courses ? courses.map((x, courseIdx) =>
                        <Display
                            key={courseIdx}
                            onClick={(_e) => setActiveCourse(x.id)}>{x.name}
                        </Display>) : 'Loading...'
                }
            </Grid>
        </PopUpWindow>
    );
}

const Grid = styled.div`
    display:grid;
    padding: 5px;
    margin: 5px;
    grid-template-columns: 1fr;

`
const Display = styled.button`
  background-color: ${props => props.theme.mainItemColor};
  font-size: 1.17em;
  font-weight: bold;
  margin: 3px;
  text-decoration: none;
  cursor: pointer;
  outline: none;
  padding: 8px;
  border-radius: 8px;
  border: "1px solid lightgrey";
`