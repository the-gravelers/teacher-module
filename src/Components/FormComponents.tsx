/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';
import {theme} from '../Theme'

//Various styled html elements usually meant for making Forms

export const Form = styled.form`
  flex-wrap: wrap;
  text-align: center;
  padding: 3px;
`
export const FakeForm = styled.div`
  text-align: center;
  padding: 3px;
`
export const Label = styled.label`
  display: block;
`
export const InputField = styled.input`
  margin: 5px;
  display: inline-block;
`
export const Input = styled(InputField)`
  border-radius: 3px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border: 1px solid grey;
  outline: none;
  color: black;
  padding: 5px 8px 5px 8px;
  background: #FFFFFF;
  width: 35%;
  place-self: center;
`
export const Wrapper = styled.div`
  background-color: white;
  border-radius: 20px;
`
export const Logo = styled.img`
  padding: 5% 5px 5px;
  width: auto;
  height: 80px;
`
export const SubmitButton = styled.button`
  background-color: ${theme.mainYellow};
  color: ${theme.mainBlack};
  display: block;
  margin: 3px auto;
  padding: 3px;
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  cursor: pointer;
  align-self: center;
  border-radius: 5px;
  border: 1px solid lightgrey;
`
export const NumberField = styled(Input)`
  text-align: center;
`
export const FormHeader = styled.h3`
  padding: 2px;
  margin: 0px;
  text-align: center;
`
export const RemoveButton = styled.button`
  border-radius: 15%;
  color: #ab4c5f;
  background-color: white;
  text-decoration: none;
  font-size: 1.2em;
  cursor: pointer;
`
export const TextField = styled.textarea`
  resize: none;
  border-radius: 3px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border: 1px solid grey;
  outline: none;
  color: black;
  padding: 5px 8px 5px 8px;
  background: #FFFFFF;
`
export const DelButtonImage = styled.img`
  height: 30px;
  width: 30px;
`