/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import styled from 'styled-components';

//Styled Article and beloning title which respect size of navbar
export const Article = styled.article`
  height: 92vh;
`
export const ArticleTitle = styled.h2`
  text-align: center;
  margin-top: 0px;
  padding-top: 5px;
`