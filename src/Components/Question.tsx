/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useCallback} from 'react';
import {Input, Label, RemoveButton, TextField} from './FormComponents';
import {FlexButton} from './FlexListComponents';
import {theme} from '../Theme';
import {QuestionState} from '../Pages/QuizCreator';

interface Props {
    index: number;
    questions: QuestionState[];
    setQuestions: (whatever: QuestionState[]) => void
}

type Event = React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLTextAreaElement>

export function Question(props: Props) {
    const {index, questions, setQuestions} = props; //Unbind the props
    const Question = questions[index];

    //Just used for less duplicate code for setText and setCorAnswer
    const HighOrder = useCallback((e: Event, func: (e: Event, question: QuestionState) => void) => {
        const copy = [...questions];
        const question = copy[index];
        func(e, question);
        setQuestions(copy);
    }, [index, questions, setQuestions]);

    //Sets the question text corresponding to the change
    const setText = (e: Event, question: QuestionState) => {
        question.text = e.target.value;
    }

    //Sets the text of the correct answer
    const setCorAnswer = (e: Event, question: QuestionState) => {
        question.corAnswer = e.target.value;
    }

    //Is called when you edit an field of a wrong answer. Keeps it up to date.
    const setWrongAnswers = useCallback((e: Event, answer: string) => {
        const copy = [...questions];
        const question = copy[index];
        const indexAnswer = question.answers.indexOf(answer);
        question.answers[indexAnswer] = e.target.value;
        setQuestions(copy);
    }, [index, questions, setQuestions])

    //Adds a possible wrong answer with the corresponding field
    const AddPosAnswer = useCallback((e: React.MouseEvent) => {
        e.preventDefault();
        const copy = [...questions];
        const question = copy[index];
        question.answers.push("");
        setQuestions(copy);
    }, [index, questions, setQuestions])

    //Removes a possible wrong anser with the corresponding field
    const RemovePosAnswer = useCallback((e: React.MouseEvent, toRemove: number) => {
        e.preventDefault();
        const copy = [...questions];
        const question = copy[index];
        if (question.answers.length > 1) {
            question.answers.splice(toRemove, 1);
        }
        setQuestions(copy);
    }, [index, questions, setQuestions])

    return (
        <div>
            <TextField placeholder={"Question"} required rows={5} cols={50} value={Question.text}
                       onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => HighOrder(e, setText)}
                       data-testid='question'/>
            <Label>Wrong answers</Label>
            {Question.answers.map((answer, answerIdx) =>
                <div key={answerIdx}>
                    <Input placeholder="Answer" value={answer} onChange={e => setWrongAnswers(e, answer)}
                           data-testid='wrongAnswer'/>
                    <RemoveButton id={answerIdx.toString()} onClick={e => RemovePosAnswer(e, answerIdx)} type="button"
                                  data-testid='removeWrongAnswer'>-</RemoveButton>
                </div>
            )}
            <FlexButton alignment="center" customColor={theme.mainItemColor} onClick={AddPosAnswer}>Add
                answer</FlexButton> <br/><br/>
            <Label>Correct answer:</Label>
            <Input placeholder="Answer" value={Question.corAnswer}
                   onChange={(e: React.ChangeEvent<HTMLInputElement>) => HighOrder(e, setCorAnswer)}
                   data-testid='correctAnswer'/>
        </div>
    );
}