/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import styled from 'styled-components';
import {Link} from 'react-router-dom';

//Various styled html elements meant for flexible lists.

export const FlexLinkItem = styled(Link)`
  background-color: ${props => props.theme.mainItemColor};
  color: ${props => props.theme.mainBlack};
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  align-self: center;
  padding: 3px 8px;
  margin: 3px;
  border-radius: 8px;
`
export const FlexButton = styled.button<{ customColor: string | null, alignment: string }>`
  font-size: 1.17em;
  font-weight: bold;
  float: ${props => props.alignment};
  text-decoration: none;
  cursor: pointer;
  border: none;
  align-self: center;
  padding: 5px 10px;
  margin: 3px;
  border-radius: 8px;
  outline: none;
  color: ${props => props.theme.buttonLinkColor};
  background-color: ${props => props.customColor || props.theme.mainYellow};
`
export const DeleteButton = styled.button<{ alignment: string }>`
  font-size: 1.17em;
  font-weight: bold;
  float: ${props => props.alignment};
  text-decoration: underline;
  cursor: pointer;
  border: none;
  align-self: center;
  padding: 2px 7px;
  margin: 3px;
  border-radius: 8px;
  outline: none;
  color: ${props => props.theme.mainRed};
  border: 3px solid;
  border-color: ${props => props.theme.mainRed};
`
export const CustomLink = styled(Link)`
  font-size: 1.17em;
  font-weight: bold;
  float: right;
  text-decoration: none;
  border: none;
  align-self: center;
  padding: 3px 8px;
  margin: 3px;
  border-radius: 8px;
  color: ${props => props.theme.ButtonLinkColor};
  background-color: ${props => props.theme.mainYellow};
`