/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React, {useEffect, useState} from 'react';
import searchIcon from '../Images/Loop_search.svg';
import styled from 'styled-components';

interface Props {
    //Is just short for properties which each Component must have which are Read-Only
    data: any[]
    dataToShow: (newData: any[]) => void
    alignment: 'center' | 'left'
    placeholder: string
}

/**
 *
 * @param data list of data to search. Elements must have {id: number} and {title: string} properties
 * @param dataToShow sets filtered data
 */
function SearchBar(props: Props) {
    //initialise
    const [, setFilteredData] = useState(props.data);
    useEffect(() => {
        setFilteredData(props.data)
    }, [props.data])

    //Do search
    function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault();

        const searchString = e.target.value.toLowerCase();

        const newData = filterData(searchString);

        setFilteredData(newData);
        props.dataToShow(newData);
    }

    function filterData(searchString: string): any[] {
        let newData = props.data;

        newData = newData.filter(d => (d.title.toLowerCase()).includes(searchString));
        return newData;
    }

    return (
        <Div>
            <Logo
                src={searchIcon}
                alt={searchIcon}
                key={"searchLogo"}
            />
            <SearchField alignment={props.alignment}
    placeholder={props.placeholder}
    onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleInputChange(e)}
    key={'SearchBar'}
    role={'SearchBar'}
    />
        </Div>
    );
}

const SearchField = styled.input<{ alignment: 'left' | 'center' }>`
  float: ${props => props.alignment};
  padding: 10px;
  margin: 0px;
  border: none;
  background-color: white;
  border-radius: 10px;

  input:focus {
    border: none;
  }
`
const Logo = styled.img`
  float: left;
  background-color: white;
  height: ${SearchField.height};
  border-radius: 10px;
  padding: 10px;
  margin: 0px;
`
const Div = styled.div`
  grid-column-end: span 2;
  display: grid;
  grid-template-columns: auto 1fr;
  margin: 3px 10px 10px 10px;
  padding: 0px;
  background-color: white;
  border-color: darkgrey;
  border-width: 1px;
  border-style: solid;
  border-radius: 8px;
  box-shadow: 1px 1px grey;
`

export default SearchBar;