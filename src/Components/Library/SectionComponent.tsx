/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import { LibrarySection, TextType, ImageType } from '../../Types';
import TextComponent from './TextComponent';
import ImageComponent from './ImageComponent';
import { SectionDiv, StyledCollapsible } from './StyledLibComponents';

interface Props {
    //Is just short for properties which each Component must have which are Read-Only
    section : LibrarySection
}

/**
 * Renders all data of Library Section it gets passed as HTML prop
 * @param section the data of this section
 * @returns Library Section JSX, containing all Library Section data
 */
function LibSection(props: Props) {

    function getComponent(index: number, content : (TextType|ImageType)) {
        var temp;
        
        if(content.type === "text") {
            temp = content as TextType;
            return <TextComponent  text={temp.text}
                                    key={index}
                                    ></TextComponent>
        }
        else {
            temp = content as ImageType;
            return <ImageComponent  imageFile={temp.file} 
                                    imageUrl={temp.previewUrl}
                                    key={index}
                                    ></ImageComponent>
        }
    }

    return (
        <SectionDiv role={'section'}>
            <StyledCollapsible 
                trigger={'˅ ' + props.section.title} 
                triggerTagName={'h3'}
                triggerWhenOpen={'˄ ' + props.section.title}
                open={true} 
                role={'sectionTitle'}
            >    
            
                {props.section.content.map((content, i) => getComponent(i, content))}
            
            </StyledCollapsible>
        </SectionDiv>
    );
};

export default LibSection;