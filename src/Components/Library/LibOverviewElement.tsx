/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React from 'react';
import styled from 'styled-components';
import { theme } from '../../Theme';
import { LibraryTableData } from '../../Types';

interface Props {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    data: LibraryTableData;
    onClick: () => void
}

function LibOverviewElement(props : Props) {


    return(
        <OverviewElementDiv
            role={'tableEntry'}
            onClick={() => props.onClick()}
        >            
            <OverviewElementLabel role={'overviewEntryTitle'}>{props.data.title}</OverviewElementLabel>

            <OverviewElementLabel>{props.data.publishedBy}</OverviewElementLabel>

            <OverviewElementLabel>{props.data.lastEdited}</OverviewElementLabel>
        </OverviewElementDiv>
    );
}

const OverviewElementDiv = styled.div`
    margin: 2px;

    height: 50px;
    width: 100%;

    display: inline-block;
    text-align: center;

    border-radius: 12px;
    border-width: 1px;
    border-color: black;
    border-style: solid;

    background-color: ${theme.mainItemColor};
`

const OverviewElementLabel = styled.label`
    display: inline-flex;
    align-items: center;

    height: 100%;
    width: 32%;
    line-height:100%; 
    text-align:center;

    padding-left: 1%;

    //font-weight: bold;
    color: grey;

    //border-width: 1px;
    //border-color: green;
    //border-style: solid;
`

export default LibOverviewElement;