/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import { Label, InputField } from '../FormComponents';
import { TextType, ImageType, LibrarySection } from '../../Types';
import React, { useCallback, useState } from 'react';
import TextEditComponent from './TextEditComponent';
import ImageEditComponent from './ImageEditComponent';
import { ConfirmButton, DeleteSectionButton, MoveSectionButton, SectionDiv, SectionModDiv, StyledCollapsible } from './StyledLibComponents';

import placeholderImg from '../../Images/placeholder.png';

interface Props {
    //Is just short for properties which each Component must have which are Read-Only
    section: LibrarySection
    index: number

    setNewSection: (section: LibrarySection) => void
    moveUp: () => void
    moveDown: () => void
    removeSection: () => void
}

/**
 * @param section LibrarySection, its current content 
 * @param index number, index of Section in Entry
 * @param setNewSection (LibrarySection) => void, function to update its current content
 * @param moveUp () => void, function to move this section up, and change its index
 * @param moveDown () => void, function to move this section down, and change its index
 * @param removeSection: () => void, function to remove this section
 * @returns SectionEditComponent JSX: handles Section Title edits, and all Content components
 */
function SectionEditComponent(props: Props) {
    //unload properties
    const {section, index, setNewSection: setSection, moveUp, moveDown, removeSection} = props;
    
    const [id, ] = useState<number>(section.id);
    const [sectionTitle, setTitle] = useState(section.title);
    const [content, setContent] = useState<(TextType|ImageType)[]>(section.content)

    //const forceUpdate = useCallback(() => updateState((tick) => tick + 1), []);
    
    //UPDATE GLOBAL STATE
    /**
     * Updates Section Title upon input change, and triggers re-render
     * @param newTitle the new string to set as section title
     */
    const updateTitle = useCallback((newTitle: string) => {
        var copy = section;
        copy.title = newTitle;

        setTitle(newTitle);
        setSection(copy);
    }, [section, setSection]);

    /**
     * Updates a specific Entry Section, and triggers re-render
     * @param index index of content to change
     * @param newContent updated content, to replace old content
     */
    const updateContent = useCallback((index: number, newContent: (TextType|ImageType)) => {
        var copy = section;
        var contentCopy = [...copy.content];
        
        contentCopy[index] = newContent;
        copy.content = contentCopy;

        setContent(contentCopy);
        setSection(copy);
    }, [section, setSection]);

    /**
     * Removes a specific Content in this Section, and triggers re-render
     * @param index index of section to change
     */
    const removeContent = useCallback((index: number) => {
        var copy = section;
        var contentCopy = [...copy.content];
        
        contentCopy.splice(index, 1);
        copy.content = contentCopy;

        setContent(contentCopy);
        setSection(copy);
    }, [section, setSection]);

    /**
     * Adds a new (empty) content, and triggers re-render
     * @param newContent content data to add
     */
    const addContent = useCallback((newContent: (TextType|ImageType)) => {
        var copy = section;
        var contentCopy = [...copy.content];

        contentCopy.push(newContent);
        copy.content = contentCopy;

        setContent(contentCopy);
        setSection(copy);
    }, [section, setSection]);

    /**
     * Exchanges content with the one above, if possible, and triggers re-render
     * @param toMoveIndex index of content to move up
     */
    const moveUpContent = useCallback((toMoveIndex: number) => {
        if(toMoveIndex === 0) {
            return;
        }
        
        var copy = section;
        var contentCopy = [...copy.content];

        //switch two content elements
        contentCopy[toMoveIndex] = copy.content[toMoveIndex-1];
        contentCopy[toMoveIndex-1] = copy.content[toMoveIndex];

        //set new indices
        contentCopy[toMoveIndex-1].index = toMoveIndex-1;
        contentCopy[toMoveIndex].index = toMoveIndex;

        copy.content = contentCopy;

        setContent(contentCopy);
        setSection(copy);
    }, [section, setSection]);

    /**
     * Exchanges content with the one below, if possible, and triggers re-render
     * @param toMoveIndex index of content to move down
     */
    const moveDownContent = useCallback((toMoveIndex: number) => {
        if(toMoveIndex === section.content.length-1) {
            return;
        }
        
        var copy = section;
        var contentCopy = [...copy.content];
        
        //switch two content elements
        contentCopy[toMoveIndex] = copy.content[toMoveIndex+1];
        contentCopy[toMoveIndex+1] = copy.content[toMoveIndex];

        //set new indices
        contentCopy[toMoveIndex+1].index = toMoveIndex+1;
        contentCopy[toMoveIndex].index = toMoveIndex;
        
        copy.content = contentCopy;

        setContent(contentCopy);
        setSection(copy);
    }, [section, setSection]);

    //EVENT HANDLERS
    /**
     * Generates new (empty) TextComponent, and adds it to the corresponding Section
     * @param event Button Click Event triggering the update
     */
    const handleAddText = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        //TODO: generate ID
        const newText: TextType = {
            id: section.content.length,
            text: "",
            type: "text",
            index: section.content.length
        }

        addContent(newText)
    };

    /**
     * Handles "Add Image" button click. Generates new (empty) ImageComponent, and adds it to the corresponding Section
     * @param event Button Click Event triggering the update
     */
    const handleAddImage = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        const file = new File(['PlaceHolder'], 'PlaceHolder.png', { type: 'image/png' });

        const newImage: ImageType = {
            id: section.content.length,
            previewUrl: placeholderImg,
            file: file,
            type: "image",
            index: section.content.length
        }

        addContent(newImage);
    };

    /**
     * Handles Section title input change. Updates Section Title upon input change, and triggers re-render
     * @param event Button Click Event triggering the update
     */
    const handleUpdateTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        
        const newTitle = event.target.value;

        updateTitle(newTitle);
    }

    /**
     * Handles "move up" button click. Calls moveUp function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveUp = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveUp();
    }, [moveUp]);

    /**
     * Handles "move down" button click. Calls moveDown function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveDown = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveDown();
    }, [moveDown]);

    /**
     * Handles "remove" button click. Calls removeSection function from Props
     * @param event Button Click Event triggering the update
     */
    const handleRemoveSection = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        removeSection();
    }

    //HTML COMPONENTS
    /**
     * Calls constructor corresponding to value
     * @param value content data to render in editor
     * @param index index of element in parent list
     * @returns 
     */
    const getComponent = (value: (TextType | ImageType), index: number) => {
        if(value.type === "text") {
            return getTextEditComponent(value as TextType, index)
        }
        else {
            return getImageEditComponent(value as ImageType, index)
        }
    }

    /**
     * Returns TextEditComponent JSX
     * @param value text to render in editor
     * @param index index of element in parent list
     * @returns 
     */
    const getTextEditComponent = (value: TextType, index: number) => {
        return( 
            <TextEditComponent  
                key={index+"text"}
                index={props.index}
                data={value} 
                setNewText={(newText: TextType) => updateContent(index, newText)}
                removeContent={() => removeContent(index)}
                moveUp={() => moveUpContent(index)}
                moveDown={() => moveDownContent(index)}
            ></TextEditComponent>
        );
    }

    /**
     * Returns ImageEditComponent JSX
     * @param value image to render in editor
     * @param index index of element in parent list
     * @returns 
     */
    const getImageEditComponent = (value: ImageType, index: number) => {
        return(
                <ImageEditComponent 
                    key={index+"image"}
                    index={props.index}
                    data={value} 
                    setNewImage={(newImage: ImageType) => updateContent(index, newImage)}
                    removeContent={() => removeContent(index)}
                    moveUp={() => moveUpContent(index)}
                    moveDown={() => moveDownContent(index)}
                ></ImageEditComponent>
        );
    }

    /**
     * Sets the string in the trigger/header of Collapsible
     * @returns title, or a placeholder if title is empty
     */
    function getTriggerText() {
        if(sectionTitle === "") return "<Insert Section Title>";
        else return sectionTitle;
    }

    return(
        <div>
        <SectionModDiv key={"section"+id}>
            <StyledCollapsible 
                trigger={'˅ ' + getTriggerText()} 
                triggerTagName={'h3'} 
                triggerWhenOpen={'˄ ' + getTriggerText()}
                open={true}
                role={'collapsible'}>
                <SectionDiv key={"section"+id} role={'sectionEdit'}>    
                    <Label 
                        key={"sectionEditTitleLabel"+index}
                        role={'sectionEditTitleLabel'}
                    >Section Title:</Label>
                    
                    <InputField placeholder="Insert Section Title" 
                                value={sectionTitle} 
                                onChange={e => handleUpdateTitle(e)} 
                                key={"sectionTitleField"+index}
                                role={'sectionEditTitleInput'}
                                ></InputField>
                    
                    { content.map((value, index) => getComponent(value, index)) }

                    <Label 
                        key={"sectionAddContentLabel"+index}
                        role={'sectionAddContentLabel'}
                    >Add element:</Label>
                    
                    <ConfirmButton  
                        onClick={e => handleAddText(e)}
                        key={"sectionAddTextButton"+index}
                        role={'sectionAddTextButton'}
                    >Add Textfield</ConfirmButton>
                        
                    <ConfirmButton  
                        onClick={e => handleAddImage(e)}
                        key={"sectionAddImageButton"+index}
                        role={'sectionAddImageButton'}
                    >Add Image</ConfirmButton>
                </SectionDiv>
                </StyledCollapsible>
        </SectionModDiv>
        <MoveSectionButton 
                onClick={e => handleMoveUp(e)} 
                key={"sectionMoveUpButton"+index} 
                role={'sectionMoveUpButton'}
        > ˄ </MoveSectionButton>

        <MoveSectionButton 
                    onClick={e => handleMoveDown(e)} 
                    key={"sectionMoveDownButton"+index} 
                    role={'sectionMoveDownButton'}
        > ˅ </MoveSectionButton>

        <DeleteSectionButton 
                    onClick={e => handleRemoveSection(e)}
                    key={"sectionRemoveButton"+index}
                    role={'sectionRemoveButton'}
        > - </DeleteSectionButton>
        </div>
    );
}

export default SectionEditComponent;