/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { theme, buttonTheme } from '../../Theme';
import Collapsible from 'react-collapsible';

/**
 * Wrapper of Entry
 */
export const EntryDiv = styled.div`
    padding: 10px;
    margin: auto;
    
    background-color: ${theme.mainBg};

    width: 70%;
`;

/**
 * Collapsible class; currently empty placeholder
 */
export const StyledCollapsible = styled(Collapsible)`
  
`

/**
 * Wrapper of Section and modify-buttons
 */
export const SectionModDiv = styled.div`
    display: inline-block;
    width: 85%;

    padding: 10px;
    margin: 10px;

    background-color: ${theme.mainItemColor};
    
    border-color: darkgrey;
    border-width: 1px;
    border-style: solid;
    border-radius: 25px;
    box-shadow: 1px 1px grey;
`

/**
 * Wrapper of Section
 */
export const SectionDiv = styled.div`
    display: inline-block;
    width: 95%;
    
    padding: 10px;
    margin: 10px;

    background-color: ${theme.mainItemColor};
    

    border-width: 0px;
    border-style: solid;
    border-radius: 25px;
`

/**
 * Wrapper of Image and modify-buttons
 */
export const ImageModDiv = styled.div`
    padding: 10px;
    margin: 10px;

    border-color: blue;
    border-width: 0px;
    border-style: solid;

    background-color: ${theme.mainItemColor};

    width: 95%;
`
/**
 * Wrapper of Image
 */
export const ImageDiv = styled.div`
  display: inline-block;
  width: 80%;

  padding: 10px;
  margin: 10px;

  border-color: ${theme.mainBlack};
  border-width: 0px;
  border-style: solid;

  background-color: ${theme.mainItemColor};
`
/**
 * Wrapper of Text and modify-buttons
 */
export const TextModDiv = styled.div`
  display: inline-block;
  width: 95%;
  
  padding: 10px;
  margin: 10px;
  border-color: blue;
  border-width: 0px;
  border-style: solid;
  
  background-color: ${theme.mainItemColor};
`

/**
 * Styled TextArea
 */
export const TextInput = styled.textarea`
  display: inline-block;
  width: 80%;
  
  padding: 10px;
  margin: 10px;

  background-color: ${theme.mainItemColor};  
`

/**
 * Wrapper of displayed Image
 */
export const ImagePreview = styled.img`
  max-width: 100%;
`

/**
 * Link styled as main-color button
 */
export const StyledLink = styled(Link)`
  background-color: ${theme.mainYellow};
  color: ${buttonTheme.TextColor};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: ${buttonTheme.Border};
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;

/**
 * Main-color button, used for confirmation
 */
export const ConfirmButton = styled.button`
  background-color: ${theme.mainYellow};
  color: ${buttonTheme.TextColor};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: ${buttonTheme.Border};
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;

/**
 * Button used to remove sections
 */
export const DeleteSectionButton = styled.button`
  float: right;

  background-color: ${theme.mainBg};
  color: ${theme.mainRed};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: none;
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;

/**
 * Button used to remove within Section
 */
export const DeleteButton = styled.button`
  float: right;

  background-color: ${theme.mainItemColor};
  color: ${theme.mainRed};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: none;
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;

/**
 * Button used to move Section
 */
export const MoveSectionButton = styled.button`
  float: right;

  background-color: ${theme.mainBg};
  color: ${buttonTheme.TextColor};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: none;
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;

/**
 * Button used to move within Section
 */
export const MoveButton = styled.button`
  float: right;

  background-color: ${theme.mainItemColor};
  color: ${buttonTheme.TextColor};
  font-size: ${buttonTheme.FontSize};
  font-weight: ${buttonTheme.FontWeight};

  padding: ${buttonTheme.Padding};
  margin: ${buttonTheme.Margin};
  
  border: none;
  border-radius: ${buttonTheme.BorderRadius};

  align-self: ${buttonTheme.AlignSelf};
  outline: ${buttonTheme.Outline};
`;