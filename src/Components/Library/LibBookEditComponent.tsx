/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import { Label, InputField, SubmitButton } from '../FormComponents';
import { LibrarySection, LibraryBook } from '../../Types';
import React, { useCallback, useState } from 'react';
import SectionEditComponent from './SectionEditComponent';
import { ConfirmButton, EntryDiv } from './StyledLibComponents';

interface Props {
    data: LibraryBook

    setBook: (newData: LibraryBook) => void
}
/**
 * @param data LibraryData, its current content 
 * @param setBook (LibraryData) => void, function to update its curren content
 * @returns LibraryEntryEditComponent JSX: handles Entry Title edits, lds all SectionEditComponents
 */
function LibEntryEditComponent(props: Props) {
    //const [id, ] = useState<number>(props.data.id);
    const [entryTitle, setEntryTitle] = useState<string>(props.data.title);
    //const [publishedBy, ] = useState<string>(props.data.title);
    //const [lastEdited, ] = useState<number>(props.data.lastEdited);
    const [sections, setSections] = useState<LibrarySection[]>(props.data.sections)

    const handleSubmit = (event: React.FormEvent) => {
        event.preventDefault();
        
        //TODO: send to server
    }

    //unload properties
    const {data, setBook: setLibraryData} = props;

    //SETTERS SECTION COMPONENT
    /**
     * Updates Entry Title upon input change, and triggers re-render
     * @param e change of input Event triggering the update
     */
    const updateTitle = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        
        var newTitle = e.target.value;

        var copy = data;
        copy.title = newTitle;
        
        setEntryTitle(newTitle);
        setLibraryData(copy);
    }, [data, setLibraryData]);

    /**
     * Updates a specific Entry Section, and triggers re-render
     * @param index index of section to change
     * @param newSection updated section, to replace old section
     */
    const updateSection = useCallback((index: number, newSection: LibrarySection) => {
        var copy = data;
        var sectionsCopy = [...copy.sections];

        sectionsCopy[index] = newSection;
        copy.sections = sectionsCopy;

        setSections(sectionsCopy);
        setLibraryData(copy);
    }, [data, setLibraryData]);

    /**
     * Removes a specific Entry Section, and triggers re-render
     * @param index index of section to change
     */
    const removeSection = useCallback((index: number) => {
        var copy = data;
        var sectionsCopy = [...copy.sections];

        //remove section
        sectionsCopy.splice(index, 1);

        //set new indices
        for(var i=index; i<sectionsCopy.length; i++) {
            sectionsCopy[i].index = i;
        }

        copy.sections = sectionsCopy;

        setSections(sectionsCopy);
        setLibraryData(copy);
    }, [data, setLibraryData]);

    /**
     * Adds a new (empty) section, and triggers re-render
     * @param e Button Click Event triggering the update
     */
    const addSection = useCallback((e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        
        //TODO: create new section on server, get ID
        var tempID = 0;
        for(let i=0; i<data.sections.length; i++) {
            tempID = Math.max(tempID, data.sections[i].id);
        }
        tempID++;

        //set new index
        var newIndex = data.sections.length;
        
        var newSection: LibrarySection = {
            id: tempID,
            index: newIndex,
            title: "",
            content: []
        }
        
        var copy = data;
        var sectionsCopy = [...copy.sections];
        
        sectionsCopy.push(newSection);
        copy.sections = sectionsCopy;

        setSections(sectionsCopy);
        setLibraryData(copy);
    }, [data, setLibraryData]);

    /**
     * Exchanges section with the one above, if possible, and triggers re-render
     * @param toMoveIndex index of section to move up
     */
    const moveUpSection = useCallback((toMoveIndex: number) => {
        if(toMoveIndex === 0) {
            return;
        }
        
        var copy = data;
        var sectionsCopy = [...copy.sections];

        //switch two section elements
        sectionsCopy[toMoveIndex] = copy.sections[toMoveIndex-1];
        sectionsCopy[toMoveIndex-1] = copy.sections[toMoveIndex];

        //set new indices
        sectionsCopy[toMoveIndex].index = toMoveIndex;
        sectionsCopy[toMoveIndex-1].index = toMoveIndex-1;

        copy.sections = sectionsCopy;

        setSections(sectionsCopy);
        setLibraryData(copy);
    }, [data, setLibraryData]);

    /**
     * Exchanges section with the one below, if possible, and triggers re-render
     * @param toMoveIndex index of section to move down
     */
    const moveDownSection = (toMoveIndex: number) => {
        if(toMoveIndex === data.sections.length-1) {
            return;
        }
        
        moveUpSection(toMoveIndex+1);
    };
    
    return (
        <EntryDiv key={-1} role={'entryEdit'}>
            <h2 key={-1} role={'entryEditTitle'}>{entryTitle}</h2>

            <div key={-2}>
                <Label key={-1} role={'entryEditTitleLabel'}>Entry Title:</Label>

                <InputField 
                    key={-2}
                    placeholder="Insert Title" 
                    value={entryTitle} 
                    onChange={e => updateTitle(e)}
                    role={'entryEditTitleInput'}
                ></InputField>
            </div>

            { sections.map((value, index) => <SectionEditComponent  
                                                key={value.id}
                                                section={value} 
                                                index={index}
                                                setNewSection={(newSection: LibrarySection) => updateSection(index, newSection)}
                                                removeSection={() => removeSection(index)}
                                                moveUp={() => moveUpSection(index)}
                                                moveDown={() => moveDownSection(index)}
                                             ></SectionEditComponent>) }

            <div key={-3}>
                <ConfirmButton key={-1} onClick={(e) => addSection(e)} role={'addSectionButton'}>Add Section</ConfirmButton>
                
                <SubmitButton key={-2} type="submit" onClick={handleSubmit} role={'submitButton'}>Confirm</SubmitButton>
            </div>
        </EntryDiv>
    );

}

export default LibEntryEditComponent;
            