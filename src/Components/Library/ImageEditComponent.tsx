/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React, { ChangeEvent, useCallback } from 'react';
import { Label, InputField } from '../FormComponents';
import { ImageType } from '../../Types';
import { DeleteButton, MoveButton, ImageDiv, ImageModDiv, ImagePreview } from './StyledLibComponents';

interface ICProps {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    data: ImageType
    index: number
    
    setNewImage: (newImage: ImageType) => void
    moveUp: () => void
    moveDown: () => void
    removeContent: () => void
}

/**
 * @param data ImageType, its current content 
 * @param index number, index of TextComponent in Entry
 * @param setSection (ImageType) => void, function to update its current content
 * @param moveUp () => void, function to move this content up, and change its index
 * @param moveDown () => void, function to move this content down, and change its index
 * @param removeContent: () => void, function to remove this content
 * @returns ImageEditComponent JSX: handles edits in its image
 */
function ImageEditComponent(props: ICProps)
{
    //unload properties
    const {data, index, setNewImage, moveUp, moveDown, removeContent} = props;
    
    //const [url, ] = useState<string>(data.url);
    //const [image, ] = useState<File>(data.file);

    //UPDATE GLOBAL STATE
    /**
     * Updates Content Image upon input change, and triggers re-render
     * @param e change of input Event triggering the update
     */
    const updateImage = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        const fileList = e.target.files;
        if (!fileList) return;

        var newImage: Blob;
        newImage = fileList[0] as Blob;
        
        var copy = data;

        var reader = new FileReader();

        reader.readAsDataURL(newImage as Blob);
        
        //newImage.arrayBuffer().then((arrayBuffer) => {
        //    var blob = new Blob([new Uint8Array(arrayBuffer)], {type: newImage.type });
        //    reader.readAsDataURL(blob);
        //});

        reader.onloadend = function(){
            copy.previewUrl = reader.result as string;
            copy.file = newImage as File;
            setNewImage(copy);
        };

    }, [data, setNewImage]);

    /**
     * Handles "move up" button click. Calls moveUp function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveUp = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveUp();
    }, [moveUp]);

    /**
     * Handles "move down" button click. Calls moveDown function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveDown = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveDown();
    }, [moveDown]);

    /**
     * Handles "remove" button click. Calls removeContent function from Props
     * @param event Button Click Event triggering the update
     */
    const handleRemoveContent = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        removeContent();
    }, [removeContent]);

    return(
        <ImageModDiv key={"ImageComponent"+index}>
            <ImageDiv key={"ImageComponent"+index} role={'imageEdit'}>
                <ImagePreview src={"" + data.previewUrl} alt={""}/>
                
                <Label role={'imageEditLabel'}>Image Upload:</Label>

                <InputField 
                    type="file" 
                    accept="image/*" 
                    onChange={e => updateImage(e)}
                    role={'imageEditInput'}
                />
            </ImageDiv>
        
            <MoveButton 
                onClick={e => handleMoveUp(e)} 
                key={"moveUpButton"}
                role={'imageMoveUpButton'}
            > ˄ </MoveButton>
            
            <MoveButton 
                onClick={e => handleMoveDown(e)} 
                key={"moveDownButton"}
                role={'imageMoveDownButton'}
            > ˅ </MoveButton>
            
            <DeleteButton 
                onClick={e => handleRemoveContent(e)}
                key={"removeButton"}
                role={'imageRemoveButton'}
            > - </DeleteButton>
        </ImageModDiv>
    );
}

export default ImageEditComponent;