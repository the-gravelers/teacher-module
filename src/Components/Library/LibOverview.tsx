/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
/* eslint-disable jsx-a11y/aria-role */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { theme } from '../../Theme';
import { LibraryTableData } from '../../Types';
import LibOverviewElement from './LibOverviewElement';
import SearchBar from '../SearchBar';

interface Props {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    data: LibraryTableData[];
}

function LibOverview(props : Props) {
    const [showData, setShowData] = useState(props.data)

    const history = useHistory();
    
    const onClick = (row: LibraryTableData) => {
        history.push('/LibEntry/'+row.id);
      }

    return(
        <LibOverviewDiv>
            <SearchBar 
                data={props.data}
                dataToShow={setShowData}
                alignment={"left"}
                placeholder={""}
            ></SearchBar>

            <Header            
                key={'tableHeader'}
                role={'tableHeader'}
            >
                <HeaderLabel
                    key={'headerTitle'}
                    role={'headerTitle'}
                >{"TITLE"}</HeaderLabel>

                <HeaderLabel
                    key={'headerAuthor'}
                    role={'headerAuthor'}
                >{"MADE BY"}</HeaderLabel>

                <HeaderLabel
                    key={'headerLastEdited'}
                    role={'headerLastEdited'}
                >{"DATE"}</HeaderLabel>
            </Header>

            {showData.map((d, i) => <LibOverviewElement 
                                        key={i}
                                        data={d}
                                        onClick={() => onClick(d)}
                                    />)}
        </LibOverviewDiv>
    );
}

const LibOverviewDiv = styled.div`
    width: 99%;
`

const Header = styled.div`
    margin: 2px;

    height: 50px;
    width: 100%;

    display: inline-block;
    text-align: center;
    background-color: ${theme.mainBg};
`

const HeaderLabel = styled.label`
    display: inline-flex;
    align-items: center;

    height: 100%;
    width: 32%;
    line-height:100%; 
    text-align:center;

    padding-left: 1%;

    font-weight: bold;
    color: black;

    //border-width: 1px;
    //border-color: green;
    //border-style: solid;
`

export default LibOverview