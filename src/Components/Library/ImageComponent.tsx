/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import { ImageDiv, ImagePreview } from './StyledLibComponents';

interface ICProps {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    imageFile: File
    imageUrl: string
    role? : string    
}

/**
 * Renders image component it gets passed as HTML prop
 * @returns Library Image JSX, containing the image
 */
function ImageComponent(props : ICProps) {
    
    return(
        <ImageDiv role={'image'}>
            <ImagePreview src={props.imageUrl} alt={""}/>
        </ImageDiv>
    );
}

export default ImageComponent;