/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React, { useCallback, useState } from "react";
import { TextType } from "../../Types";
import { DeleteButton, MoveButton, TextModDiv, TextInput } from './StyledLibComponents';

interface TCProps {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    data: TextType
    index: number
    
    setNewText: (text: TextType) => void
    moveUp: () => void
    moveDown: () => void
    removeContent: () => void
}

/**
 * @param data TextType, its current content 
 * @param index number, index of TextComponent in Entry
 * @param setSection (TextType) => void, function to update its current content
 * @param moveUp () => void, function to move this content up, and change its index
 * @param moveDown () => void, function to move this content down, and change its index
 * @param removeContent: () => void, function to remove this content
 * @returns TextEditComponent JSX: handles edits in its textarea
 */
function TextEditComponent(props: TCProps) {
    //unload properties
    const {data, index, setNewText, moveUp, moveDown, removeContent} = props;

    const [, setText] = useState<string>(data.text);

    /**
     * Updates Content Text upon input change, and triggers re-render
     * @param e change of input Event triggering the update
     */
    const updateText = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => {
        event.preventDefault();
        
        var newText = event.target.value;
        
        var copy = data;
        copy.text = newText;
        setText(newText);
        setNewText(copy);
    }, [data, setNewText]);

    /**
     * Handles "move up" button click. Calls moveUp function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveUp = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveUp();
    }, [moveUp]);

    /**
     * Handles "move down" button click. Calls moveDown function from Props
     * @param event Button Click Event triggering the update
     */
    const handleMoveDown = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        moveDown();
    }, [moveDown]);

    /**
     * Handles "remove" button click. Calls removeContent function from Props
     * @param event Button Click Event triggering the update
     */
    const handleRemoveContent = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        removeContent();
    }, [removeContent]);
    
    return (
        <TextModDiv key={"TextComponent"+index} role={'textEdit'}>
            <TextInput
                key={"0"}
                placeholder="Enter text"
                onChange={e => updateText(e)}
                value={data.text}
                role={'textArea'}
            ></TextInput>
            
            <MoveButton 
                onClick={e => handleMoveUp(e)} 
                key={"moveUpButton"}
                role={'textMoveUpButton'}
            > ˄ </MoveButton>

            <MoveButton 
                onClick={e => handleMoveDown(e)} 
                key={"moveDownButton"}
                role={'textMoveDownButton'}
            > ˅ </MoveButton>

            <DeleteButton 
                onClick={e => handleRemoveContent(e)}
                key={"removeButton"}
                role={'textRemoveButton'}
            > - </DeleteButton>
        </TextModDiv>
    );
}

export default TextEditComponent;