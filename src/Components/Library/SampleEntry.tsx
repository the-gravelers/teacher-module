/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import {LibraryBook, LibraryTableData, LibrarySection, TextType, ImageType} from '../../Types';

import rock from '../../Images/rock.png';
import rockisland from '../../Images/rockisland.png';

/**
 * Sample data to load in LibOverview
 */
export const SampleLibraryData: LibraryTableData[] = [
    {
      id: 0,
      title: "Demo Entry: Uncyclopedia page on 'Rocks'",
      publishedBy: 'Uncyclopedia',
      lastEdited: 0, 
    },
    {
      id: 1,
      title: "Demo Entry: Uncyclopedia page on 'Rocks', copy",
      publishedBy: 'Uncyclopedia',
      lastEdited: 1, 
    },
    {
      id: 2,
      title: "Test",
      publishedBy: "Developer",
      lastEdited: 2,
    }                                
  ];

/**
 * Returns a sample LibraryBook, to load in LibBook or LibBookEditor
 * Taken from Uncyclopedia
 */
export const SampleEntry = () => {
    let entryId = 0;
    let author = "Uncyclopedia";
    let lastEdited = 0;
    let entryTitle = "Demo Entry: Uncyclopedia page on 'Rocks'";
    
    let sections : LibrarySection[] = [];
    
    let sectionId = 0;
    let sectionIndex = 0;
    let sectionTitle = "";
    let contents : (TextType|ImageType)[] = [];

    let contentId = 0;
    let contentIndex = 0;
    let text = "";
    let imageUrl = "";
    let imageFile: File = File.prototype;
    const textType = "text"; const imageType = "image";

    //Section 0: Main
    sectionTitle = "Main";
    text = 'Rocks (a.k.a. Ianitus Lobreus/ Lobrites) are a group of hard, stony organisms in the mineral kingdom belonging to the phylum Lithos. Rocks are by far the Earth\'s most common inhabitants, being present in abundance on all seven continents and in every ocean, with fossilized rocks composing a large percentage of the Earth\'s crust. They were discovered and named in 12000 BC by Gordon Youd who named them "wocks", throughout the years the pronunciation has changed to rocks, however Gordon Youd insists on still using the original pronunciation. The fossil record also shows rocks to be among the oldest inhabitants of the earth; whereas the earliest multicelled organisms evolved approximately 600 million years ago, rocks have existed almost since the earth was first formed more than 4.5 billion years ago, in forms virtually unchanged since then. Also, rocks have an IQ roughly equivalent to that of Barak Obama. A sentient rock has been recently spotted in classified premises somewhere in Rosallywood.';
    addTextType();

    imageUrl = rock;
    addImageType();
    addSection();

    //Section 1: Taxonomy
    sectionTitle = "Taxonomy";

    text = 'Wocks, as they are traditionally known by the standards of Gordon Youd, often resemble Geodudes, an easy to obtain rock. Some nasty rocks, however, seek to attack us, as Gordon Youd came to discover when he was assaulted by a wild graveller on steroids. Far from deterred, Gordon Youd fought bravely against the mighty wock,in a fiery battle of spaceships and sand but, given that he was at least 30,000 years older than the wocks themselves, he fell...';
    addTextType();

    text = 'Thus ends this chapter of Gordon Youd vs the wocks, but will he make it??? We may never know...';
    addTextType();
    addSection();

    //Section 2: Domestication
    sectionTitle = "Domestication";

    text = 'Humans first domesticated rocks during the so-called Stone Age, when prehistoric man first found rocks\' immobility and extremely rugged physiology ideal in the making of small tools and weapons. Archaeologists have found the perfectly preserved bodies of small flints and other rocks that have been shaped by paleolithic humans into objects such as spear points and arrowheads. The use of rocks as tools diminished during the Bronze Age and thereafter, but domestic rocks have thrived alongside humans ever since. Today, rocks play important roles in a wide variety of fields, from building construction to curling, and have even come back into fashion as weapons in some of the more conflict-prone areas of the Middle East.';
    addTextType();

    text = 'Tame rocks may be found in and around the 1970s, being kept as pets. This was a result of the wholesale agricultural breeding of rocks and modern methods of taming, as well as a sudden decrease in the production and hence market price of rock food.';
    addTextType();
    addSection();

    //Section 3: Rocks in the food chain
    sectionTitle = "Rocks in the food chain"

    text = 'The rock is part of one of the most remarkable food chains in existence. The primary diet of the rock is scissors, which it eats by first smashing the scissors\' beak (or, in the case of the Eastern Rock, simply stunning it) and then absorbing the resultant shards. The only known predator of the rock is paper, which prefers the tactic of grabbing the rock and pulling it into the darkness, where, through some unknown function, the paper simply devours the rock.';
    addTextType();

    text = 'However, paper is kept in check by scissors, as the razor-sharp beak of the scissors can destroy paper, where the mass of the rock could not. In a nutshell, as long as scissors, paper, and rock are present in an ecosystem, they will hold one another in check quite effectively. A few unscrupulous biologists have experimented with dynamite in such ecosystems. This weapon can be very detrimental to the rock population, resulting in severe outbreaks of explosion.';
    addTextType();

    text = 'Although the scissors can attempt to sever the wick of the dynamite prematurely, some biologists have taken to even more inventive methods of destroying both rocks and scissors with one blow (and paper as well, once it begins to foil these new weapons). These methods are highly controversial in the biology community, and therefore the once-endangered rock can live in safety.';
    addTextType();

    imageUrl = rockisland;
    addImageType();
    addSection();

    //Section 4: Lifespan of rocks
    sectionTitle = "Lifespan of rocks";

    text = 'The life span of the average rock is extremely long. The U.S. Department of the Inferior, claims that rock in the San Gabriel mountains are up to 1.7 billion years old. The life span of billions years is disputed by experts in leading science journals such as Conservapedia and CreationWiki who propose a much simpler theory that the maximum life span of a rock is 6000 years. People believing this simpler theory are known as Simpletons, although they claim to have talked to San Gabriel himself who confirms their belief. BABIES!!!!!!!!!!!!1';
    addTextType();
    addSection();

    //Section 5: Reproductive Behaviour
    sectionTitle = "Reproductive behaviour";

    text = 'Many rocks breed by releasing vast amounts of their gametes, called sediment. This happens only once per year under the harvest moon. Most of this sediment eventually washes down to the sea. Here some lie down on the ocean floor and others lie on top of them. They take part in a vast orgy of sexual frenzy, and finally collapse in exhaustion to create new rocks, which live under water for quite some period. All this action takes place in something geologists call a bed. This aquatic juvenile phase seems to be an essential part of the life cycle of the Sedimenta class of rock, and is similar to that of many salamanders, to which genetic tests suggest they may be very closely related. However, like many insects they undergo a metamorphosis (or diagenesis) slowly form hard parts and eventually crawl out onto dry land where they live out the adult part of their life cycle.';
    addTextType();

    text = 'Some rocks are known to live only in caves. These rocks are known as troglodites. Little is known of their complex ecosystems, although two forms are known: stalagmites (named after the type locality of the first cave in which they were discovered, which lay under a German POW camp) and stalactites. Palaeontologists once classified these as two distinct morphological species, although recent genetic sequencing suggests sexual dimorphism of one single troglodite organism. The male stalactite drips its reproductive fluid over the sex organs of the female stalagmite, which lies underneath it. Note that a female stalagmite is never found on top of the male stalactite, as this would be considered an abomination. Most troglodites have lost all their natural pigments and appear to be a bleached white colour. Scientific tests have confirmed that they have lost the ability to see, which serves no purpose in this dark cave environment.';
    addTextType();

    text = 'Most rocks live a sessile lifestyle, although exceptions do exist, including the famous Rolling Stones, whose great proven age is direct evidence against the 6000-year limit of life span proposed by Simpletons.';
    addTextType();
    addSection();


    const result : LibraryBook = {
        id: entryId,
        title: entryTitle,
        publishedBy: author,
        lastEdited: lastEdited,
        sections: sections
    }

    return result;

    function addSection() {
        const x : LibrarySection = {
            id: sectionId,
            index: sectionIndex,
            title: sectionTitle,
            content: contents
        }

        sections.push(x);
        sectionId++;
        sectionIndex++;
        contents = [];

        contentId = 0;
        contentIndex = 0;
    }

    function addContent(c:(TextType|ImageType)) {
        contents.push(c);

        contentId++;
        contentIndex++;
    }

    function addTextType() {
        const x : TextType = {
            id: contentId,
            index: contentIndex,
            text: text,
            type: textType
        }

        addContent(x);
    }

    function addImageType() {
        const x : ImageType = {
            id: contentId, 
            index: contentIndex,
            previewUrl: imageUrl,
            file: imageFile,
            type: imageType
        }
    
        addContent(x);
    }
}