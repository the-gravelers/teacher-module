/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import { TextModDiv } from './StyledLibComponents';

interface TCProps {
    //Is just short for properties which each Component(Class) must have which are Read-Only
    text: string
    role? : string
}

/**
 * Renders text component it gets passed as HTML prop
 * @param text string to display
 * @returns Library Text JSX, containing the text
 */
function TextComponent(props : TCProps) {
    
    return (
        <TextModDiv role={'text'}>
            <p>{props.text}</p>
        </TextModDiv>
    );
}

export default TextComponent;