/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react';
import styled from 'styled-components';
import {ToggleArrow} from './ToggleArrow';
import {Group} from '../Types';
import delButtonImg from '../Images/minus_centered.png';
import { kickMember } from '../Server/GroupsRequests';

interface extensionProps {
    group: Group
}

/**
 * Display of a single group with visible name and button to toggle to show the members
 * @param props the group you want displayed
 * @returns JSX Element
 */
export function GroupDisplay(props: extensionProps) {
    const {group} = props;
    const [open, toggleOpen] = useState(false);
    return (
        <Display>
            <GroupHeader>{group.name + ", members: " + group.members.length}</GroupHeader>
            <ToggleArrow open={open} toggleOpen={toggleOpen}/>
            {open && group.users ? group.users.map((member, memberIdx) => <MemberContainer key={memberIdx}>
                <Member>{member.name}</Member>
                <KickButton src={delButtonImg} onClick={() => kickMember(member.id_str)}
                            alt="KickButton"/>
            </MemberContainer>) : undefined}
        </Display>
    )
}

const Display = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
`
const GroupHeader = styled.div`
  font-size: 1.17em;
  font-weight: bold;
  color: ${props => props.theme.buttonLinkColor};
  text-align: center;
`
const MemberContainer = styled.div`
  grid-column-end: span 2;
  display: grid;
  grid-template-columns: 1fr auto;
`
const Member = styled.div`
  margin: 0px 5%;
`
const KickButton = styled.img`
  text-align: center;
  height: 25px;
  width: auto;
  cursor: pointer;
`