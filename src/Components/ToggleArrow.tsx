/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import styled from 'styled-components';

interface ArrowProps {
    open: boolean;
    toggleOpen: (whatever: boolean) => void;
}

/**
 * Gives an div displaying a Up-arrow or Down-arrow depending on wheter or not its open
 * @param props containing a bool wheter open or not and a function
 * @returns JSX element with an onClick to change the bool to open
 */
export function ToggleArrow(props: ArrowProps) {
    if (props.open) {
        return (
            <Arrow onClick={() => props.toggleOpen(!props.open)} style={{cursor: 'pointer'}}
                   role="showDetails"> ˄ </Arrow>
        );
    } else {
        return (
            <Arrow onClick={() => props.toggleOpen(!props.open)} style={{cursor: 'pointer'}}
                   role="showDetails"> ˅ </Arrow>
        );
    }
}

const Arrow = styled.div`
  margin: auto 2px;
  font-weight:bold;
  font-size: 1.17em;
`