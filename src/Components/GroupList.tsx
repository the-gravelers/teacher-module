/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {GridLink} from './GridComponents'
import {theme} from '../Theme';
import {deleteGroup, getGroups, getUsersFromGroup} from '../Server/GroupsRequests';
import {browsingStore} from '../Stores/BrowsingStore';
import {useMutation, useQuery, useQueryClient} from 'react-query';
import {DeleteButton} from './FlexListComponents';
import {GroupDisplay} from './GroupDisplay';
import {Group} from '../Types';
import { SuccessToast } from './Toasts';

interface GroupListProps {
    classId: string
}

/**
 * Displays a the groups from given class and a button to add more groups
 * @param props the class you want the user-groups from
 * @returns A JSX element
 */
export function GroupList(props: GroupListProps) {
    const {classId} = props //Unbind the props

    const [activeIndex, setActiveIndex] = useState(0);
    const [groups, setGroups] = useState<Group[]>([]);

    //Gets the query stuf from react-query
    const queryClient = useQueryClient();

    //Remove group form the server
    const groupRemove = useMutation((groupId: string) => deleteGroup(groupId), {
        onSuccess: () => {
            SuccessToast('Group deleted');
            //Invalids the current list of groups so it retrieves again
            queryClient.invalidateQueries('groups');
        }
    })

    return (
        <Grid>
            {GroupsData(classId)}
            <ButtonHolder>
                <GridLink backgroundcolor={theme.mainYellow} to="/Group">Add Group</GridLink>
                <DeleteButton alignment="left"
                              onClick={_e => groups !== [] ? groupRemove.mutate(groups[activeIndex].id) : undefined}>Delete</DeleteButton>
            </ButtonHolder>
        </Grid>
    );

    /**
     * Handles the stages of retrieving the groups from the server
     * @param activeClass the id of the currentyl active class as string
     * @returns JSX.Element | JSX.Element[] | undefined
     */
    function GroupsData(activeClass: string) {
        const {data: groups, isLoading, isError} = useFetchGroups(activeClass);

        useEffect(() => {
            if (groups) {
                setGroups(groups);
            }
        }, [groups])

        //If still loading
        if (isLoading) {
            return <div>Loading...</div>
        }
        //If error along the way
        if (isError)
            return <div>Error getting groups</div>

        return (
            groups ? groups.map((group, groupIdx) =>
                <GroupElem key={groupIdx} onClick={_e => setActiveIndex(groupIdx)} active={activeIndex === groupIdx}>
                    <GroupDisplay group={group}/>
                </GroupElem>) : undefined
        )
    }
}

/**
 *
 * @param getClass the class to retrieve groups from.
 * @returns Query for the groups
 */
export function useFetchGroups(getClass: string) {
    //Just for making new group that it has a sensible number
    const setAmountOfGroups = browsingStore((state) => state.setAmountOfGroups)

    return (useQuery(['groups', getClass], async () => {
        const groups = await getGroups(getClass);
        let copy : Group[] = [...groups]
        for(let i=0; i< copy.length; i++) {
            copy[i].users = await getUsersFromGroup(copy[i].id);
        }
        setAmountOfGroups(copy.length);
        return copy;
    }));
}

//Styling
const Grid = styled.div`
  display: grid;
  grid-template-columns: auto;
  align-content: start;
  overflow-y: auto;
  height: 78vh; //TODO fix this terror
`
const ButtonHolder = styled.div`

`
const GroupElem = styled.div<{ active: boolean }>`
  margin: 3px;
  padding: 3px;
  background-color: ${props => props.theme.mainItemColor};
  border-radius: 8px;
  padding: ${props => props.active ? "5px" : "6px"};
  border: ${props => props.active ? "2px solid #307D91" : "1px solid lightgrey"};
`
