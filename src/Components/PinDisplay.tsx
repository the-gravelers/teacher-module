/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React, {useState} from 'react';
import styled from 'styled-components';
import {activePinStore} from '../Stores/ActivePinStore';
import {PinContent, PinPreview} from '../Types';
import {theme} from '../Theme';
import {getPin} from '../Server/PinRequests';
import {ToggleArrow} from './ToggleArrow';
import {OpenableText} from './OpenableText';
import {getPinContent} from '../Server/PinContentRequests';
import {useHistory} from 'react-router-dom';

interface extensionProps {
    pin: PinPreview;
}

/**
 * Displays a pin and a button toggle the display of the text
 * @param props the pin you want to display
 * @returns JSX component
 */
export function PinDisplay(props: extensionProps) {
    const history = useHistory();
    const {pin} = props;
    const setActivePin = activePinStore((state) => state.setActivePin)
    const [open, toggleOpen] = useState(false);
    return (
        <Display>
            <PinName onClick={(_event) => {
                getPin(pin.id).then(data => {
                    if (data) {
                        let content: PinContent[] = [];
                        const getContent = async () => {
                            for (let i = 0; i < data.contents.length; i++) {
                                let val = data.contents[i];
                                if (val.contentId) {
                                    const singleFile = await getPinContent(pin.id, val.contentId);
                                    if (singleFile) {
                                        content.push({
                                            content: singleFile,
                                            contentId: data.contents[i].contentId,
                                            content_nr: data.contents[i].content_nr,
                                            content_type: data.contents[i].content_type,
                                            pinId: pin.id
                                        })
                                    }
                                }
                            }
                            setActivePin({
                                id: pin.id,
                                last_edited: data.last_edited,
                                utm: data.utm,
                                lat: data.lat,
                                lon: data.lon,
                                contents: content,
                                text: data.text,
                                title: data.title
                            });
                            history.push('/Pin');
                        }
                        getContent();
                    }
                })
            }} data-testid="pinName">
                {props.pin.title}
            </PinName>
            <Date>{props.pin.last_edited}</Date>
            {OpenableText(<ToggleArrow open={open} toggleOpen={toggleOpen}/>, props.pin.text)}
        </Display>
    );
}

const Display = styled.div`
  display: grid;
  background-color: ${theme.mainItemColor};
  margin: .75% 0px;
  padding: 3px;
  border-radius: 8px;
  border: 1px solid lightgrey;
  grid-template-columns: 1fr auto auto;
`
const Date = styled.div`
  margin: auto 1px;
  padding: 3px;
`
const PinName = styled.div`
  font-size: 1.17em;
  font-weight: bold;
  text-decoration: none;
  cursor: pointer;
  color: ${props => props.theme.ButtonLinkColor};
`