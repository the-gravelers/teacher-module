/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

export const theme = {
    mainYellow: "#FFD600",
    mainRed: "#E93A3A",
    mainBg: "#E9E9E9",
    mainBlack: "black",
    mainItemColor: "white",
    ButtonLinkColor: "black",
    expandedList: "white"
}

export const buttonTheme = {
    TextColor: "black",
    FontSize: "1.17em",
    FontWeight: "bold",

    Padding: "3px 8px",
    Margin: "3px",

    Border: "none",
    BorderRadius: "8px",

    AlignSelf: "center",
    Outline: "none"
}