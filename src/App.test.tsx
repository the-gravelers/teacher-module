/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */
import React from 'react';
import {fireEvent, render, waitFor} from '@testing-library/react';
import App from './App';
import {AuthToken, UserInfo} from './Types';
import {rendWrapper} from './Tests/renderWrapper';
import {setCookie} from './Server/Cookie';
import {nockLogin, nockUserInfo} from './Tests/mocks/AuthorizationMocks';
import {nockGetCourses} from './Tests/mocks/CourseRequestMocks';
import {ServerCourse, uniId} from './Server/CourseRequests';
import {nockGetClasses} from './Tests/mocks/ClassRequestMocks';
import {ServerClass} from './Server/ClassRequests';

//DUMMY DATA
const courseGeo: ServerCourse = {
    course_id_str: "1",
    university_id_str: uniId.toString(),
    name: "GEO3-1210"
}
const courseEarth: ServerCourse = {
    course_id_str: "2",
    university_id_str: uniId.toString(),
    name: "GEO4-1517A"
}

const class2021Earth: ServerClass = {
    class_id_str: "1",
    course_id_str: courseEarth.course_id_str,
    name: "Class of 2021/22",
    pin_group_id_str: "1",
}

const token: AuthToken = {
    access_token: "1",
    refresh_token: "1",
    expires_in: 10000,
    session_id_str: '1',
    token_type: 'bearer',
}

const teacherAcc: UserInfo = {
    auth_level: 3,
    class_id_str: "1",
    email: "henk@krol.hotmail",
    id_str: "1",
    name: "Henk Krol",
}
const studentAcc: UserInfo = {
    auth_level: 1,
    class_id_str: "1",
    email: "jan@uu.gmail",
    id_str: "1",
    name: "Jan Pieters",
}
//END DUMMY DATA

test('Login session expired automatic reauth', async () => {
    //Fake being loged in with expired session
    setCookie("refresh_token", '1', 364); //Refresh token technically lasts forever but we store it a year
    setCookie("session_id", '1', 364); //Session id

    nockUserInfo(teacherAcc);
    nockLogin(token);

    const {getByText, getByRole} = render(rendWrapper(<App/>));

    nockUserInfo(teacherAcc);

    nockGetCourses([courseGeo, courseEarth]);
    nockGetClasses(courseGeo.course_id_str, []);
    nockGetClasses(courseEarth.course_id_str, [class2021Earth]);

    await waitFor(() => {
        getByText('Course List');
        getByText(courseGeo.name);
    })

    const earthCourse = getByRole('button', {name: courseEarth.name});
    fireEvent.click(earthCourse);

    await waitFor(() => {
        getByText(class2021Earth.name);
    })
})