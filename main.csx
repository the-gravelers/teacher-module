//A small C# script which counts the combined numbers of lines in the src folder.
//To run it have this file open press F5 and press it ones again to continue then it should write it in the console
//or you can just hover over count to see the number

#! "netcoreapp3.1"
#r "nuget: Newtonsoft.Json, 13.0.1"

using System;
using System.IO;
using System.Linq;
using System.Diagnostics;

var files = Directory.GetFiles("./src", "*.*", SearchOption.AllDirectories);
var count = 0;
foreach(var file in files) {
    count += File.ReadLines(file).Count();
}
Console.WriteLine("Number of files:" + files.Length);
Console.WriteLine("Lines of code:" + count);

var foo = "kl";