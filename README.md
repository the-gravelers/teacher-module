# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Recommended to use Visual studio or Webstorm as IDE.

For installation clone the repositery.
then open a terminal or command line in the repositery.
use the command `npm install`. After this all packages should be installed.
You should do `npm audit fix` to fix some insecureties. Not all of them get fixed, but that is not important.

Then you should be able to run `npm start` to get a local version of the Teacher Module running.
`npm test -a` would run all our test. If you want the code coverage run `npm test -- --coverage --watchAll=false`

## The Project

This is a web interface for Teachers and Teaching assistents. The site is not meant for students. The only thing
students will reach the website for is registering of their account for the Mobile App. This module is supposedly mostly
used when the Teacher is on their desktop computer.

#### Here teachers can:
-  Manage courses of the University.
-  Manage library content. 
-  Manage user groups.
-  Manage Pins.

#### The goal:
- To make it easier to make pins for the teacher while not on the physical location.
- The only place to manage the library which is accesible for students in the Mobile App.
- Make new years and copy over the old content you would like to keep.
- Manage student and teacher pins in an easy and concise way.
